system("cut FreqDepSysBathCou.txt -d' ' -f3- | cut -d']' -f1 > data.txt")
set title 'Frequency dependence of system-bath coupling'
set label 'Single atom \"system\" between two bulk \"atoms\" with exponentially decaying potential in 1-D'
set xlabel '{/symbol w} / cm^{-1}'
set ylabel '{/symbol G_{eff.}} / cm^{-1}'

set terminal svg enhanced
set output 'FreqDepSysBathCou.svg'
p 'data.txt' w errorlines noti

set terminal x11 enhanced
p 'data.txt' w errorlines noti
pause -1

