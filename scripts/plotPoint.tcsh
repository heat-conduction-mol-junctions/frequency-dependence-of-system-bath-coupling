#! /bin/bash -e
#PBS -q nano1
#PBS -l nodes=1:ppn=15
###  -N 1234567890123456
#PBS -N FreqSysBathCoupl
#PBS -o FreqDepSysBathCoup.io
#PBS -e FreqDepSysBathCoup.err
#PBS -M inonshar@tau.ac.il
### send e-mail on abort or exit
#PBS -m ae

function remoteNd()
{
echo
echo ==============================
echo PBS_JOBID = $PBS_JOBID
echo PBS_JOBNAME = $PBS_JOBNAME
echo PBS_QUEUE = $PBS_QUEUE
echo
echo HOST = $HOST
echo 'hostname' = `hostname`
echo PBS_O_HOST = $PBS_O_HOST
echo PBS_SERVER = $PBS_SERVER
echo
echo USER = $USER
echo LOGNAME = $LOGNAME
echo PBS_O_LOGNAME = $PBS_O_LOGNAME
echo
echo SHELL = $SHELL
echo PBS_O_SHELL = $PBS_O_SHELL
echo
echo HOME = $HOME
echo PBS_O_HOME = $PBS_O_HOME
echo
echo PWD = $PWD
echo 'pwd' = `pwd`
echo PBS_O_WORKDIR = $PBS_O_WORKDIR
echo PBS_O_INITDIR = $PBS_O_INITDIR
echo
echo PBS_NODEFILE:
cat $PBS_NODEFILE
echo
echo "# of nodes = $(wc -l $PBS_NODEFILE)" 
echo ==============================
echo
}

function probeSystemResources()
{
	echo "Probing System Resources:"
	mpstat -P ALL 1 1
	ps -U $USER -L -o pid,tid,psr,pcpu,comm
}


#MAIN()
if [ "power.tau.ac.il" != $HOST ]
	then 
	echo "Working on remote node:"
	pwd
	ls
	if [ ! -z $PBS_O_WORKDIR ] 
		then
		remoteNd
		cd $PBS_O_WORKDIR
		pwd
		ls
	else
	        echo "PBS_O_WORKDIR=$PBS_O_WORKDIR is not defined. Aborting."
	        exit 129
	fi
else
	ulimit -St 1000 # soft limit on CPU time (in seconds)
	ulimit -Sv 1000000 # soft limit on virtual memory (in KB)
fi

#scriptFilename="FrequencyDependenceOfSystemBathCoupling_py3.py"
#scriptFilename="plotPoint.py"
scriptFilename="plotPointsMP.py"

if [ ! -f $scriptFilename ] ; then echo "Required $scriptFilename file missing! Aborting." ; exit 87 ; fi

iniFilename="FrequencyDependenceOfSystemBathCoupling.ini"
bakFilename="FrequencyDependenceOfSystemBathCoupling.bak"


if [ ! -f $bakFilename ] ; then echo "Required $bakFilename file missing! Aborting." ; exit 84 ; fi


echo -e "\n*================================================*\n$(date)\n"
echo

INSTANCE=$(echo `date +%y%m%d%H%M%S%N` 1000 / p | dc)

#source /usr/share/modules/init/csh # for 'module'
module_bash () {
    eval `/usr/bin/modulecmd bash $*`
}

module_bash avail
PYTHON_DISTR="python/anaconda_python-3.4.0"
#if [ 0 -eq $($(module avail) | grep $PYTHON_DISTR | wc -l) ] ; then exit 10 ; fi

module_bash load $PYTHON_DISTR
if [ 0 -ne $? ]
	then exit 11
fi

if [ ! -z $1 ] ; then arg=$1 ; fi


if [ -f temp_$arg.old ] ; then rm temp_$arg.old ; fi

if [ ! -z $arg ]
	then
	cp $bakFilename $iniFilename 
	#sed -e 's/gamma\_b/gamma\_B/' < temp_$arg.old > temp_$arg.ini
	M=16
	for i in $(seq 1 $arg) ; do M=$[ $M * 2 ] ; done
	echo $M
	regex="'/m0\ \=\ 16/m0\ \=\ $M'"
	echo $regex
	grep m0 $iniFilename
	sed -i `$regex` $iniFilename
	grep m0 $iniFilename
fi

OUTPUT_FILENAME="FreqDepSysBathCoupl.txt"
if [ ! -f $OUTPUT_FILENAME ] ; then touch $OUTPUT_FILENAME ; fi

probeSystemResources
python ./$scriptFilename > $INSTANCE.log
probeSystemResources

if [ -f temp_$arg.old ] ; then mv temp_$arg.old FrequencyDependenceOfSystemBathCoupling.ini ; fi

#if [ "power.tau.ac.il" != $HOSTNAME ] ; then scp ; fi

if [ "power.tau.ac.il" != $HOST ]
	then
	echo "__________________________"
	echo "Present working directory:"
	echo "PWD=$PWD"
	ls
fi

echo "_____________________________________________________________________________"
echo "#Normal termination of System-Bath Coupling script."
exit 0
