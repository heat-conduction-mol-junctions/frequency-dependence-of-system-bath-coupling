#! /bin/bash -e
#PBS -q nano1
### #PBS -l nodes=nNodes:ppn=procsPerNode,walltime=[HH:]MM:SS # nNodes < 25 (nano1), ppn < 25 (and sometimes < 13)
#PBS -l nodes=1:ppn=15
#PBS -l walltime=100:00:00
###  -N 1234567890123456
#PBS -N FreqSysBathCoupl
#PBS -o FreqDepSysBathCoup.io
#PBS -e FreqDepSysBathCoup.err
#PBS -M inonshar@tau.ac.il
### send e-mail on abort or exit
#PBS -m ae
###PBS -v list environment varaibles to be passed (-V passes them all)
#PBS -v nProcs

## @fn
remoteNd() {
echo
echo ==============================
echo PBS_JOBID = $PBS_JOBID
echo PBS_JOBNAME = $PBS_JOBNAME
echo PBS_QUEUE = $PBS_QUEUE
echo
#echo queue resources max:
#qmgr -c "list queue $PBS_QUEUE" | grep resources_max
echo
echo HOST = $HOST
echo 'hostname' = `hostname`
echo PBS_O_HOST = $PBS_O_HOST
echo PBS_SERVER = $PBS_SERVER
echo
echo PBS_NODEFILE:
cat $PBS_NODEFILE
echo
echo "# of nodes = $(wc -l $PBS_NODEFILE)" 
echo
echo "Number of nodes allocated to job					= $PBS_NUM_NODES"
echo "Number of cores/node						= $PBS_NUM_PPN"
echo "Total number of cores for job (?)					= $PBS_NP"
echo "Index to node running on, relative to nodes assigned to job	= $PBS_O_NODENUM"
echo "Index to core running on, within node				= $PBS_O_VNODENUM"
echo "Index to task relative to job					= $PBS_O_TASKNUM"
echo
echo USER = $USER
echo LOGNAME = $LOGNAME
echo PBS_O_LOGNAME = $PBS_O_LOGNAME
echo
echo SHELL = $SHELL
echo PBS_O_SHELL = $PBS_O_SHELL
echo
echo HOME = $HOME
echo PBS_O_HOME = $PBS_O_HOME
echo
echo PWD = $PWD
echo 'pwd' = `pwd`
echo PBS_O_WORKDIR = $PBS_O_WORKDIR
echo PBS_O_INITDIR = $PBS_O_INITDIR
echo
echo ==============================
echo
}

## @fn
probeSystemResources() {
	echo "Probing System Resources:"
	date
	mpstat -P ALL 1 1
	ps -U $USER -L -o pid,tid,psr,pcpu,comm
}


#MAIN()
if [ "power.tau.ac.il" != $HOSTNAME ]
	then 
	echo "Working on remote node:"
	pwd
	ls
	if [ ! -z $PBS_O_WORKDIR ] 
		then
		remoteNd
		cd $PBS_O_WORKDIR
		pwd
		ls
	else
	        echo "PBS_O_WORKDIR=$PBS_O_WORKDIR is not defined. Aborting."
	        exit 129
	fi
else
	ulimit -St 1000 # soft limit on CPU time (in seconds)
	ulimit -Sv 1000000 # soft limit on virtual memory (in KB)
fi

cd ~/FrequencyDependenceOfSystemBathCoupling

#scriptFilename="FrequencyDependenceOfSystemBathCoupling_py3.py"
#scriptFilename="plotPoint.py"
declare -r scriptFilename="frequency-dependence-of-system-bath-coupling/plotPointsMP.py"

if [ ! -f $scriptFilename ] ; then echo "Required $scriptFilename file missing! Aborting." ; exit 87 ; fi

declare -r iniFilename="resources/FrequencyDependenceOfSystemBathCoupling.ini"
declare -r bakFilename="resources/FrequencyDependenceOfSystemBathCoupling.bak"


if [ ! -f $bakFilename ] ; then echo "Required $bakFilename file missing! Aborting." ; exit 84 ; fi

if [ ! -f $iniFilename ] ; then cp $bakFilename $iniFilename ; fi

echo -e "\n*================================================*\n$(date)\n"
echo

declare -ri INSTANCE=$(echo `date +%y%m%d%H%M%S%N` 1000 / p | dc)

#source /usr/share/modules/init/csh # for 'module'
## @fn
module_bash () {
	#eval `/usr/bin/modulecmd bash $*`
	BASH_MODULE="/usr/local/Modules/3.2.10/init/bash"
	source $BASH_MODULE
}

module_bash 
#module avail
declare -r PYTHON_DISTR="python/anaconda_python-3.4.0"
#if [ 0 -eq $($(module avail) | grep $PYTHON_DISTR | wc -l) ] ; then exit 10 ; fi

module load $PYTHON_DISTR
if [ 0 -ne $? ]
	then exit 11
fi

python -V 2>&1

## @fn
verifyInstallConfigParser() {
	which python
	whereis python
	which pip
	whereis pip

	CONDA_PIP="/usr/local/python_anaconda/bin/pip"
	$CONDA_PIP show configparser
	if [ 0 -ne $? ] ; then $CONDA_PIP install configparser --user ; fi
}
#verifyInstallConfigParser

if [ ! -z $1 ] ; then nProcs=$1 ; fi


if [ -f temp_$nProcs.old ] ; then rm temp_$nProcs.old ; fi

#echo -e "\$1 = $1\nnProcs = $nProcs"
#
#	if [ ! -z $nProcs ]
#		then
#		cp $bakFilename $iniFilename 
#		#sed -e 's/gamma\_b/gamma\_B/' < temp_$nProcs.old > temp_$nProcs.ini
#		M=16
#		for i in $(seq 1 $nProcs) ; do M=$[ $M * 2 ] ; done
#		echo $M
#		regex="'/m0\ \=\ 16/m0\ \=\ $M'"
#		echo $regex
#		grep m0 $iniFilename
#		sed -i `$regex` $iniFilename
#		grep m0 $iniFilename
#	fi

declare -r OUTPUT_FILENAME="out/FreqDepSysBathCou.txt"
if [ ! -f $OUTPUT_FILENAME ] ; then touch $OUTPUT_FILENAME ; fi

# probeSystemResources
echo "$(date) starting python script"
python ./$scriptFilename $nProcs 2> out/logs/$INSTANCE.err 1> out/logs/$INSTANCE.io
echo "$(date) finishing python script"
# probeSystemResources

if [ -f temp_$nProcs.old ] ; then mv temp_$nProcs.old $iniFilename ; fi

rm $iniFilename

#if [ "power.tau.ac.il" != $HOSTNAME ] ; then scp ; fi

if [ "power.tau.ac.il" != $HOSTNAME ]
	then
	echo "__________________________"
	echo "Present working directory:"
	echo "PWD=$PWD"
	ls
fi

date
echo "_____________________________________________________________________________"
echo "#Normal termination of System-Bath Coupling script."
exit 0
