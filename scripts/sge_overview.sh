#! /bin/bash

date

echo -e "\nUSAGE:\n======\nfor i in \`seq 1 \$(qselect | wc -l)\` ; do rm -f sge_overview_\$i.* ; qsub -N sge_ovrvw\$i -o sge_overview_\$i.io -e sge_overview_\$i.err -q all.q@node0\$i -hard -v node=\$i sge_overview.sh ; done ; while [ 0 -ne \`qstat | grep \$USER | wc -l\` ] ; do echo waiting ; sleep 1 ; tail -n1 \`ls -tr1 sge_overview.sh.o* | tail -n1\` ; done ; echo done ; ls -ltr"


echo -e "\nuser-host-home:\n===============\n$USER@$HOSTNAME:$HOME\n"

echo -e "\nwho's on:\n=========\n`who`\n"

echo -e "\ndefined software module paths:\n==============================\n$MODULEPATH\n"

echo -e "\navailable software modules:\n===========================\n`module avail`"

echo -e "\nSun Grid Engine (SGE) overview.\nSee also 'man sge_intro'\n"

echo "queue configuration:"
echo "===================="
echo
qconf -sconf
echo
echo "parallel environments:"
qconf -spl
echo
qconf -sep
echo

echo "queue accounting:"
echo "================="
echo
qacct -q
echo

echo "queue selections:"
echo "================="
echo
qselect
echo

echo "Full info:"
echo "=========="
echo
statq -F
echo

echo
echo node no. =		$node
echo JOB_ID =		$JOB_ID
echo JOB_NAME =		$JOB_NAME

echo QUEUE =            $QUEUE
echo quotas = 		`qquota`

echo ARC =              $ARC            $SGE_ARCH  
echo system info = 	`uname -a`
grep "cpu cores" /proc/cpuinfo | head -n1
echo no. cpus =		`grep processor /proc/cpuinfo | wc -l`
memtotal=`grep MemTotal: /proc/meminfo`
echo "$memtotal (`echo $memtotal | awk '{print int($2 / 2^20), "GB"}'`)"
echo hard-disk quota =	`quota -Qs`
echo hard-disk space = 	`df | grep /data | awk '{print ($2 / 2^30), "TB"}'`

echo BINDING =          $SGE_BINDING

echo LOGNAME =          $LOGNAME        $SGE_O_LOGNAME
echo CWD =              $CWD            $SGE_O_WORKDIR
echo CWD_PATH =         $SGE_CWD_PATH
echo STDERR_PATH =      $SGE_STDERR_PATH
echo STDOUT_PATH =      $SGE_STDOUT_PATH
echo STDIN_PATH =       $SGE_STDIN_PATH 
echo TEMPDIR =          $TEMPDIR

echo HOME =		$HOME		$SGE_O_HOME
echo HOST =		$HOST		$SGE_O_HOST
echo HOSTNAME = 	$HOSTNAME
echo MAIL = 		$MAIL		$SGE_O_MAIL
echo PATH = 		$PATH		$SGE_O_PATH
echo TZ = 		$TZ		$SGE_O_TZ
echo SHELL =		$SHELL		$SGE_O_SHELL
echo TERM =		$TERM		$SGE_O_TERM
echo


echo "queue status:"
echo "============="
echo
qstat
echo

date
echo Normal termination of $0
