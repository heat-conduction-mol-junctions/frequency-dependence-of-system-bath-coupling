Frequency Dependence of System-Bath Coupling
============================================
# Single particle in 1-D junction with exponentially decaying interactions

This program started off as an Interactive Python (IPython) notebook. As the code grew more complex, and production runs became a more dominant factor than fast prototyping, a more structured approach became necessary. The code was then exported as a Python 3 package.

# Components
Python scripts are located in `frequency-dependence-of-system-bath-coupling/`, shell scripts are located in `scripts/`.

1. `PlotPoint.py`: Performs MD simulation for a given particle mass, or range of masses, and calculates heat conduction through the system. For the purposes of parallel execution, this is only used in the single-particle-mass capacity, and parallelization is done using `plotPointsMP.py`.
2. `plotPointsMP.py`: Multiprocessing distributed execution of `PlotPoint.py`, each process given a particular particle mass to simulate for.
3. `plotPoints.bash`: BASH script for batch execution of simulations. Submits `plotPointsMP.py`, which takes care of parallelization (map and reduce phases), therefore requests multiple processors on a single node.

# Starting out
Python is interpreted, rather than compiled, so no installation is necessary. Just execute the script of your choice using a Python 3 interpreter (or as a module, passing the script after the command-line argument `-m`).

## Dependencies
1. SciPy stack. Anaconda distribution if not.

## Configuration
1. If using PyDev as IDE, then in `.pydevproject`, set `<pydev_property name="org.python.pydev.PYTHON_PROJECT_VERSION">` >= 3.

## Deployment instructions

```bash 
qsub scripts/plotPoint.bash
```
# Usage
While the program was designed to be capable of simulating any MD ensemble, it is convenient to conduct the calculation in the particular case where `T_1 = T_2 = 0`. This removes the added complexity and noise due to the random force, which exists at non-zero tempratures.

## Input
1. `frequency-dependence-of-system-bath-coupling/resources/FrequencyDependenceOfSystemBathCoupling.bak`
    1. Performance and debugging options
    2. Space-time topology
    3. Particle masses
    4. Bath properties (spectrum, temperatures, ...)
    5. Interaction parameters and functional form
    6. MD simulated-time properties
2. `scripts/plotPoint.bash` command-line argument: How many processors to use.

## Output
1. Logs of both STDOUT and STDERR.
2. `traj-*.svg`: Plot of various MD properties along the trajectory
    1. X-component of position vs. time, for all particles.
    2. X-component of velocity vs. time, for all particles.
    3. X-components of phase-space trajectory, for all particles.
    4. Atomic temperature vs. time, for all particles.
    5. Atomic energy vs. time, for all particles.
    6. Total system energy vs. time
    7. X-component of total system momentum vs. time
    8. X-component of bath random force vs. time
3. `FreqDepSysBathCou.csv`: Particle effective coupling strength (to bath pseudo-particles) vs. zero-point vibrational frequency, in comma-separated value format. Also includes column for error estimate in effective coupling strength.

# Description of `PlotPoint.py`
1. Takes input in a configuration file and prints it out to log.
2. Generates Markovian random force for step
3. EOM integration step (e.g. velocity-Verlet)
    1. Includes possible arguments for Markovian Langevin dynamics
        1. Dissipation
        2. Random force
4. Calculates potential energy term for given configuration
    1. Exponentially decaying interaction function
    2. Harmonic Oscillator
5. Calculates heat current and conductivity during a given step
6. Prints output results to log.

* In order to make this module more object-oriented, it should be refactored into the following classes:
    * MD class (synonymous with the module class PlotPoint)
        * Topology class
            * Particle class
                * Phase-space position class
            * Bath class
            * Interaction class
        * SystemUnits class
        * Integrator class
        * HeatConduction class
    * Plotting classes
    * Unit test classes


# See also
1. [Python MD](http://codepad.org/FcbFcoue)
2. [Python Verlet integrator](http://gdrcorelec.ups-tlse.fr/files/python_verlet.pdf)
3. [Numpy routines](http://docs.scipy.org/doc/numpy/reference/routines.html)

## Further reading
1. [Writing a faster Python physics simulator](http://stackoverflow.com/questions/15374291/writing-a-faster-python-physics-simulator/15375757#15375757)
2. [An MD integrator written in Python](http://dirac.cnrs-orleans.fr/MMTK/using-mmtk/mmtk-example-scripts/molecular-dynamics/an-md-integrator-written-in-pyth)

# Who do I talk to?
* @inon_s