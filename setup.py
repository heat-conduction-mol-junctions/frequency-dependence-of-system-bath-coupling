import frequency-dependence-of-system-bath-coupling # FredSybac

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': 'frequency-dependence-of-system-bath-coupling'
    'description': 'Frequency Dependence of System-Bath Coupling',
    'long_description'='',
    'url': 'https://bitbucket.org/heat-conduction-mol-junctions/frequency-dependence-of-system-bath-coupling',
    'download_url': 'ssh://git@bitbucket.org:heat-conduction-mol-junctions/frequency-dependence-of-system-bath-coupling.git',
    'author': 'Inon Sharony',
    'author_email': 'InonShar@TAU.ac.IL',
    'version': '',
    'packages': ['frequency-dependence-of-system-bath-coupling'],
    'scripts': [],
    'install_requires': [],
    'tests_require'=[],
    'classifiers' = [
        'Programming Language :: Python',
        'Natural Language :: English'
        ],
    'include_package_data'=True
}

setup(**config)
