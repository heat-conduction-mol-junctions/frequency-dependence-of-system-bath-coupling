nprocs = 11
chunk-size = 1
started <Process(Process-1, started)>
started <Process(Process-2, started)>
started <Process(Process-3, started)>
started <Process(Process-4, started)>
m0 = 64.0

running iteration #2
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 08:49:21
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 360000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
m0 = 32.0

running iteration #1
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 08:49:21
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 360000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:21
Silent? True Debug? False
Running 360000 time-steps:
RUN: @2015-09-05 08:49:21
Silent? True Debug? False
Running 360000 time-steps:
#2-#0: 0% done, 0:00:00.929996 left
mpute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 08:49:21
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 360000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
#1-#0: 0% done, 0:00:00.659998 left
RUN: @2015-09-05 08:49:21
Silent? True Debug? False
Running 360000 time-steps:
#2-#0: 0% done, 0:00:00.579997 left
RUN: @2015-09-05 08:49:21
Silent? True Debug? False
Running 360000 time-steps:
#0-#0: 0% done, 0:00:00.679998 left
#0-#0: 0% done, 0:00:00.679996 left
#3-#0: 0% done, 0:00:00.499997 left
m0 = 256.0

running iteration #4
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 256.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 640.0)
START: i = 0
START: @ 2015-09-05 08:49:25
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.   20.    5.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 720000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:25
Silent? True Debug? False
Running 720000 time-steps:
#4-#0: 0% done, 0:00:01.179997 left
#4-#0: 0% done, 0:00:00.469999 left
started <Process(Process-5, started)>
#0-#0: 2% done, 0:05:27.668056 left
m0 = 512.0

running iteration #5
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 512.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 905.0966799187809)
START: i = 0
START: @ 2015-09-05 08:49:31
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.     2.5   20. ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 1440000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:31
Silent? True Debug? False
Running 1440000 time-steps:
#5-#0: 0% done, 0:00:01.899998 left
#5-#0: 0% done, 0:00:00.469999 left
started <Process(Process-6, started)>
#2-#0: 2% done, 0:06:35.966667 left
#1-#0: 2% done, 0:06:39.787500 left
#3-#0: 2% done, 0:06:42.354167 left
#4-#0: 1% done, 0:13:37.525556 left
m0 = 1024.0

running iteration #6
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 1024.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1280.0)
START: i = 0
START: @ 2015-09-05 08:49:38
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.      1.25   20.  ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 2880000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:38
Silent? True Debug? False
Running 2880000 time-steps:
#6-#0: 0% done, 0:00:00.560000 left
#6-#0: 0% done, 0:00:00.560000 left
started <Process(Process-7, started)>
#0-#0: 5% done, 0:05:18.306111 left
#5-#0: 0% done, 0:27:53.308542 left
#1-#0: 5% done, 0:05:54.648333 left
#3-#0: 5% done, 0:06:23.472778 left
#2-#0: 5% done, 0:06:45.204444 left
m0 = 2048.0

running iteration #7
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 2048.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1810.1933598375617)
START: i = 0
START: @ 2015-09-05 08:49:46
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.       0.625   20.   ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 5760000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:46
Silent? True Debug? False
Running 5760000 time-steps:
#7-#0: 0% done, 0:00:00.450000 left
#7-#0: 0% done, 0:00:06.209996 left
started <Process(Process-8, started)>
#4-#0: 2% done, 0:12:40.413889 left
#6-#0: 0% done, 0:52:06.576007 left
#0-#0: 8% done, 0:05:24.976667 left
#5-#0: 1% done, 0:23:49.160972 left
#1-#0: 8% done, 0:05:23.042500 left
#3-#0: 8% done, 0:05:27.424167 left
#2-#0: 8% done, 0:05:28.551667 left
m0 = 4096.0

running iteration #8
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 4096.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 2560.0)
START: i = 0
START: @ 2015-09-05 08:49:56
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [  4.00000000e+02   3.12500000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 11520000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:49:56
Silent? True Debug? False
Running 11520000 time-steps:
#8-#0: 0% done, 0:00:00.670000 left
#8-#0: 0% done, 0:00:00.670000 left
started <Process(Process-9, started)>
#7-#0: 0% done, 1:37:18.056858 left
#4-#0: 4% done, 0:11:27.393333 left
#6-#0: 0% done, 0:49:22.314514 left
#0-#0: 11% done, 0:05:08.871111 left
#5-#0: 2% done, 0:23:29.432083 left
#1-#0: 11% done, 0:05:15.813333 left
#3-#0: 11% done, 0:05:17.786667 left
#2-#0: 11% done, 0:05:18.595556 left
m0 = 8192.0

running iteration #9
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 8192.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 3620.3867196751235)
START: i = 0
START: @ 2015-09-05 08:50:06
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [  4.00000000e+02   1.56250000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 23040000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:50:06
Silent? True Debug? False
Running 23040000 time-steps:
#9-#0: 0% done, 0:00:00.680000 left
#9-#0: 0% done, 0:00:23.719996 left
#8-#0: 0% done, 3:13:24.178177 left
started <Process(Process-10, started)>
#7-#0: 0% done, 1:36:04.982951 left
#4-#0: 5% done, 0:11:21.284444 left
#6-#0: 1% done, 0:49:23.238021 left
#0-#0: 13% done, 0:05:04.023889 left
#5-#0: 2% done, 0:25:11.047222 left
#1-#0: 13% done, 0:05:26.765833 left
#2-#0: 13% done, 0:05:08.906389 left
#8-#0: 0% done, 3:13:25.576389 left
m0 = 16384.0

running iteration #10
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16384.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 5120.0)
START: i = 0
START: @ 2015-09-05 08:50:16
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [  4.00000000e+02   7.81250000e-02   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 46080000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 08:50:16
Silent? True Debug? False
Running 46080000 time-steps:
#10-#0: 0% done, 0:00:46.489998 left
#10-#0: 0% done, 0:00:00.420000 left
#3-#0: 13% done, 0:05:44.694167 left
#9-#0: 0% done, 7:30:49.194800 left
#7-#0: 0% done, 1:46:56.157552 left
#4-#0: 6% done, 0:11:11.265556 left
#0-#0: 16% done, 0:05:15.716667 left
#6-#0: 1% done, 1:00:13.771806 left
#5-#0: 3% done, 0:23:10.231667 left
#1-#0: 16% done, 0:04:55.725000 left
#8-#0: 0% done, 3:13:15.484583 left
#2-#0: 16% done, 0:05:39.691667 left
#3-#0: 16% done, 0:05:23.575000 left
#9-#0: 0% done, 6:51:08.447812 left
#4-#0: 8% done, 0:11:27.261667 left
#10-#0: 0% done, 17:02:32.342799 left
#0-#0: 19% done, 0:04:41.831667 left
#7-#0: 0% done, 2:19:47.377014 left
#5-#0: 4% done, 0:23:00.230000 left
#6-#0: 1% done, 0:52:58.355312 left
#1-#0: 19% done, 0:04:45.633889 left
#8-#0: 0% done, 3:13:16.832917 left
#2-#0: 19% done, 0:04:50.290000 left
#9-#0: 0% done, 7:07:48.853437 left
#4-#0: 9% done, 0:11:05.112500 left
#3-#0: 19% done, 0:06:16.331389 left
#0-#0: 22% done, 0:04:32.113333 left
#10-#0: 0% done, 17:26:06.265868 left
#6-#0: 2% done, 0:46:56.445625 left
#5-#0: 4% done, 0:22:50.228333 left
#1-#0: 22% done, 0:04:36.002222 left
#8-#0: 0% done, 3:12:55.310929 left
#2-#0: 22% done, 0:04:46.377778 left
#7-#0: 0% done, 2:41:48.576198 left
#4-#0: 11% done, 0:11:44.942222 left
#9-#0: 0% done, 7:41:18.184028 left
#0-#0: 25% done, 0:04:22.192500 left
#3-#0: 22% done, 0:05:34.607778 left
#10-#0: 0% done, 14:03:23.353646 left
#5-#0: 5% done, 0:22:41.511111 left
#1-#0: 25% done, 0:04:26.145000 left
#6-#0: 2% done, 0:54:54.481076 left
#8-#0: 0% done, 3:12:33.818802 left
#2-#0: 25% done, 0:04:47.895000 left
#0-#0: 27% done, 0:04:12.481667 left
#7-#0: 1% done, 2:00:43.542188 left
#9-#0: 0% done, 7:42:37.910469 left
#4-#0: 12% done, 0:12:56.615000 left
#5-#0: 6% done, 0:22:30.234375 left
#1-#0: 27% done, 0:04:16.287778 left
#10-#0: 0% done, 14:33:06.366215 left
#8-#0: 0% done, 3:12:35.117361 left
#3-#0: 25% done, 0:05:29.947500 left
#6-#0: 2% done, 0:51:43.090278 left
#2-#0: 27% done, 0:04:52.254444 left
#0-#0: 30% done, 0:04:02.944444 left
#5-#0: 6% done, 0:22:18.985694 left
#1-#0: 30% done, 0:04:06.256944 left
#4-#0: 13% done, 0:11:30.611111 left
#7-#0: 1% done, 1:53:57.374670 left
#9-#0: 0% done, 8:13:22.369219 left
#8-#0: 0% done, 3:12:25.025556 left
#10-#0: 0% done, 14:55:54.389397 left
#6-#0: 3% done, 1:01:07.706875 left
#2-#0: 30% done, 0:04:17.923611 left
#0-#0: 33% done, 0:03:53.226667 left
#3-#0: 27% done, 0:05:50.025000 left
#5-#0: 7% done, 0:22:08.993264 left
#1-#0: 33% done, 0:03:56.406667 left
#4-#0: 15% done, 0:10:32.959722 left
#7-#0: 1% done, 1:47:07.679306 left
#8-#0: 0% done, 3:12:14.933750 left
#9-#0: 0% done, 7:19:20.557435 left
#10-#0: 0% done, 15:59:17.397760 left
#0-#0: 36% done, 0:03:43.361944 left
#6-#0: 3% done, 0:49:16.858194 left
#2-#0: 33% done, 0:04:14.166667 left
#5-#0: 8% done, 0:21:59.000833 left
#1-#0: 36% done, 0:03:46.703333 left
#3-#0: 30% done, 0:05:37.256944 left
#4-#0: 16% done, 0:10:54.083333 left
#8-#0: 0% done, 3:12:16.162813 left
#7-#0: 1% done, 1:51:13.107656 left
#9-#0: 0% done, 7:19:09.081354 left
#0-#0: 38% done, 0:03:33.650556 left
#10-#0: 0% done, 14:27:12.077960 left
#2-#0: 36% done, 0:04:11.217500 left
#6-#0: 3% done, 1:00:06.943958 left
#1-#0: 38% done, 0:03:36.846667 left
#8-#0: 0% done, 3:11:54.760043 left
#4-#0: 18% done, 0:10:51.884444 left
#7-#0: 1% done, 1:39:37.244965 left
#3-#0: 33% done, 0:05:18.646667 left
#9-#0: 0% done, 7:07:54.655078 left
#0-#0: 41% done, 0:03:24.061667 left
#10-#0: 0% done, 14:14:46.046875 left
#2-#0: 38% done, 0:03:56.396111 left
#1-#0: 41% done, 0:03:26.867500 left
#5-#0: 9% done, 0:21:37.851389 left
#6-#0: 4% done, 0:53:22.462500 left
#8-#0: 1% done, 3:11:55.949479 left
#4-#0: 19% done, 0:11:01.393333 left
#7-#0: 1% done, 1:52:33.662760 left
#0-#0: 44% done, 0:03:14.233333 left
#10-#0: 0% done, 13:41:41.190176 left
#9-#0: 0% done, 8:27:39.959010 left
#2-#0: 41% done, 0:03:33.890833 left
#3-#0: 36% done, 0:05:03.900278 left
#1-#0: 44% done, 0:03:17.016667 left
#5-#0: 10% done, 0:21:30.179167 left
#8-#0: 1% done, 3:11:34.586302 left
#6-#0: 4% done, 1:01:40.277778 left
#7-#0: 2% done, 1:37:10.496875 left
#0-#0: 47% done, 0:03:04.621944 left
#4-#0: 20% done, 0:11:10.296250 left
#10-#0: 0% done, 13:23:55.225204 left
#9-#0: 0% done, 6:24:58.471806 left
#1-#0: 47% done, 0:03:07.065556 left
#5-#0: 11% done, 0:21:20.177778 left
#3-#0: 38% done, 0:04:26.890556 left
#8-#0: 1% done, 3:11:35.736181 left
#7-#0: 2% done, 1:34:59.094566 left
#0-#0: 50% done, 0:02:54.815000 left
#4-#0: 22% done, 0:09:30.554444 left
#2-#0: 44% done, 0:04:55.261111 left
#10-#0: 0% done, 12:49:21.022908 left
#9-#0: 0% done, 6:23:39.996458 left
#1-#0: 50% done, 0:02:57.400000 left
#5-#0: 11% done, 0:21:09.056319 left
#8-#0: 1% done, 3:11:25.634479 left
#6-#0: 4% done, 1:22:51.977361 left
#3-#0: 41% done, 0:04:14.759167 left
#0-#0: 52% done, 0:02:45.183333 left
#7-#0: 2% done, 1:34:48.971840 left
#4-#0: 23% done, 0:09:27.928472 left
#10-#0: 0% done, 12:59:06.905417 left
#1-#0: 52% done, 0:02:47.464167 left
#5-#0: 12% done, 0:20:59.063750 left
#9-#0: 0% done, 7:58:47.916940 left
#2-#0: 47% done, 0:03:54.871667 left
#8-#0: 1% done, 3:11:04.330556 left
#6-#0: 5% done, 0:52:01.660208 left
#0-#0: 55% done, 0:02:35.395556 left
#7-#0: 2% done, 1:35:06.168646 left
#3-#0: 44% done, 0:04:02.627778 left
#10-#0: 0% done, 12:49:01.069434 left
#1-#0: 55% done, 0:02:37.613333 left
#5-#0: 13% done, 0:20:49.071181 left
#2-#0: 50% done, 0:03:08.760000 left
#8-#0: 1% done, 3:10:54.238715 left
#6-#0: 5% done, 0:46:00.856667 left
#4-#0: 25% done, 0:14:57.397500 left
#0-#0: 58% done, 0:02:25.683333 left
#7-#0: 2% done, 1:34:28.775000 left
#10-#0: 0% done, 12:48:05.227426 left
#3-#0: 47% done, 0:03:50.396111 left
#1-#0: 58% done, 0:02:27.762500 left
#5-#0: 13% done, 0:20:39.078611 left
#9-#0: 0% done, 11:49:36.735547 left
#2-#0: 52% done, 0:02:59.477500 left
#8-#0: 1% done, 3:10:44.146875 left
#6-#0: 5% done, 0:45:50.706458 left
#0-#0: 61% done, 0:02:15.971111 left
#7-#0: 2% done, 1:34:18.652187 left
#10-#0: 0% done, 12:47:09.415326 left
#1-#0: 61% done, 0:02:17.911667 left
#5-#0: 14% done, 0:20:29.086042 left
#4-#0: 26% done, 0:12:09.015000 left
#3-#0: 50% done, 0:03:38.270000 left
#8-#0: 1% done, 3:10:34.055035 left
#9-#0: 0% done, 8:13:38.527422 left
#2-#0: 55% done, 0:03:03.426667 left
#6-#0: 6% done, 0:45:40.556250 left
#0-#0: 63% done, 0:02:06.258889 left
#7-#0: 3% done, 1:36:07.453125 left
#1-#0: 63% done, 0:02:08.060833 left
#5-#0: 15% done, 0:20:19.093472 left
#4-#0: 27% done, 0:08:59.124444 left
#10-#0: 0% done, 16:36:33.320903 left
#8-#0: 1% done, 3:10:12.839722 left
#3-#0: 52% done, 0:03:26.224167 left
#2-#0: 58% done, 0:02:38.337500 left
#6-#0: 6% done, 0:45:32.918576 left
#0-#0: 66% done, 0:01:56.546667 left
#7-#0: 3% done, 1:33:58.619306 left
#9-#0: 0% done, 10:36:56.889028 left
#1-#0: 66% done, 0:01:58.210000 left
#5-#0: 15% done, 0:20:09.100903 left
#4-#0: 29% done, 0:08:48.395417 left
#8-#0: 1% done, 3:10:24.965365 left
#2-#0: 61% done, 0:02:27.672778 left
#6-#0: 6% done, 0:45:15.277361 left
#0-#0: 69% done, 0:01:46.800833 left
#3-#0: 55% done, 0:03:14.022222 left
#7-#0: 3% done, 1:33:48.496111 left
#1-#0: 69% done, 0:01:48.359167 left
#5-#0: 16% done, 0:19:59.108333 left
#10-#0: 0% done, 23:35:37.644653 left
#4-#0: 30% done, 0:08:38.034722 left
#8-#0: 1% done, 3:10:03.779514 left
#2-#0: 63% done, 0:02:17.171667 left
#6-#0: 7% done, 0:45:12.571667 left
#0-#0: 72% done, 0:01:37.119444 left
#7-#0: 3% done, 1:33:38.372917 left
#9-#0: 0% done, 11:45:31.814132 left
#3-#0: 58% done, 0:03:01.833333 left
#1-#0: 72% done, 0:01:38.508333 left
#5-#0: 17% done, 0:19:49.115764 left
#4-#0: 31% done, 0:08:28.007500 left
#8-#0: 1% done, 3:10:04.752266 left
#2-#0: 66% done, 0:02:06.980000 left
#0-#0: 75% done, 0:01:27.407500 left
#10-#0: 0% done, 19:50:31.536328 left
#6-#0: 7% done, 0:51:06.019444 left
#7-#0: 3% done, 1:33:54.891736 left
#1-#0: 75% done, 0:01:28.680000 left
#5-#0: 18% done, 0:19:39.123194 left
#9-#0: 0% done, 7:41:46.478750 left
#3-#0: 61% done, 0:02:48.731111 left
#4-#0: 33% done, 0:08:30.440000 left
#8-#0: 2% done, 3:09:43.605625 left
#2-#0: 69% done, 0:01:57.978056 left
#0-#0: 77% done, 0:01:17.713333 left
#10-#0: 0% done, 12:49:03.440671 left
#7-#0: 3% done, 1:38:57.962309 left
#1-#0: 77% done, 0:01:18.808889 left
#5-#0: 18% done, 0:19:29.130625 left
#9-#0: 0% done, 6:20:22.121810 left
#3-#0: 63% done, 0:02:26.538889 left
#4-#0: 34% done, 0:08:29.930417 left
#8-#0: 2% done, 3:09:33.513776 left
#6-#0: 7% done, 1:15:41.897569 left
#2-#0: 72% done, 0:01:45.669444 left
#0-#0: 80% done, 0:01:07.985556 left
#10-#0: 0% done, 12:49:39.066311 left
#1-#0: 80% done, 0:01:08.957778 left
#5-#0: 19% done, 0:19:19.138056 left
#7-#0: 4% done, 1:54:39.424583 left
#9-#0: 0% done, 6:32:39.311302 left
#3-#0: 66% done, 0:02:15.226667 left
#4-#0: 36% done, 0:08:17.905278 left
#8-#0: 2% done, 3:09:34.427795 left
#0-#0: 83% done, 0:00:58.283333 left
#2-#0: 75% done, 0:01:36.340000 left
#10-#0: 0% done, 13:20:41.100365 left
#6-#0: 8% done, 1:03:59.898333 left
#1-#0: 83% done, 0:00:59.106667 left
#5-#0: 20% done, 0:19:10.063889 left
#3-#0: 69% done, 0:02:05.638333 left
#7-#0: 4% done, 1:43:59.730625 left
#8-#0: 2% done, 3:09:13.339844 left
#9-#0: 0% done, 8:14:16.913164 left
#4-#0: 37% done, 0:08:45.331250 left
#0-#0: 86% done, 0:00:48.562500 left
#10-#0: 0% done, 12:47:48.061884 left
#2-#0: 77% done, 0:01:26.897778 left
#1-#0: 86% done, 0:00:49.255556 left
#5-#0: 20% done, 0:18:59.160833 left
#3-#0: 72% done, 0:01:54.716667 left
#7-#0: 4% done, 1:41:10.854167 left
#8-#0: 2% done, 3:09:03.247986 left
#6-#0: 8% done, 1:03:28.541354 left
#0-#0: 88% done, 0:00:38.854444 left
#9-#0: 0% done, 7:18:41.414036 left
#10-#0: 0% done, 12:48:23.637663 left
#4-#0: 38% done, 0:08:33.926111 left
#2-#0: 80% done, 0:01:17.614444 left
#1-#0: 88% done, 0:00:39.404444 left
#5-#0: 21% done, 0:18:48.281458 left
#7-#0: 4% done, 1:36:01.554844 left
#3-#0: 75% done, 0:01:43.447500 left
#8-#0: 2% done, 3:08:42.208828 left
#0-#0: 91% done, 0:00:29.138333 left
#9-#0: 0% done, 7:00:25.929466 left
#10-#0: 0% done, 12:48:59.183594 left
#4-#0: 40% done, 0:07:59.903889 left
#1-#0: 91% done, 0:00:29.555833 left
#5-#0: 22% done, 0:18:41.781111 left
#2-#0: 83% done, 0:01:04.976667 left
#6-#0: 9% done, 1:12:53.243958 left
#8-#0: 2% done, 3:09:26.765781 left
#7-#0: 4% done, 1:39:30.031875 left
#0-#0: 94% done, 0:00:19.424444 left
#3-#0: 77% done, 0:01:36.113333 left
#9-#0: 1% done, 7:30:19.870312 left
#1-#0: 94% done, 0:00:19.702778 left
#4-#0: 41% done, 0:07:51.928333 left
#5-#0: 22% done, 0:18:28.342708 left
#6-#0: 9% done, 0:44:32.449688 left
#2-#0: 86% done, 0:00:54.306944 left
#10-#0: 0% done, 19:23:05.842266 left
#8-#0: 2% done, 3:08:00.276580 left
#7-#0: 5% done, 1:38:53.164653 left
#0-#0: 97% done, 0:00:09.712500 left
#3-#0: 80% done, 0:01:21.050278 left
#9-#0: 1% done, 7:08:20.542157 left
#1-#0: 97% done, 0:00:09.851667 left
#5-#0: 23% done, 0:18:18.357639 left
#4-#0: 43% done, 0:07:43.493611 left
#6-#0: 9% done, 0:49:46.127083 left
#8-#0: 2% done, 3:07:50.213889 left
#10-#0: 0% done, 16:18:19.178672 left
#2-#0: 88% done, 0:00:44.672222 left
#7-#0: 5% done, 1:39:49.601042 left
#9-#0: 1% done, 6:30:59.494575 left
#5-#0: 24% done, 0:18:08.372569 left
#3-#0: 83% done, 0:01:13.221667 left
#6-#0: 10% done, 0:44:06.224583 left
#8-#0: 2% done, 3:05:40.587214 left
#4-#0: 44% done, 0:07:49.077778 left
#10-#0: 0% done, 13:44:46.968281 left
#7-#0: 5% done, 1:39:48.944444 left
#2-#0: 91% done, 0:00:33.434167 left
#9-#0: 1% done, 6:39:26.769062 left
#5-#0: 25% done, 0:17:51.097500 left
#3-#0: 86% done, 0:00:58.580556 left
#8-#0: 2% done, 3:07:19.131719 left
#6-#0: 10% done, 0:49:54.251250 left
#4-#0: 45% done, 0:07:23.830833 left
#10-#0: 0% done, 15:35:22.334028 left
#2-#0: 94% done, 0:00:21.889444 left
#9-#0: 1% done, 6:23:09.457153 left
#5-#0: 25% done, 0:17:48.335556 left
#7-#0: 5% done, 2:23:18.222222 left
#3-#0: 88% done, 0:00:46.442222 left
#8-#0: 3% done, 3:07:19.909462 left
#6-#0: 10% done, 0:44:16.987361 left
#4-#0: 47% done, 0:06:41.966111 left
#10-#0: 0% done, 12:54:23.693325 left
#2-#0: 97% done, 0:00:10.946389 left
#9-#0: 1% done, 6:21:07.038086 left
#5-#0: 26% done, 0:17:38.351111 left
#7-#0: 5% done, 1:31:20.359479 left
#3-#0: 91% done, 0:00:34.796667 left
#8-#0: 3% done, 3:06:59.035625 left
#6-#0: 11% done, 0:44:22.577778 left
#4-#0: 48% done, 0:06:30.247222 left
#10-#0: 0% done, 13:34:24.089714 left
#5-#0: 27% done, 0:17:27.601042 left
#9-#0: 1% done, 6:47:08.060078 left
#7-#0: 5% done, 1:31:10.266736 left
#3-#0: 94% done, 0:00:23.197778 left
#8-#0: 3% done, 3:06:59.774609 left
#6-#0: 11% done, 0:43:27.020833 left
#4-#0: 50% done, 0:06:20.060000 left
#10-#0: 0% done, 12:49:31.152993 left
#5-#0: 27% done, 0:17:18.375000 left
#9-#0: 1% done, 6:19:40.353264 left
#7-#0: 6% done, 1:30:55.092726 left
#3-#0: 97% done, 0:00:11.599444 left
#8-#0: 3% done, 3:06:38.939497 left
#6-#0: 11% done, 0:43:10.076806 left
#4-#0: 51% done, 0:06:09.162500 left
#10-#0: 0% done, 12:50:51.949792 left
#5-#0: 28% done, 0:17:08.390625 left
#9-#0: 1% done, 6:19:30.331111 left
#7-#0: 6% done, 1:30:50.071875 left
#8-#0: 3% done, 3:06:39.639792 left
#6-#0: 12% done, 0:42:50.989514 left
#4-#0: 52% done, 0:05:57.330556 left
#10-#0: 0% done, 12:37:04.239746 left
#5-#0: 29% done, 0:16:58.406250 left
#9-#0: 1% done, 6:10:23.075521 left
#7-#0: 6% done, 1:30:34.935382 left
#8-#0: 3% done, 3:06:29.577222 left
#6-#0: 12% done, 0:41:43.497500 left
#4-#0: 54% done, 0:05:43.342083 left
#10-#0: 0% done, 12:27:04.078338 left
#9-#0: 1% done, 6:08:43.829861 left
#5-#0: 29% done, 0:16:48.421875 left
#7-#0: 6% done, 1:30:24.852014 left
#8-#0: 3% done, 3:06:08.800061 left
#6-#0: 12% done, 0:41:29.187917 left
#4-#0: 55% done, 0:05:32.937778 left
#10-#0: 0% done, 12:26:08.895924 left
#9-#0: 1% done, 6:08:34.083681 left
#5-#0: 30% done, 0:16:39.131944 left
#7-#0: 6% done, 1:35:20.159427 left
#6-#0: 13% done, 0:41:23.611111 left
#8-#0: 3% done, 3:06:20.137760 left
#4-#0: 56% done, 0:05:22.666944 left
#10-#0: 0% done, 12:25:59.106094 left
#9-#0: 1% done, 6:08:24.337500 left
#5-#0: 31% done, 0:16:29.140625 left
#6-#0: 13% done, 0:41:20.135104 left
#7-#0: 6% done, 1:29:55.277361 left
#8-#0: 3% done, 3:05:59.389514 left
#4-#0: 58% done, 0:05:12.258333 left
#10-#0: 0% done, 12:26:34.659234 left
#9-#0: 1% done, 6:06:45.367552 left
#5-#0: 31% done, 0:16:18.482361 left
#6-#0: 13% done, 0:41:10.174722 left
#7-#0: 7% done, 1:29:45.211545 left
#8-#0: 3% done, 3:05:59.983750 left
#4-#0: 59% done, 0:05:01.616111 left
#10-#0: 0% done, 12:26:24.859484 left
#9-#0: 1% done, 6:06:35.660729 left
#5-#0: 32% done, 0:16:07.844444 left
#6-#0: 14% done, 0:41:00.214340 left
#7-#0: 7% done, 1:29:35.145729 left
#8-#0: 3% done, 3:05:49.911562 left
#4-#0: 61% done, 0:04:51.542222 left
#10-#0: 0% done, 12:26:15.059733 left
#9-#0: 1% done, 6:06:25.953906 left
#5-#0: 33% done, 0:15:59.146667 left
#6-#0: 14% done, 0:40:48.152708 left
#7-#0: 7% done, 1:29:25.079913 left
#8-#0: 3% done, 3:05:29.221007 left
#4-#0: 62% done, 0:04:41.028750 left
#10-#0: 0% done, 12:26:05.259983 left
#9-#0: 1% done, 6:06:16.247083 left
#5-#0: 34% done, 0:15:48.528819 left
#6-#0: 14% done, 0:40:38.200868 left
#7-#0: 7% done, 1:29:15.014097 left
#4-#0: 63% done, 0:04:30.526389 left
#8-#0: 4% done, 3:05:29.757595 left
#10-#0: 0% done, 12:24:24.932936 left
#9-#0: 1% done, 6:05:44.312964 left
#5-#0: 34% done, 0:15:38.544306 left
#6-#0: 15% done, 0:40:28.249028 left
#7-#0: 7% done, 1:29:04.948281 left
#4-#0: 65% done, 0:04:20.208333 left
#8-#0: 4% done, 3:05:09.105417 left
#10-#0: 0% done, 12:25:00.396836 left
#9-#0: 1% done, 6:05:34.615964 left
#5-#0: 35% done, 0:15:28.559792 left
#6-#0: 15% done, 0:40:18.297188 left
#7-#0: 7% done, 1:28:54.882465 left
#4-#0: 66% done, 0:04:09.800000 left
#8-#0: 4% done, 3:04:59.042821 left
#10-#0: 0% done, 12:25:35.831011 left
#9-#0: 1% done, 6:06:09.295013 left
#5-#0: 36% done, 0:15:19.163056 left
#6-#0: 15% done, 0:40:08.345347 left
#4-#0: 68% done, 0:03:59.391667 left
#7-#0: 8% done, 1:28:44.816649 left
#8-#0: 4% done, 3:04:59.521927 left
#10-#0: 0% done, 12:24:40.827066 left
#9-#0: 1% done, 6:05:15.241580 left
#5-#0: 36% done, 0:15:08.597083 left
#6-#0: 16% done, 0:40:00.410208 left
#4-#0: 69% done, 0:03:48.983333 left
#7-#0: 8% done, 1:28:34.750833 left
#8-#0: 4% done, 3:04:38.927187 left
#10-#0: 0% done, 12:25:16.221621 left
#9-#0: 1% done, 6:05:27.693359 left
#5-#0: 37% done, 0:14:59.175000 left
#6-#0: 16% done, 0:39:48.450000 left
#4-#0: 70% done, 0:03:38.575000 left
#7-#0: 8% done, 1:28:24.685017 left
#8-#0: 4% done, 3:04:39.368056 left
#10-#0: 0% done, 12:25:06.421879 left
#9-#0: 1% done, 6:04:55.857361 left
#5-#0: 38% done, 0:14:48.084028 left
#6-#0: 17% done, 0:39:38.498125 left
#4-#0: 72% done, 0:03:28.111111 left
#7-#0: 8% done, 1:28:14.619201 left
#8-#0: 4% done, 3:04:18.811519 left
#10-#0: 1% done, 12:24:56.622138 left
#9-#0: 2% done, 6:04:46.160347 left
#6-#0: 17% done, 0:39:28.546250 left
#5-#0: 38% done, 0:14:39.181111 left
#4-#0: 73% done, 0:03:17.755694 left
#7-#0: 8% done, 1:28:04.553385 left
#8-#0: 4% done, 3:04:19.214219 left
#10-#0: 1% done, 12:23:16.572396 left
#9-#0: 2% done, 6:04:14.373333 left
#6-#0: 17% done, 0:39:18.594375 left
#5-#0: 39% done, 0:14:28.664792 left
#4-#0: 75% done, 0:03:07.392500 left
#7-#0: 9% done, 1:27:54.487569 left
#10-#0: 1% done, 12:25:22.108077 left
#8-#0: 4% done, 3:04:09.142075 left
#9-#0: 2% done, 6:04:26.756532 left
#6-#0: 18% done, 0:39:10.576389 left
#5-#0: 40% done, 0:14:19.193750 left
#4-#0: 76% done, 0:02:56.981806 left
#7-#0: 9% done, 1:27:39.672986 left
#10-#0: 1% done, 12:24:27.213021 left
#8-#0: 4% done, 3:03:48.642708 left
#9-#0: 2% done, 6:04:17.059523 left
#6-#0: 18% done, 0:39:00.616319 left
#5-#0: 40% done, 0:14:08.701389 left
#4-#0: 77% done, 0:02:46.571111 left
#7-#0: 9% done, 1:27:34.346875 left
#10-#0: 1% done, 12:23:32.347637 left
#9-#0: 2% done, 6:04:07.362513 left
#8-#0: 4% done, 3:03:48.988281 left
#6-#0: 18% done, 0:38:48.755000 left
#5-#0: 41% done, 0:13:59.696667 left
#4-#0: 79% done, 0:02:36.160417 left
#7-#0: 9% done, 1:27:24.281076 left
#10-#0: 1% done, 12:24:07.603655 left
#9-#0: 2% done, 6:03:57.665503 left
#8-#0: 5% done, 3:03:28.526944 left
#6-#0: 19% done, 0:38:38.803056 left
#5-#0: 42% done, 0:13:48.743472 left
#4-#0: 80% done, 0:02:25.722500 left
#7-#0: 9% done, 1:27:18.909722 left
#10-#0: 1% done, 12:23:57.803917 left
#9-#0: 2% done, 6:04:09.960686 left
#8-#0: 5% done, 3:03:49.574957 left
#6-#0: 19% done, 0:38:32.588889 left
#5-#0: 43% done, 0:13:39.225556 left
#4-#0: 81% done, 0:02:15.313750 left
#7-#0: 9% done, 1:27:04.158490 left
#9-#0: 2% done, 6:04:00.253906 left
#10-#0: 1% done, 12:23:48.004180 left
#8-#0: 5% done, 3:03:18.781354 left
#6-#0: 19% done, 0:38:22.620833 left
#5-#0: 43% done, 0:13:28.779375 left
#4-#0: 83% done, 0:02:04.945000 left
#9-#0: 2% done, 6:05:18.359644 left
#10-#0: 1% done, 12:25:53.164136 left
#7-#0: 10% done, 1:26:54.092674 left
#8-#0: 5% done, 3:02:58.376910 left
#6-#0: 20% done, 0:38:16.326389 left
#5-#0: 44% done, 0:13:19.683333 left
#4-#0: 84% done, 0:01:54.566528 left
#9-#0: 2% done, 6:03:40.879375 left
#10-#0: 1% done, 12:18:13.666701 left
#7-#0: 10% done, 1:26:44.026858 left
#8-#0: 5% done, 3:02:58.627604 left
#6-#0: 20% done, 0:38:00.879757 left
#4-#0: 86% done, 0:01:44.095833 left
#5-#0: 45% done, 0:13:09.687292 left
#9-#0: 2% done, 6:03:09.258477 left
#10-#0: 1% done, 12:21:48.671361 left
#7-#0: 10% done, 1:26:33.961042 left
#8-#0: 5% done, 3:02:48.555469 left
#6-#0: 20% done, 0:37:41.894583 left
#4-#0: 87% done, 0:01:33.663750 left
#5-#0: 45% done, 0:12:59.268750 left
#9-#0: 2% done, 6:04:49.034436 left
#10-#0: 1% done, 12:26:08.455187 left
#7-#0: 10% done, 1:26:19.290625 left
#8-#0: 5% done, 3:02:38.483333 left
#6-#0: 21% done, 0:37:40.920000 left
#4-#0: 88% done, 0:01:23.301111 left
#5-#0: 46% done, 0:12:48.866389 left
#9-#0: 2% done, 6:02:49.913108 left
#10-#0: 1% done, 12:22:58.985749 left
#7-#0: 10% done, 1:26:13.820486 left
#8-#0: 5% done, 3:02:28.411198 left
#6-#0: 21% done, 0:37:22.092639 left
#4-#0: 90% done, 0:01:12.874861 left
#5-#0: 47% done, 0:12:39.282222 left
#9-#0: 2% done, 6:02:40.216042 left
#10-#0: 1% done, 12:22:04.298203 left
#7-#0: 10% done, 1:26:03.754687 left
#8-#0: 5% done, 3:02:08.101250 left
#6-#0: 21% done, 0:37:12.171875 left
#4-#0: 91% done, 0:01:02.479167 left
#5-#0: 47% done, 0:12:29.682292 left
#9-#0: 2% done, 6:03:14.191276 left
#10-#0: 1% done, 12:24:09.112565 left
#7-#0: 11% done, 1:25:53.688889 left
#8-#0: 5% done, 3:02:08.257509 left
#6-#0: 22% done, 0:37:03.993333 left
#4-#0: 93% done, 0:00:52.055556 left
#5-#0: 48% done, 0:12:19.686528 left
#9-#0: 2% done, 6:02:42.658056 left
#10-#0: 1% done, 12:22:29.596415 left
#7-#0: 11% done, 1:25:43.623090 left
#8-#0: 5% done, 3:01:58.185382 left
#6-#0: 22% done, 0:36:50.611389 left
#4-#0: 94% done, 0:00:41.646667 left
#9-#0: 2% done, 6:03:16.545703 left
#5-#0: 49% done, 0:12:09.320694 left
#10-#0: 1% done, 12:23:49.453906 left
#7-#0: 11% done, 1:25:29.041667 left
#8-#0: 5% done, 3:01:37.931927 left
#6-#0: 22% done, 0:36:47.543333 left
#4-#0: 95% done, 0:00:31.236250 left
#9-#0: 2% done, 6:02:23.263889 left
#5-#0: 50% done, 0:11:59.330000 left
#10-#0: 1% done, 12:22:10.016667 left
#7-#0: 11% done, 1:25:18.984722 left
#6-#0: 23% done, 0:36:32.511840 left
#8-#0: 6% done, 3:01:38.031736 left
#4-#0: 97% done, 0:00:20.822500 left
#9-#0: 2% done, 6:01:51.798737 left
#5-#0: 50% done, 0:11:49.339306 left
#10-#0: 1% done, 12:21:15.427758 left
#7-#0: 11% done, 1:25:13.408056 left
#6-#0: 23% done, 0:36:24.271528 left
#8-#0: 6% done, 3:01:27.959618 left
#4-#0: 98% done, 0:00:10.411528 left
#9-#0: 2% done, 6:02:03.840547 left
#10-#0: 1% done, 12:21:05.637878 left
#5-#0: 51% done, 0:11:39.348611 left
#7-#0: 11% done, 1:24:58.879635 left
#6-#0: 23% done, 0:36:12.677708 left
#8-#0: 6% done, 3:01:07.762500 left
#9-#0: 2% done, 6:01:54.133737 left
#10-#0: 1% done, 12:20:55.847997 left
#5-#0: 52% done, 0:11:29.357917 left
#7-#0: 12% done, 1:24:48.822674 left
#6-#0: 24% done, 0:36:01.106667 left
#8-#0: 6% done, 3:00:57.699757 left
#9-#0: 2% done, 6:01:01.026788 left
#10-#0: 1% done, 12:17:47.137977 left
#5-#0: 52% done, 0:11:19.046111 left
#7-#0: 12% done, 1:24:38.765712 left
#6-#0: 24% done, 0:35:47.923264 left
#8-#0: 6% done, 3:00:47.637014 left
#9-#0: 2% done, 6:00:07.978060 left
#10-#0: 1% done, 12:17:37.387507 left
#5-#0: 53% done, 0:11:09.371875 left
#7-#0: 12% done, 1:24:33.118750 left
#6-#0: 25% done, 0:35:38.025000 left
#8-#0: 6% done, 3:00:37.574271 left
#9-#0: 3% done, 6:00:19.971328 left
#10-#0: 1% done, 12:16:42.946402 left
#5-#0: 54% done, 0:10:59.381250 left
#6-#0: 25% done, 0:35:28.126736 left
#7-#0: 12% done, 1:37:33.706163 left
#8-#0: 6% done, 3:00:27.511528 left
#9-#0: 3% done, 6:00:10.293633 left
#10-#0: 1% done, 12:17:17.876719 left
#5-#0: 54% done, 0:10:49.390625 left
#6-#0: 25% done, 0:35:18.228472 left
#7-#0: 12% done, 1:30:00.186562 left
#8-#0: 6% done, 3:00:17.448785 left
#9-#0: 3% done, 6:00:00.615937 left
#10-#0: 1% done, 12:16:23.475000 left
#5-#0: 55% done, 0:10:39.400000 left
#6-#0: 26% done, 0:35:09.905521 left
#7-#0: 13% done, 1:24:05.174375 left
#8-#0: 6% done, 3:00:07.386042 left
#9-#0: 3% done, 6:00:12.541372 left
#10-#0: 1% done, 12:17:42.997504 left
#5-#0: 56% done, 0:10:29.409375 left
#6-#0: 26% done, 0:35:01.560417 left
#7-#0: 13% done, 1:23:55.104167 left
#8-#0: 6% done, 2:59:57.323299 left
#9-#0: 3% done, 6:00:46.021528 left
#10-#0: 1% done, 12:18:17.849080 left
#5-#0: 56% done, 0:10:19.418750 left
#6-#0: 26% done, 0:34:51.647396 left
#7-#0: 13% done, 1:23:45.033958 left
#8-#0: 6% done, 2:59:47.260556 left
#9-#0: 3% done, 6:00:14.750378 left
#10-#0: 1% done, 12:17:23.486725 left
#5-#0: 57% done, 0:10:09.428125 left
#6-#0: 27% done, 0:34:41.734375 left
#7-#0: 13% done, 1:23:34.963750 left
#9-#0: 3% done, 6:00:26.598385 left
#8-#0: 7% done, 2:59:47.154766 left
#10-#0: 1% done, 12:16:29.153880 left
#5-#0: 58% done, 0:09:59.437500 left
#6-#0: 27% done, 0:34:31.821354 left
#9-#0: 3% done, 5:59:55.365920 left
#7-#0: 13% done, 1:23:24.893542 left
#8-#0: 7% done, 2:59:37.082726 left
#10-#0: 1% done, 12:14:50.297671 left
#5-#0: 59% done, 0:09:49.446875 left
#6-#0: 27% done, 0:34:21.908333 left
#9-#0: 3% done, 5:59:45.668854 left
#7-#0: 13% done, 1:23:14.823333 left
#8-#0: 7% done, 2:59:27.010686 left
#10-#0: 1% done, 12:12:26.967253 left
#6-#0: 28% done, 0:34:23.897812 left
#5-#0: 59% done, 0:09:39.456250 left
#9-#0: 3% done, 6:00:40.433051 left
#7-#0: 14% done, 1:24:00.053906 left
#8-#0: 7% done, 2:59:07.037396 left
#10-#0: 1% done, 12:15:15.320111 left
#6-#0: 28% done, 0:34:13.927292 left
#5-#0: 60% done, 0:09:29.465625 left
#9-#0: 3% done, 6:02:39.513681 left
#7-#0: 14% done, 1:24:23.765868 left
#8-#0: 7% done, 2:59:06.857344 left
#10-#0: 1% done, 12:15:05.579479 left
#6-#0: 28% done, 0:33:56.660764 left
#5-#0: 61% done, 0:09:19.692778 left
#9-#0: 3% done, 6:02:51.178203 left
#7-#0: 14% done, 1:22:49.123316 left
#8-#0: 7% done, 2:58:46.921111 left
#10-#0: 1% done, 12:17:53.735801 left
#6-#0: 29% done, 0:33:48.170833 left
#5-#0: 61% done, 0:09:09.488194 left
#9-#0: 3% done, 6:02:19.955417 left
#7-#0: 14% done, 1:22:39.043958 left
#10-#0: 1% done, 12:16:15.046688 left
#8-#0: 7% done, 2:58:36.858333 left
#6-#0: 29% done, 0:33:35.367083 left
#5-#0: 62% done, 0:08:59.497500 left
#9-#0: 3% done, 6:02:10.171458 left
#7-#0: 14% done, 1:22:24.779167 left
#10-#0: 1% done, 12:16:05.286404 left
#8-#0: 7% done, 2:58:26.795556 left
#6-#0: 29% done, 0:33:24.022361 left
#5-#0: 63% done, 0:08:49.506806 left
#9-#0: 3% done, 6:01:17.606250 left
#10-#0: 1% done, 12:14:26.695495 left
#7-#0: 14% done, 1:23:04.729167 left
#8-#0: 7% done, 2:58:06.924019 left
#6-#0: 30% done, 0:33:16.907083 left
#5-#0: 63% done, 0:08:39.703889 left
#9-#0: 3% done, 6:01:50.584280 left
#10-#0: 1% done, 12:15:01.350525 left
#7-#0: 15% done, 1:22:21.345000 left
#8-#0: 7% done, 2:57:17.709219 left
#6-#0: 30% done, 0:33:04.194444 left
#5-#0: 64% done, 0:08:25.916458 left
#9-#0: 3% done, 6:02:02.152431 left
#10-#0: 1% done, 12:19:17.856363 left
#7-#0: 15% done, 1:20:15.475556 left
#8-#0: 7% done, 2:52:43.851684 left
#6-#0: 30% done, 0:32:58.398576 left
#5-#0: 65% done, 0:08:14.434028 left
#9-#0: 3% done, 6:02:13.691706 left
#10-#0: 1% done, 12:17:39.334167 left
#7-#0: 15% done, 1:21:19.723090 left
#8-#0: 7% done, 2:55:49.153125 left
#6-#0: 31% done, 0:32:45.734375 left
#5-#0: 65% done, 0:08:09.380694 left
#9-#0: 3% done, 6:00:59.947674 left
#10-#0: 1% done, 12:16:00.870833 left
#7-#0: 15% done, 1:22:03.011250 left
#8-#0: 8% done, 2:57:36.022031 left
#6-#0: 31% done, 0:32:42.544097 left
#5-#0: 66% done, 0:07:59.553333 left
#9-#0: 3% done, 6:01:32.762088 left
#10-#0: 1% done, 12:16:35.417919 left
#7-#0: 15% done, 1:21:56.965365 left
#8-#0: 8% done, 2:57:35.676389 left
#6-#0: 31% done, 0:32:31.248056 left
#5-#0: 67% done, 0:07:49.562639 left
#9-#0: 3% done, 6:01:01.693359 left
#10-#0: 1% done, 12:16:25.638008 left
#7-#0: 15% done, 1:21:46.827292 left
#8-#0: 8% done, 2:57:15.906519 left
#6-#0: 32% done, 0:32:23.933333 left
#5-#0: 68% done, 0:07:39.571944 left
#9-#0: 3% done, 6:01:13.165339 left
#10-#0: 1% done, 12:17:44.414039 left
#7-#0: 16% done, 1:21:36.689219 left
#8-#0: 8% done, 2:57:15.524167 left
#6-#0: 32% done, 0:32:13.964444 left
#5-#0: 68% done, 0:07:29.581250 left
#9-#0: 3% done, 6:01:03.371771 left
#10-#0: 1% done, 12:16:06.097786 left
#7-#0: 16% done, 1:21:26.551146 left
#8-#0: 8% done, 2:56:55.790972 left
#6-#0: 32% done, 0:32:01.408819 left
#5-#0: 69% done, 0:07:19.590556 left
#9-#0: 4% done, 6:00:32.360664 left
#10-#0: 2% done, 12:15:12.079102 left
#7-#0: 16% done, 1:21:12.396389 left
#8-#0: 8% done, 2:56:45.728611 left
#6-#0: 33% done, 0:31:51.453333 left
#9-#0: 4% done, 5:58:57.783290 left
#5-#0: 70% done, 0:07:09.599861 left
#10-#0: 2% done, 12:13:33.870634 left
#7-#0: 16% done, 1:21:06.266667 left
#6-#0: 33% done, 0:31:40.231146 left
#8-#0: 8% done, 2:56:35.666250 left
#9-#0: 4% done, 5:58:26.858516 left
#5-#0: 70% done, 0:06:59.486667 left
#10-#0: 2% done, 12:15:36.718865 left
#6-#0: 34% done, 0:31:34.042708 left
#7-#0: 16% done, 1:20:56.128611 left
#8-#0: 8% done, 2:56:25.603889 left
#9-#0: 4% done, 5:59:20.602500 left
#5-#0: 71% done, 0:06:49.615625 left
#10-#0: 2% done, 12:14:42.758958 left
#6-#0: 34% done, 0:31:26.554687 left
#7-#0: 17% done, 1:20:45.990556 left
#8-#0: 8% done, 2:56:15.541528 left
#9-#0: 4% done, 5:58:07.415221 left
#5-#0: 72% done, 0:06:39.513889 left
#10-#0: 2% done, 12:16:01.309681 left
#6-#0: 34% done, 0:31:11.664028 left
#7-#0: 17% done, 1:20:35.852500 left
#8-#0: 8% done, 2:55:55.908854 left
#9-#0: 4% done, 5:58:18.800877 left
#5-#0: 72% done, 0:06:29.737292 left
#10-#0: 2% done, 12:15:07.379145 left
#6-#0: 35% done, 0:31:01.708368 left
#7-#0: 17% done, 1:20:21.780833 left
#8-#0: 8% done, 2:55:55.407700 left
#9-#0: 4% done, 5:57:47.952734 left
#5-#0: 73% done, 0:06:19.543472 left
#10-#0: 2% done, 12:14:13.477969 left
#6-#0: 35% done, 0:30:51.752708 left
#7-#0: 17% done, 1:20:15.568142 left
#8-#0: 9% done, 2:55:45.345347 left
#9-#0: 4% done, 5:57:59.300104 left
#5-#0: 74% done, 0:06:09.460417 left
#10-#0: 2% done, 12:14:47.809549 left
#6-#0: 35% done, 0:30:41.797049 left
#7-#0: 17% done, 1:20:01.529479 left
#8-#0: 9% done, 2:55:25.767292 left
#9-#0: 4% done, 5:58:10.618780 left
#5-#0: 75% done, 0:05:59.655000 left
#10-#0: 2% done, 12:13:09.865369 left
#6-#0: 36% done, 0:30:30.665833 left
#7-#0: 17% done, 1:19:55.283854 left
#8-#0: 9% done, 2:55:15.714028 left
#9-#0: 4% done, 5:57:18.773307 left
#5-#0: 75% done, 0:05:49.579514 left
#10-#0: 2% done, 12:13:44.167604 left
#6-#0: 36% done, 0:30:20.716563 left
#7-#0: 18% done, 1:19:45.145833 left
#8-#0: 9% done, 2:55:15.140148 left
#9-#0: 4% done, 5:57:30.063307 left
#10-#0: 2% done, 12:13:34.397483 left
#5-#0: 76% done, 0:05:39.671806 left
#6-#0: 36% done, 0:30:10.767292 left
#7-#0: 18% done, 1:19:35.007813 left
#8-#0: 9% done, 2:54:55.616562 left
#9-#0: 4% done, 5:56:59.310764 left
#10-#0: 2% done, 12:12:40.603889 left
#5-#0: 77% done, 0:05:29.605833 left
#6-#0: 37% done, 0:30:00.818021 left
#7-#0: 18% done, 1:19:24.869792 left
#8-#0: 9% done, 2:54:55.006424 left
#9-#0: 4% done, 5:56:49.574714 left
#10-#0: 2% done, 12:12:30.843542 left
#5-#0: 77% done, 0:05:19.617778 left
#6-#0: 37% done, 0:29:50.868750 left
#7-#0: 18% done, 1:19:14.731771 left
#8-#0: 9% done, 2:54:35.519063 left
#9-#0: 4% done, 5:56:18.869896 left
#10-#0: 2% done, 12:13:05.067578 left
#5-#0: 78% done, 0:05:09.629722 left
#6-#0: 37% done, 0:29:40.919479 left
#9-#0: 4% done, 5:56:51.042769 left
#7-#0: 18% done, 1:19:04.593750 left
#8-#0: 9% done, 2:54:34.872734 left
#10-#0: 2% done, 12:13:39.262307 left
#5-#0: 79% done, 0:04:59.641667 left
#6-#0: 38% done, 0:29:32.070347 left
#9-#0: 4% done, 5:56:41.297188 left
#7-#0: 18% done, 1:18:54.455729 left
#8-#0: 9% done, 2:54:24.810417 left
#10-#0: 2% done, 12:12:45.537109 left
#5-#0: 79% done, 0:04:49.653611 left
#6-#0: 38% done, 0:29:22.114896 left
#9-#0: 4% done, 5:56:31.551606 left
#7-#0: 19% done, 1:18:44.317708 left
#8-#0: 9% done, 2:54:14.748099 left
#10-#0: 2% done, 12:13:19.692773 left
#5-#0: 80% done, 0:04:39.665556 left
#6-#0: 38% done, 0:29:11.083889 left
#9-#0: 4% done, 5:55:19.128472 left
#8-#0: 9% done, 2:54:04.685781 left
#7-#0: 19% done, 1:18:30.425781 left
#10-#0: 2% done, 12:11:42.100373 left
#5-#0: 81% done, 0:04:29.626875 left
#6-#0: 39% done, 0:29:02.197917 left
#9-#0: 4% done, 5:53:45.917552 left
#8-#0: 9% done, 2:53:54.623464 left
#7-#0: 19% done, 1:18:24.033611 left
#10-#0: 2% done, 12:10:48.453275 left
#5-#0: 81% done, 0:04:19.734583 left
#6-#0: 39% done, 0:28:51.191250 left
#9-#0: 4% done, 5:53:57.093056 left
#8-#0: 10% done, 2:53:35.244340 left
#7-#0: 19% done, 1:18:13.895608 left
#10-#0: 2% done, 12:12:50.304340 left
#5-#0: 82% done, 0:04:09.701389 left
#6-#0: 39% done, 0:28:41.241875 left
#9-#0: 4% done, 5:53:26.569180 left
#8-#0: 10% done, 2:53:34.489844 left
#7-#0: 19% done, 1:18:03.757604 left
#10-#0: 2% done, 12:11:12.829047 left
#6-#0: 40% done, 0:28:31.292500 left
#5-#0: 83% done, 0:03:59.673333 left
#9-#0: 4% done, 5:53:16.890234 left
#8-#0: 10% done, 2:53:24.427535 left
#7-#0: 19% done, 1:17:53.619601 left
#10-#0: 2% done, 12:11:03.068685 left
#6-#0: 40% done, 0:28:21.343125 left
#5-#0: 84% done, 0:03:49.760417 left
#9-#0: 4% done, 5:53:48.806089 left
#8-#0: 10% done, 2:53:14.365226 left
#7-#0: 20% done, 1:17:43.481597 left
#10-#0: 2% done, 12:10:53.308322 left
#6-#0: 40% done, 0:28:10.390278 left
#5-#0: 84% done, 0:03:39.737222 left
#9-#0: 5% done, 5:53:18.329740 left
#10-#0: 2% done, 12:11:27.337161 left
#8-#0: 10% done, 2:53:04.302917 left
#7-#0: 20% done, 1:17:37.001250 left
#6-#0: 41% done, 0:28:01.438507 left
#5-#0: 85% done, 0:03:29.718542 left
#9-#0: 5% done, 5:52:47.881875 left
#10-#0: 2% done, 12:11:17.567051 left
#8-#0: 10% done, 2:52:45.013516 left
#7-#0: 20% done, 1:17:23.213542 left
#6-#0: 41% done, 0:27:51.489167 left
#5-#0: 86% done, 0:03:19.759722 left
#9-#0: 5% done, 5:52:58.943351 left
#10-#0: 2% done, 12:10:24.046723 left
#8-#0: 10% done, 2:52:44.169358 left
#7-#0: 20% done, 1:17:13.075521 left
#6-#0: 42% done, 0:27:41.539826 left
#5-#0: 86% done, 0:03:09.796806 left
#9-#0: 5% done, 5:52:49.254905 left
#10-#0: 2% done, 12:10:58.017088 left
#8-#0: 10% done, 2:52:34.107057 left
#7-#0: 20% done, 1:17:02.937500 left
#6-#0: 42% done, 0:27:31.590486 left
#5-#0: 87% done, 0:02:59.762500 left
#9-#0: 5% done, 5:52:39.566458 left
#10-#0: 2% done, 12:10:04.535729 left
#8-#0: 10% done, 2:52:14.871285 left
#7-#0: 21% done, 1:16:52.799479 left
#6-#0: 42% done, 0:27:21.641146 left
#5-#0: 88% done, 0:02:49.795764 left
#9-#0: 5% done, 5:52:09.194466 left
#10-#0: 2% done, 12:09:54.775360 left
#8-#0: 10% done, 2:52:13.973542 left
#7-#0: 21% done, 1:16:42.661458 left
#6-#0: 43% done, 0:27:11.691806 left
#5-#0: 88% done, 0:02:39.825556 left
#9-#0: 5% done, 5:52:20.180095 left
#10-#0: 2% done, 12:11:56.031892 left
#8-#0: 10% done, 2:51:54.773437 left
#7-#0: 21% done, 1:16:32.523437 left
#6-#0: 43% done, 0:27:02.665000 left
#9-#0: 5% done, 5:52:10.491654 left
#5-#0: 89% done, 0:02:29.820833 left
#10-#0: 2% done, 12:09:35.283822 left
#8-#0: 11% done, 2:51:53.840061 left
#7-#0: 21% done, 1:16:22.385417 left
#6-#0: 43% done, 0:26:50.887500 left
#9-#0: 5% done, 5:51:40.176476 left
#5-#0: 90% done, 0:02:19.832778 left
#10-#0: 2% done, 12:10:09.156814 left
#8-#0: 11% done, 2:51:43.777778 left
#6-#0: 44% done, 0:26:41.843785 left
#7-#0: 21% done, 1:16:15.778663 left
#9-#0: 5% done, 5:51:51.105313 left
#5-#0: 90% done, 0:02:09.844722 left
#10-#0: 2% done, 12:09:59.386708 left
#6-#0: 44% done, 0:26:31.894444 left
#8-#0: 11% done, 2:51:33.715495 left
#7-#0: 21% done, 1:15:58.601562 left
#9-#0: 5% done, 5:51:41.416875 left
#5-#0: 91% done, 0:01:59.856667 left
#10-#0: 2% done, 12:09:49.616602 left
#6-#0: 44% done, 0:26:23.700729 left
#8-#0: 11% done, 2:51:23.653212 left
#7-#0: 22% done, 1:15:55.471372 left
#9-#0: 5% done, 5:51:31.728438 left
#5-#0: 92% done, 0:01:49.868611 left
#10-#0: 2% done, 12:10:23.421497 left
#6-#0: 45% done, 0:26:11.139931 left
#8-#0: 11% done, 2:51:04.541962 left
#7-#0: 22% done, 1:15:41.841111 left
#9-#0: 5% done, 5:52:03.142222 left
#5-#0: 93% done, 0:01:39.873611 left
#10-#0: 2% done, 12:10:13.641667 left
#6-#0: 45% done, 0:26:00.340139 left
#8-#0: 11% done, 2:51:03.519792 left
#7-#0: 22% done, 1:15:31.703073 left
#9-#0: 5% done, 5:54:37.692708 left
#5-#0: 93% done, 0:01:29.891875 left
#10-#0: 2% done, 12:08:36.789609 left
#6-#0: 45% done, 0:25:48.711667 left
#8-#0: 11% done, 2:50:44.443967 left
#7-#0: 22% done, 1:15:21.565035 left
#9-#0: 5% done, 5:54:48.423212 left
#5-#0: 94% done, 0:01:19.903889 left
#10-#0: 2% done, 12:12:04.612595 left
#6-#0: 46% done, 0:25:41.286632 left
#8-#0: 11% done, 2:50:34.390538 left
#7-#0: 22% done, 1:15:14.864931 left
#9-#0: 5% done, 5:54:38.630929 left
#5-#0: 95% done, 0:01:09.915903 left
#10-#0: 2% done, 12:10:27.809132 left
#6-#0: 46% done, 0:25:33.813264 left
#8-#0: 11% done, 2:50:42.293516 left
#9-#0: 5% done, 5:54:08.363021 left
#7-#0: 22% done, 1:15:01.296667 left
#5-#0: 95% done, 0:00:59.927917 left
#10-#0: 2% done, 12:09:34.541771 left
#6-#0: 46% done, 0:25:24.666250 left
#8-#0: 11% done, 2:50:23.261875 left
#9-#0: 5% done, 5:54:19.036940 left
#7-#0: 23% done, 1:14:51.158611 left
#5-#0: 96% done, 0:00:49.939931 left
#10-#0: 2% done, 12:08:41.303548 left
#6-#0: 47% done, 0:25:12.294444 left
#9-#0: 5% done, 5:53:28.368793 left
#8-#0: 11% done, 2:50:13.199609 left
#7-#0: 23% done, 1:14:41.020556 left
#5-#0: 97% done, 0:00:39.951944 left
#10-#0: 2% done, 12:12:08.728260 left
#6-#0: 47% done, 0:25:07.095347 left
#9-#0: 5% done, 5:54:40.271758 left
#8-#0: 11% done, 2:50:03.137344 left
#7-#0: 23% done, 1:14:30.882500 left
#5-#0: 97% done, 0:00:29.963958 left
#10-#0: 2% done, 12:09:05.231387 left
#6-#0: 47% done, 0:24:53.989583 left
#9-#0: 5% done, 5:53:08.859549 left
#8-#0: 12% done, 2:49:53.075078 left
#7-#0: 23% done, 1:14:24.105556 left
#5-#0: 98% done, 0:00:19.975972 left
#10-#0: 2% done, 12:09:38.851684 left
#6-#0: 48% done, 0:24:42.487917 left
#9-#0: 5% done, 5:53:19.467552 left
#8-#0: 12% done, 2:49:34.122674 left
#7-#0: 23% done, 1:14:10.614010 left
#5-#0: 99% done, 0:00:09.987986 left
#10-#0: 2% done, 12:10:55.823600 left
#6-#0: 48% done, 0:24:38.622778 left
#9-#0: 5% done, 5:55:32.223281 left
#8-#0: 12% done, 2:49:32.941771 left
#7-#0: 23% done, 1:14:00.475938 left
#10-#0: 2% done, 12:10:02.653320 left
#6-#0: 48% done, 0:24:22.629583 left
#9-#0: 6% done, 5:52:19.279883 left
#8-#0: 12% done, 2:46:08.068750 left
#7-#0: 24% done, 1:12:30.767448 left
#10-#0: 3% done, 12:08:26.170211 left
#6-#0: 49% done, 0:24:13.419861 left
#9-#0: 6% done, 5:53:10.490990 left
#8-#0: 12% done, 2:44:20.986780 left
#7-#0: 24% done, 1:11:51.108958 left
#10-#0: 3% done, 12:08:59.712891 left
#6-#0: 49% done, 0:24:03.464931 left
#9-#0: 6% done, 5:52:40.392422 left
#8-#0: 12% done, 2:45:04.133750 left
#7-#0: 24% done, 1:12:00.932031 left
#10-#0: 3% done, 12:07:23.317051 left
#6-#0: 50% done, 0:23:53.510000 left
#9-#0: 6% done, 5:52:10.322014 left
#8-#0: 12% done, 2:44:01.493168 left
#7-#0: 24% done, 1:11:28.108368 left
#10-#0: 3% done, 12:07:13.546888 left
#6-#0: 50% done, 0:23:44.265104 left
#9-#0: 6% done, 5:53:41.892279 left
#8-#0: 12% done, 2:43:51.720087 left
#7-#0: 24% done, 1:11:24.737969 left
#10-#0: 3% done, 12:11:23.362988 left
#6-#0: 50% done, 0:23:35.005347 left
#9-#0: 6% done, 5:52:11.071875 left
#8-#0: 12% done, 2:43:41.947005 left
#7-#0: 25% done, 1:11:08.362500 left
#10-#0: 3% done, 12:06:54.064687 left
#6-#0: 51% done, 0:23:23.659896 left
#9-#0: 6% done, 5:51:41.057717 left
#8-#0: 12% done, 2:43:32.173924 left
#7-#0: 25% done, 1:11:01.707049 left
#10-#0: 3% done, 12:07:27.520139 left
#6-#0: 51% done, 0:23:13.024306 left
#9-#0: 6% done, 5:51:31.284184 left
#8-#0: 12% done, 2:44:23.529861 left
#7-#0: 25% done, 1:11:17.499653 left
#10-#0: 3% done, 12:07:17.740278 left
#6-#0: 51% done, 0:23:03.074132 left
#9-#0: 6% done, 5:51:21.510651 left
#8-#0: 13% done, 2:43:30.119271 left
#7-#0: 25% done, 1:10:54.771354 left
#10-#0: 3% done, 12:05:41.586628 left
#6-#0: 52% done, 0:22:53.123958 left
#9-#0: 6% done, 5:50:51.562049 left
#8-#0: 13% done, 2:43:02.932882 left
#7-#0: 25% done, 1:10:32.132361 left
#10-#0: 3% done, 12:08:24.496267 left
#6-#0: 52% done, 0:22:44.477187 left
#9-#0: 6% done, 5:52:02.423307 left
#8-#0: 13% done, 2:43:53.923611 left
#7-#0: 25% done, 1:10:44.402240 left
#10-#0: 3% done, 12:07:31.548874 left
#6-#0: 52% done, 0:22:35.801944 left
#9-#0: 6% done, 5:51:52.621719 left
#8-#0: 13% done, 2:42:43.447266 left
#7-#0: 26% done, 1:10:12.407812 left
#10-#0: 3% done, 12:06:38.630508 left
#6-#0: 53% done, 0:22:23.301563 left
#9-#0: 6% done, 5:50:22.344280 left
#8-#0: 13% done, 2:42:33.674045 left
#7-#0: 26% done, 1:10:02.519531 left
#10-#0: 3% done, 12:05:02.631682 left
#6-#0: 53% done, 0:22:12.727708 left
#9-#0: 6% done, 5:50:32.680347 left
#8-#0: 13% done, 2:42:23.900825 left
#7-#0: 26% done, 1:09:52.631250 left
#10-#0: 3% done, 12:05:35.961302 left
#6-#0: 53% done, 0:22:03.396181 left
#9-#0: 6% done, 5:50:22.906797 left
#8-#0: 13% done, 2:42:05.516354 left
#7-#0: 26% done, 1:09:42.742969 left
#10-#0: 3% done, 12:06:09.261914 left
#6-#0: 54% done, 0:21:54.655833 left
#9-#0: 6% done, 5:50:33.196181 left
#8-#0: 13% done, 2:42:04.345747 left
#7-#0: 26% done, 1:09:32.854687 left
#10-#0: 3% done, 12:08:08.636458 left
#6-#0: 54% done, 0:21:45.292187 left
#9-#0: 6% done, 5:51:23.546124 left
#8-#0: 13% done, 2:41:54.572535 left
#7-#0: 26% done, 1:09:22.966406 left
#10-#0: 3% done, 12:05:06.699054 left
#6-#0: 54% done, 0:21:33.567708 left
#9-#0: 6% done, 5:49:53.632760 left
#8-#0: 13% done, 2:42:36.156042 left
#7-#0: 27% done, 1:09:31.453125 left
#10-#0: 3% done, 12:04:56.928854 left
#6-#0: 55% done, 0:21:23.617188 left
#9-#0: 6% done, 5:49:23.852205 left
#8-#0: 13% done, 2:41:35.077778 left
#7-#0: 27% done, 1:09:03.233490 left
#10-#0: 3% done, 12:04:47.158655 left
#6-#0: 55% done, 0:21:14.235556 left
#9-#0: 6% done, 5:51:14.018056 left
#8-#0: 13% done, 2:41:25.304514 left
#7-#0: 27% done, 1:08:53.345104 left
#10-#0: 3% done, 12:08:12.259332 left
#6-#0: 55% done, 0:21:04.840625 left
#9-#0: 6% done, 5:49:24.349284 left
#8-#0: 14% done, 2:41:07.023438 left
#7-#0: 27% done, 1:08:43.456719 left
#10-#0: 3% done, 12:05:53.576257 left
#6-#0: 56% done, 0:20:53.227500 left
#9-#0: 6% done, 5:49:34.526806 left
#8-#0: 14% done, 2:41:14.240035 left
#7-#0: 27% done, 1:08:30.563889 left
#10-#0: 3% done, 12:05:00.851181 left
#6-#0: 56% done, 0:20:43.281250 left
#9-#0: 6% done, 5:49:44.676415 left
#8-#0: 14% done, 2:40:55.984722 left
#7-#0: 27% done, 1:08:23.672743 left
#10-#0: 3% done, 12:04:51.071313 left
#6-#0: 56% done, 0:20:32.801111 left
#9-#0: 7% done, 5:49:14.970312 left
#8-#0: 14% done, 2:40:46.211458 left
#7-#0: 28% done, 1:08:16.760000 left
#10-#0: 3% done, 12:05:24.188398 left
#6-#0: 57% done, 0:20:23.384479 left
#9-#0: 7% done, 5:48:25.396780 left
#8-#0: 14% done, 2:41:01.755799 left
#7-#0: 28% done, 1:08:12.786979 left
#10-#0: 3% done, 12:04:31.521224 left
#6-#0: 57% done, 0:20:12.921458 left
#9-#0: 7% done, 5:48:35.509201 left
#8-#0: 14% done, 2:41:25.645226 left
#7-#0: 28% done, 1:08:11.717917 left
#10-#0: 3% done, 12:03:38.882986 left
#6-#0: 57% done, 0:20:03.487847 left
#9-#0: 7% done, 5:47:46.019271 left
#8-#0: 14% done, 2:40:16.977083 left
#7-#0: 28% done, 1:07:44.190625 left
#10-#0: 3% done, 12:02:46.273678 left
#6-#0: 58% done, 0:19:52.541667 left
#9-#0: 7% done, 5:47:16.424635 left
#8-#0: 14% done, 2:39:58.815807 left
#7-#0: 28% done, 1:07:34.302083 left
#10-#0: 3% done, 12:02:36.513095 left
#6-#0: 58% done, 0:19:42.603819 left
#9-#0: 7% done, 5:46:27.036788 left
#8-#0: 14% done, 2:39:57.421858 left
#7-#0: 28% done, 1:07:24.413542 left
#10-#0: 3% done, 12:02:26.752511 left
#6-#0: 59% done, 0:19:32.665972 left
#9-#0: 7% done, 5:46:56.914583 left
#8-#0: 14% done, 2:39:47.648516 left
#7-#0: 29% done, 1:07:11.635000 left
#10-#0: 3% done, 12:02:16.991927 left
#6-#0: 59% done, 0:19:23.203438 left
#9-#0: 7% done, 5:46:07.600911 left
#8-#0: 14% done, 2:39:29.538368 left
#7-#0: 29% done, 1:07:04.629392 left
#10-#0: 3% done, 12:02:49.993325 left
#6-#0: 59% done, 0:19:13.261528 left
#9-#0: 7% done, 5:46:17.639132 left
#8-#0: 15% done, 2:39:28.093333 left
#7-#0: 29% done, 1:06:54.740868 left
#10-#0: 3% done, 12:01:57.480391 left
#6-#0: 60% done, 0:19:02.860417 left
#9-#0: 7% done, 5:46:07.902656 left
#8-#0: 15% done, 2:39:18.320000 left
#7-#0: 29% done, 1:06:44.852344 left
#10-#0: 3% done, 12:03:13.166719 left
#6-#0: 60% done, 0:18:53.825000 left
#9-#0: 7% done, 5:45:58.166181 left
#8-#0: 15% done, 2:39:58.261719 left
#7-#0: 29% done, 1:06:54.799097 left
#10-#0: 3% done, 12:04:28.795278 left
#6-#0: 60% done, 0:18:43.879167 left
#9-#0: 7% done, 5:46:08.139605 left
#8-#0: 15% done, 2:38:50.555278 left
#7-#0: 30% done, 1:06:25.124271 left
#10-#0: 3% done, 12:02:53.626280 left
#6-#0: 61% done, 0:18:33.933333 left
#9-#0: 7% done, 5:45:38.702474 left
#8-#0: 15% done, 2:38:57.294271 left
#7-#0: 30% done, 1:06:15.235625 left
#10-#0: 3% done, 12:03:26.512135 left
#6-#0: 61% done, 0:18:23.987500 left
#9-#0: 7% done, 5:46:27.984757 left
#8-#0: 15% done, 2:38:31.042361 left
#7-#0: 30% done, 1:06:05.346979 left
#10-#0: 3% done, 12:07:32.601428 left
#6-#0: 61% done, 0:18:16.982639 left
#9-#0: 7% done, 5:46:57.529444 left
#8-#0: 15% done, 2:39:27.022500 left
#7-#0: 30% done, 1:06:14.902778 left
#10-#0: 3% done, 12:03:49.618125 left
#6-#0: 62% done, 0:18:04.534861 left
#9-#0: 7% done, 5:45:09.566888 left
#8-#0: 15% done, 2:38:27.982500 left
#7-#0: 30% done, 1:05:48.382083 left
#10-#0: 3% done, 12:02:14.603047 left
#6-#0: 62% done, 0:17:53.775000 left
#9-#0: 7% done, 5:44:40.212856 left
#8-#0: 15% done, 2:38:10.016241 left
#7-#0: 30% done, 1:06:22.487500 left
#10-#0: 3% done, 12:02:04.823177 left
#6-#0: 62% done, 0:17:44.230174 left
#9-#0: 7% done, 5:44:50.084635 left
#8-#0: 15% done, 2:38:57.415538 left
#7-#0: 31% done, 1:05:50.591111 left
#10-#0: 3% done, 12:01:55.043307 left
#6-#0: 63% done, 0:17:33.893958 left
#9-#0: 7% done, 5:44:20.767500 left
#8-#0: 15% done, 2:37:50.528229 left
#7-#0: 31% done, 1:05:16.137500 left
#10-#0: 3% done, 12:01:45.263437 left
#6-#0: 63% done, 0:17:23.951563 left
#9-#0: 7% done, 5:44:11.040208 left
#8-#0: 15% done, 2:37:40.754722 left
#7-#0: 31% done, 1:05:06.248264 left
#10-#0: 3% done, 12:00:52.952472 left
#6-#0: 63% done, 0:17:14.009167 left
#9-#0: 7% done, 5:43:41.769149 left
#8-#0: 16% done, 2:37:22.864123 left
#7-#0: 31% done, 1:04:59.054097 left
#10-#0: 3% done, 12:00:43.182209 left
#6-#0: 64% done, 0:17:03.698403 left
#9-#0: 7% done, 5:43:51.576419 left
#8-#0: 16% done, 2:37:53.600573 left
#7-#0: 31% done, 1:04:46.476615 left
#10-#0: 3% done, 12:01:15.904622 left
#6-#0: 64% done, 0:16:54.482083 left
#9-#0: 7% done, 5:44:01.356076 left
#8-#0: 16% done, 2:37:27.626432 left
#7-#0: 31% done, 1:04:39.255139 left
#10-#0: 3% done, 12:01:06.124757 left
#6-#0: 64% done, 0:16:44.181979 left
#9-#0: 8% done, 5:43:32.131042 left
#8-#0: 16% done, 2:37:25.902951 left
#7-#0: 32% done, 1:04:26.704896 left
#10-#0: 4% done, 12:00:13.890618 left
#6-#0: 65% done, 0:16:34.586806 left
#9-#0: 8% done, 5:43:22.403750 left
#8-#0: 16% done, 2:37:24.154375 left
#7-#0: 32% done, 1:04:16.815625 left
#10-#0: 4% done, 12:01:28.990508 left
#6-#0: 65% done, 0:16:25.321562 left
#9-#0: 8% done, 5:44:30.483559 left
#8-#0: 16% done, 2:37:14.347361 left
#7-#0: 32% done, 1:05:23.112274 left
#10-#0: 4% done, 12:02:44.032823 left
#6-#0: 65% done, 0:16:15.035347 left
#9-#0: 8% done, 5:44:40.152917 left
#8-#0: 16% done, 2:36:56.523672 left
#7-#0: 32% done, 1:03:49.391597 left
#10-#0: 4% done, 12:01:09.430773 left
#6-#0: 66% done, 0:16:05.086007 left
#9-#0: 8% done, 5:44:49.794727 left
#8-#0: 16% done, 2:36:46.725000 left
#7-#0: 32% done, 1:03:36.921875 left
#10-#0: 4% done, 12:00:17.263789 left
#6-#0: 66% done, 0:15:55.136667 left
#9-#0: 8% done, 5:43:22.425582 left
#8-#0: 16% done, 2:36:44.909670 left
#7-#0: 32% done, 1:03:29.645764 left
#10-#0: 4% done, 12:00:07.483915 left
#6-#0: 67% done, 0:15:44.873958 left
#9-#0: 8% done, 5:43:32.058212 left
#8-#0: 16% done, 2:36:35.102674 left
#7-#0: 33% done, 1:03:22.349566 left
#10-#0: 4% done, 11:58:33.025703 left
#6-#0: 67% done, 0:15:34.621111 left
#9-#0: 8% done, 5:42:43.583333 left
#8-#0: 16% done, 2:36:17.345599 left
#7-#0: 33% done, 1:03:04.793333 left
#10-#0: 4% done, 11:58:23.265000 left
#6-#0: 67% done, 0:15:24.678333 left
#9-#0: 8% done, 5:42:53.188459 left
#8-#0: 17% done, 2:38:22.415938 left
#7-#0: 33% done, 1:02:54.937101 left
#10-#0: 4% done, 11:58:55.805132 left
#6-#0: 68% done, 0:15:14.735556 left
#9-#0: 8% done, 5:44:20.059505 left
#8-#0: 17% done, 2:36:05.806033 left
#7-#0: 33% done, 1:02:45.080868 left
#10-#0: 4% done, 12:00:10.598199 left
#6-#0: 68% done, 0:15:04.792778 left
#9-#0: 8% done, 5:43:50.962969 left
#8-#0: 17% done, 2:40:24.609531 left
#7-#0: 33% done, 1:02:35.224635 left
#10-#0: 4% done, 11:59:18.546237 left
#6-#0: 68% done, 0:14:55.131250 left
#9-#0: 8% done, 5:44:19.754097 left
#8-#0: 17% done, 2:35:46.473047 left
#7-#0: 34% done, 1:02:25.368403 left
#10-#0: 4% done, 11:59:08.766372 left
#6-#0: 69% done, 0:14:44.910313 left
#9-#0: 8% done, 5:43:50.685013 left
#8-#0: 17% done, 2:35:36.665625 left
#7-#0: 34% done, 1:02:18.005938 left
#10-#0: 4% done, 11:58:58.986506 left
#6-#0: 69% done, 0:14:34.967500 left
#9-#0: 8% done, 5:43:21.643359 left
#8-#0: 17% done, 2:35:26.858203 left
#7-#0: 34% done, 1:02:05.662500 left
#10-#0: 4% done, 11:58:07.001562 left
#6-#0: 69% done, 0:14:25.287500 left
#9-#0: 8% done, 5:42:52.629123 left
#8-#0: 17% done, 2:35:17.050781 left
#7-#0: 34% done, 1:01:55.806250 left
#10-#0: 4% done, 11:58:39.417207 left
#6-#0: 70% done, 0:14:15.084861 left
#9-#0: 8% done, 5:43:21.283125 left
#8-#0: 17% done, 2:35:07.243359 left
#7-#0: 34% done, 1:01:48.404444 left
#10-#0: 4% done, 11:58:29.637344 left
#6-#0: 70% done, 0:14:05.392882 left
#9-#0: 8% done, 5:43:11.491641 left
#8-#0: 17% done, 2:34:57.435937 left
#7-#0: 34% done, 1:01:36.100260 left
