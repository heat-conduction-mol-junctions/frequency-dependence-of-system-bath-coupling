nprocs = 11
chunk-size = 1
started <Process(Process-1, started)>
m0 = 16.0

running iteration #0
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 160.0)
START: i = 0
START: @ 2015-09-05 09:23:50
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   80.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:23:50
Silent? True Debug? False
Running 12000 time-steps:
#0-#0: 0% done, 0:00:00.919923 left
#0-#0: 0% done, 0:00:00.919847 left
m0 = 32.0

running iteration #1
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 32.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 226.27416997969522)
START: i = 0
START: @ 2015-09-05 09:23:51
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   40.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:23:51
Silent? True Debug? False
Running 12000 time-steps:
#1-#0: 0% done, 0:00:00.739938 left
#1-#0: 0% done, 0:00:00.739877 left
started <Process(Process-2, started)>
#0-#0: 10% done, 0:00:15.894000 left
#1-#0: 10% done, 0:00:16.704000 left
#0-#0: 20% done, 0:00:14.192000 left
m0 = 64.0

running iteration #2
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 64.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 320.0)
START: i = 0
START: @ 2015-09-05 09:23:54
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:23:54
Silent? True Debug? False
Running 12000 time-steps:
#2-#0: 0% done, 0:00:00.829923 left
#2-#0: 0% done, 0:00:00.739877 left
started <Process(Process-3, started)>
#1-#0: 20% done, 0:00:14.912000 left
#0-#0: 30% done, 0:00:13.055000 left
#2-#0: 10% done, 0:00:16.785000 left
#1-#0: 30% done, 0:00:12.950000 left
#0-#0: 40% done, 0:00:09.462000 left
#2-#0: 20% done, 0:00:14.280000 left
m0 = 128.0

running iteration #3
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 09:23:58
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 24000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:23:58
Silent? True Debug? False
Running 24000 time-steps:
#3-#0: 0% done, 0:00:00.719970 left
#3-#0: 0% done, 0:00:00.719940 left
started <Process(Process-4, started)>
#0-#0: 50% done, 0:00:07.610000 left
#1-#0: 40% done, 0:00:11.064000 left
#2-#0: 30% done, 0:00:12.495000 left
#0-#0: 60% done, 0:00:06.040000 left
#1-#0: 50% done, 0:00:09.245000 left
#2-#0: 40% done, 0:00:10.710000 left
#0-#0: 70% done, 0:00:04.962000 left
#1-#0: 60% done, 0:00:07.236000 left
m0 = 256.0

running iteration #4
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 256.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 640.0)
START: i = 0
START: @ 2015-09-05 09:24:03
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.    5.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 48000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:24:03
Silent? True Debug? False
Running 48000 time-steps:
#4-#0: 0% done, 0:00:00.819981 left
#4-#0: 0% done, 0:00:00.729970 left
started <Process(Process-5, started)>
#2-#0: 50% done, 0:00:08.925000 left
#3-#0: 10% done, 0:00:47.385000 left
#0-#0: 80% done, 0:00:03.304000 left
#1-#0: 70% done, 0:00:05.445000 left
#2-#0: 60% done, 0:00:07.140000 left
#0-#0: 90% done, 0:00:01.659000 left
#1-#0: 80% done, 0:00:03.630000 left
#2-#0: 70% done, 0:00:05.355000 left
#1-#0: 90% done, 0:00:01.816000 left
m0 = 512.0

running iteration #5
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 512.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 905.0966799187809)
START: i = 0
START: @ 2015-09-05 09:24:09
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.     2.5   20. ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 96000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:24:09
Silent? True Debug? False
Running 96000 time-steps:
#5-#0: 0% done, 0:00:00.709993 left
#5-#0: 0% done, 0:00:00.709985 left
started <Process(Process-6, started)>
#2-#0: 80% done, 0:00:03.570000 left
#3-#0: 20% done, 0:00:42.120000 left
#4-#0: 10% done, 0:01:03.999000 left
#2-#0: 90% done, 0:00:01.785000 left
#3-#0: 30% done, 0:00:37.002000 left
m0 = 1024.0

running iteration #6
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 1024.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1280.0)
START: i = 0
START: @ 2015-09-05 09:24:16
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.      1.25   20.  ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 192000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:24:16
Silent? True Debug? False
Running 192000 time-steps:
#6-#0: 0% done, 0:00:00.709996 left
#6-#0: 0% done, 0:00:00.901989 left
started <Process(Process-7, started)>
#4-#0: 20% done, 0:00:58.296000 left
#3-#0: 40% done, 0:00:31.716000 left
m0 = 2048.0

running iteration #7
b'compute-0-99\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 2048.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1810.1933598375617)
START: i = 0
START: @ 2015-09-05 09:24:24
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.       0.625   20.   ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 384000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:24:24
Silent? True Debug? False
Running 384000 time-steps:
#7-#0: 0% done, 0:00:00.789998 left
#7-#0: 0% done, 0:00:00.789996 left
#5-#0: 10% done, 0:02:01.167000 left
started <Process(Process-8, started)>
