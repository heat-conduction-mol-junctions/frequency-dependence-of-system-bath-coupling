nprocs = 11
chunk-size = 1
m0 = 16.0

running iteration #0
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 160.0)
START: i = 0
START: @ 2015-09-05 09:25:02
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   80.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:02
Silent? True Debug? False
Running 12000 time-steps:
#0-#0: 0% done, 0:00:00.399967 left
#0-#0: 0% done, 0:00:00.499900 left
started <Process(Process-1, started)>
#0-#0: 10% done, 0:00:09.522000 left
m0 = 32.0

running iteration #1
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 32.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 226.27416997969522)
START: i = 0
START: @ 2015-09-05 09:25:03
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   40.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:03
Silent? True Debug? False
Running 12000 time-steps:
#1-#0: 0% done, 0:00:00.379968 left
#1-#0: 0% done, 0:00:00.379937 left
#0-#0: 20% done, 0:00:08.720000 left
started <Process(Process-2, started)>
#1-#0: 10% done, 0:00:09.819000 left
#0-#0: 30% done, 0:00:07.630000 left
#1-#0: 20% done, 0:00:08.728000 left
#0-#0: 40% done, 0:00:06.576000 left
m0 = 64.0

running iteration #2
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 64.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 320.0)
START: i = 0
START: @ 2015-09-05 09:25:07
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:07
Silent? True Debug? False
Running 12000 time-steps:
#2-#0: 0% done, 0:00:00.379968 left
#2-#0: 0% done, 0:00:00.379937 left
#1-#0: 30% done, 0:00:07.686000 left
started <Process(Process-3, started)>
#0-#0: 50% done, 0:00:05.455000 left
#2-#0: 10% done, 0:00:09.900000 left
#1-#0: 40% done, 0:00:06.552000 left
#0-#0: 60% done, 0:00:04.364000 left
#2-#0: 20% done, 0:00:08.736000 left
#1-#0: 50% done, 0:00:05.460000 left
#0-#0: 70% done, 0:00:03.273000 left
#2-#0: 30% done, 0:00:07.644000 left
m0 = 128.0

running iteration #3
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 09:25:11
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 24000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:11
Silent? True Debug? False
Running 24000 time-steps:
#3-#0: 0% done, 0:00:00.489975 left
#3-#0: 0% done, 0:00:00.399967 left
#1-#0: 60% done, 0:00:04.400000 left
#0-#0: 80% done, 0:00:02.190000 left
started <Process(Process-4, started)>
#2-#0: 40% done, 0:00:06.516000 left
#1-#0: 70% done, 0:00:03.291000 left
#0-#0: 90% done, 0:00:01.093000 left
#2-#0: 50% done, 0:00:05.455000 left
#3-#0: 10% done, 0:00:19.395000 left
#1-#0: 80% done, 0:00:02.194000 left
#2-#0: 60% done, 0:00:04.348000 left
#1-#0: 90% done, 0:00:01.097000 left
#2-#0: 70% done, 0:00:03.270000 left
#3-#0: 20% done, 0:00:17.240000 left
m0 = 256.0

running iteration #4
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 256.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 640.0)
START: i = 0
START: @ 2015-09-05 09:25:16
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.    5.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 48000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:16
Silent? True Debug? False
Running 48000 time-steps:
#4-#0: 0% done, 0:00:00.389992 left
#4-#0: 0% done, 0:00:00.389984 left
#2-#0: 80% done, 0:00:02.180000 left
started <Process(Process-5, started)>
#2-#0: 90% done, 0:00:01.089000 left
#3-#0: 30% done, 0:00:15.085000 left
#3-#0: 40% done, 0:00:12.930000 left
#4-#0: 10% done, 0:00:38.340000 left
m0 = 512.0

running iteration #5
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 512.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 905.0966799187809)
START: i = 0
START: @ 2015-09-05 09:25:22
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.     2.5   20. ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 96000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:22
Silent? True Debug? False
Running 96000 time-steps:
#5-#0: 0% done, 0:00:00.389996 left
#5-#0: 0% done, 0:00:00.389992 left
started <Process(Process-6, started)>
#3-#0: 50% done, 0:00:10.750000 left
#3-#0: 60% done, 0:00:08.600000 left
#4-#0: 20% done, 0:00:34.208000 left
#3-#0: 70% done, 0:00:06.450000 left
m0 = 1024.0

running iteration #6
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 1024.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1280.0)
START: i = 0
START: @ 2015-09-05 09:25:29
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.      1.25   20.  ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 192000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:29
Silent? True Debug? False
Running 192000 time-steps:
#6-#0: 0% done, 0:00:00.389998 left
#6-#0: 0% done, 0:00:00.389996 left
started <Process(Process-7, started)>
#3-#0: 80% done, 0:00:04.304000 left
#4-#0: 30% done, 0:00:29.932000 left
#5-#0: 10% done, 0:01:15.681000 left
#3-#0: 90% done, 0:00:02.151000 left
#4-#0: 40% done, 0:00:25.692000 left
m0 = 2048.0

running iteration #7
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 2048.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1810.1933598375617)
START: i = 0
START: @ 2015-09-05 09:25:37
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.       0.625   20.   ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 384000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:37
Silent? True Debug? False
Running 384000 time-steps:
#7-#0: 0% done, 0:00:00.399999 left
#7-#0: 0% done, 0:00:00.399998 left
started <Process(Process-8, started)>
#6-#0: 5% done, 0:02:48.232458 left
#4-#0: 50% done, 0:00:21.435000 left
#5-#0: 20% done, 0:01:07.784000 left
#4-#0: 60% done, 0:00:17.116000 left
m0 = 4096.0

running iteration #8
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 4096.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 2560.0)
START: i = 0
START: @ 2015-09-05 09:25:46
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   3.12500000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 768000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:46
Silent? True Debug? False
Running 768000 time-steps:
#8-#0: 0% done, 0:00:00.509999 left
#8-#0: 0% done, 0:00:00.509999 left
started <Process(Process-9, started)>
#7-#0: 2% done, 0:05:59.550354 left
#6-#0: 10% done, 0:02:38.680750 left
#4-#0: 70% done, 0:00:12.864000 left
#5-#0: 30% done, 0:00:59.262000 left
#4-#0: 80% done, 0:00:08.572000 left
#8-#0: 1% done, 0:12:15.167224 left
m0 = 8192.0

running iteration #9
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 8192.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 3620.3867196751235)
START: i = 0
START: @ 2015-09-05 09:25:56
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   1.56250000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 1536000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:25:56
Silent? True Debug? False
Running 1536000 time-steps:
#9-#0: 0% done, 0:00:00.400000 left
#9-#0: 0% done, 0:00:00.399999 left
started <Process(Process-10, started)>
#7-#0: 5% done, 0:05:49.591667 left
#6-#0: 15% done, 0:02:30.548625 left
#4-#0: 90% done, 0:00:04.296000 left
#5-#0: 40% done, 0:00:50.832000 left
#8-#0: 2% done, 0:12:19.310344 left
#9-#0: 0% done, 0:25:17.978565 left
#7-#0: 7% done, 0:05:44.556313 left
m0 = 16384.0

running iteration #10
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16384.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 5120.0)
START: i = 0
START: @ 2015-09-05 09:26:07
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   7.81250000e-02   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 3072000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:26:07
Silent? True Debug? False
Running 3072000 time-steps:
#10-#0: 0% done, 0:00:00.420000 left
#10-#0: 0% done, 0:00:00.420000 left
#6-#0: 20% done, 0:02:22.819833 left
#5-#0: 50% done, 0:00:42.335000 left
#8-#0: 3% done, 0:12:12.972375 left
#9-#0: 1% done, 0:25:25.986255 left
#7-#0: 10% done, 0:05:37.596583 left
#10-#0: 0% done, 0:51:13.815413 left
#6-#0: 26% done, 0:02:14.263958 left
#5-#0: 60% done, 0:00:33.868000 left
#8-#0: 5% done, 0:12:04.420667 left
#9-#0: 1% done, 0:25:17.396969 left
#7-#0: 13% done, 0:05:27.782729 left
#10-#0: 0% done, 0:51:15.905346 left
#5-#0: 70% done, 0:00:25.410000 left
#6-#0: 31% done, 0:02:04.899500 left
#8-#0: 6% done, 0:11:54.469833 left
#9-#0: 2% done, 0:25:05.864250 left
#7-#0: 15% done, 0:05:17.968875 left
#5-#0: 80% done, 0:00:17.072000 left
#10-#0: 0% done, 0:51:08.839312 left
#6-#0: 36% done, 0:01:55.282375 left
#8-#0: 7% done, 0:11:44.519000 left
#9-#0: 3% done, 0:24:57.235940 left
#5-#0: 90% done, 0:00:08.502000 left
#7-#0: 18% done, 0:05:08.155021 left
#10-#0: 1% done, 0:50:58.751083 left
#6-#0: 41% done, 0:01:46.225000 left
#8-#0: 9% done, 0:11:34.568167 left
#9-#0: 3% done, 0:24:45.741984 left
#7-#0: 20% done, 0:04:58.100500 left
#10-#0: 1% done, 0:50:48.662854 left
#6-#0: 46% done, 0:01:36.740625 left
#8-#0: 10% done, 0:11:24.001000 left
#9-#0: 4% done, 0:24:37.075172 left
#7-#0: 23% done, 0:04:48.744750 left
#10-#0: 1% done, 0:50:38.574625 left
#6-#0: 52% done, 0:01:27.124000 left
#8-#0: 11% done, 0:11:14.059125 left
#9-#0: 5% done, 0:24:25.619458 left
#7-#0: 26% done, 0:04:38.503417 left
#10-#0: 2% done, 0:50:28.486396 left
#6-#0: 57% done, 0:01:17.618979 left
#8-#0: 13% done, 0:10:57.726021 left
#9-#0: 5% done, 0:24:00.579383 left
#7-#0: 28% done, 0:04:26.546344 left
#10-#0: 2% done, 0:49:37.601000 left
#6-#0: 62% done, 0:01:07.775250 left
#8-#0: 14% done, 0:10:38.295984 left
#9-#0: 6% done, 0:23:26.451682 left
#7-#0: 31% done, 0:04:16.636875 left
#10-#0: 2% done, 0:48:44.229574 left
#6-#0: 67% done, 0:00:58.081729 left
#8-#0: 15% done, 0:10:27.501937 left
#9-#0: 7% done, 0:23:16.657451 left
#7-#0: 33% done, 0:04:03.891594 left
#10-#0: 3% done, 0:48:34.423305 left
#6-#0: 72% done, 0:00:48.699625 left
