nprocs = 11
chunk-size = 1
Current Working Directory: /a/home/cc/tree/taucc/students/chemist/inonshar/FrequencyDependenceOfSystemBathCoupling
m0 = 16.0

running iteration #0
b'power.tau.ac.il\n' cls.isCloudCompute? False
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from resources/FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.15
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 160.0)
START: i = 0
START: @ 2015-09-15 16:18:04
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 180.0
frequencies = [ 400.   80.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 180000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-15 16:18:04
Silent? True Debug? False
Running 180000 time-steps:
#0-#0: 0% done, 0:00:00.759996 left
#0-#0: 0% done, 0:00:00.759992 left
started <Process(Process-1, started)>
started <Process(Process-2, started)>
Current Working Directory: /a/home/cc/tree/taucc/students/chemist/inonshar/FrequencyDependenceOfSystemBathCoupling
m0 = 32.0

running iteration #1
