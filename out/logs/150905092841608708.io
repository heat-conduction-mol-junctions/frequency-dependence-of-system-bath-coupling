nprocs = 11
chunk-size = 1
m0 = 16.0

running iteration #0
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 160.0)
START: i = 0
START: @ 2015-09-05 09:28:42
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.   80.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 3600
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:28:42
Silent? True Debug? False
Running 3600 time-steps:
#0-#0: 0% done, 0:00:00.469869 left
#0-#0: 0% done, 0:00:00.469739 left
started <Process(Process-1, started)>
#0-#0: 10% done, 0:00:03.825000 left
#0-#0: 20% done, 0:00:03.464000 left
#0-#0: 30% done, 0:00:02.933000 left
#0-#0: 40% done, 0:00:02.550000 left
m0 = 32.0

running iteration #1
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 32.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 226.27416997969522)
START: i = 0
START: @ 2015-09-05 09:28:44
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.   40.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 3600
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:28:44
Silent? True Debug? False
Running 3600 time-steps:
#1-#0: 0% done, 0:00:00.449875 left
#1-#0: 0% done, 0:00:00.449750 left
#0-#0: 50% done, 0:00:02.125000 left
#1-#0: 10% done, 0:00:03.240000 left
started <Process(Process-2, started)>
#0-#0: 60% done, 0:00:01.700000 left
#1-#0: 20% done, 0:00:02.880000 left
#0-#0: 70% done, 0:00:01.275000 left
#1-#0: 30% done, 0:00:02.520000 left
#1-#0: 40% done, 0:00:02.160000 left
#0-#0: 80% done, 0:00:00.846000 left
#1-#0: 50% done, 0:00:01.800000 left
#0-#0: 90% done, 0:00:00.424000 left
#1-#0: 60% done, 0:00:01.440000 left
#1-#0: 70% done, 0:00:01.080000 left
#1-#0: 80% done, 0:00:00.720000 left
m0 = 64.0

running iteration #2
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 64.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 320.0)
START: i = 0
START: @ 2015-09-05 09:28:47
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 3600
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:28:47
Silent? True Debug? False
Running 3600 time-steps:
#2-#0: 0% done, 0:00:00.549819 left
#2-#0: 0% done, 0:00:00.459744 left
#1-#0: 90% done, 0:00:00.360000 left
#2-#0: 10% done, 0:00:03.816000 left
started <Process(Process-3, started)>
#2-#0: 20% done, 0:00:03.392000 left
#2-#0: 30% done, 0:00:02.968000 left
#2-#0: 40% done, 0:00:02.544000 left
#2-#0: 50% done, 0:00:02.120000 left
#2-#0: 60% done, 0:00:01.696000 left
#2-#0: 70% done, 0:00:01.272000 left
#2-#0: 80% done, 0:00:00.848000 left
#2-#0: 90% done, 0:00:00.425000 left
m0 = 128.0

running iteration #3
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-09-05 09:28:51
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 7200
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:28:51
Silent? True Debug? False
Running 7200 time-steps:
#3-#0: 0% done, 0:00:00.549910 left
#3-#0: 0% done, 0:00:00.459872 left
started <Process(Process-4, started)>
#3-#0: 10% done, 0:00:07.137000 left
#3-#0: 20% done, 0:00:06.472000 left
#3-#0: 30% done, 0:00:05.614000 left
#3-#0: 40% done, 0:00:04.812000 left
#3-#0: 50% done, 0:00:04.010000 left
m0 = 256.0

running iteration #4
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 256.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 640.0)
START: i = 0
START: @ 2015-09-05 09:28:56
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.   20.    5.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 14400
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:28:56
Silent? True Debug? False
Running 14400 time-steps:
#4-#0: 0% done, 0:00:00.459968 left
#4-#0: 0% done, 0:00:00.459936 left
#3-#0: 60% done, 0:00:03.208000 left
started <Process(Process-5, started)>
#3-#0: 70% done, 0:00:02.406000 left
#4-#0: 10% done, 0:00:14.022000 left
#3-#0: 80% done, 0:00:01.604000 left
#3-#0: 90% done, 0:00:00.802000 left
#4-#0: 20% done, 0:00:12.400000 left
#4-#0: 30% done, 0:00:10.850000 left
m0 = 512.0

running iteration #5
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 512.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 905.0966799187809)
START: i = 0
START: @ 2015-09-05 09:29:02
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.     2.5   20. ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 28800
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:02
Silent? True Debug? False
Running 28800 time-steps:
#5-#0: 0% done, 0:00:00.459984 left
#5-#0: 0% done, 0:00:00.459968 left
started <Process(Process-6, started)>
#4-#0: 40% done, 0:00:09.300000 left
#4-#0: 50% done, 0:00:07.725000 left
#5-#0: 10% done, 0:00:27.549000 left
#4-#0: 60% done, 0:00:06.196000 left
#4-#0: 70% done, 0:00:04.647000 left
#5-#0: 20% done, 0:00:24.424000 left
m0 = 1024.0

running iteration #6
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 1024.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1280.0)
START: i = 0
START: @ 2015-09-05 09:29:09
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.      1.25   20.  ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 57600
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:09
Silent? True Debug? False
Running 57600 time-steps:
#6-#0: 0% done, 0:00:00.449992 left
#6-#0: 0% done, 0:00:00.449984 left
#4-#0: 80% done, 0:00:03.098000 left
started <Process(Process-7, started)>
#4-#0: 90% done, 0:00:01.549000 left
#5-#0: 30% done, 0:00:21.420000 left
#5-#0: 40% done, 0:00:18.360000 left
#6-#0: 10% done, 0:00:54.189000 left
m0 = 2048.0

running iteration #7
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 2048.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1810.1933598375617)
START: i = 0
START: @ 2015-09-05 09:29:17
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [ 400.       0.625   20.   ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 115200
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:17
Silent? True Debug? False
Running 115200 time-steps:
#7-#0: 0% done, 0:00:00.895191 left
#7-#0: 0% done, 0:00:00.789986 left
started <Process(Process-8, started)>
#5-#0: 50% done, 0:00:15.625000 left
#5-#0: 60% done, 0:00:11.396000 left
#6-#0: 20% done, 0:00:46.696000 left
#5-#0: 70% done, 0:00:08.547000 left
m0 = 4096.0

running iteration #8
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 4096.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 2560.0)
START: i = 0
START: @ 2015-09-05 09:29:26
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [  4.00000000e+02   3.12500000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 230400
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:26
Silent? True Debug? False
Running 230400 time-steps:
#8-#0: 0% done, 0:00:00.399998 left
#8-#0: 0% done, 0:00:00.399997 left
started <Process(Process-9, started)>
#5-#0: 80% done, 0:00:05.702000 left
#7-#0: 8% done, 0:01:38.038364 left
#6-#0: 30% done, 0:00:36.890000 left
#5-#0: 90% done, 0:00:02.850000 left
#6-#0: 40% done, 0:00:31.548000 left
#8-#0: 4% done, 0:03:26.578318 left
m0 = 8192.0

running iteration #9
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 8192.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 3620.3867196751235)
START: i = 0
START: @ 2015-09-05 09:29:36
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [  4.00000000e+02   1.56250000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 460800
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:36
Silent? True Debug? False
Running 460800 time-steps:
#9-#0: 0% done, 0:00:00.389999 left
#9-#0: 0% done, 0:00:00.389998 left
started <Process(Process-10, started)>
#7-#0: 17% done, 0:01:25.729583 left
#6-#0: 50% done, 0:00:26.365000 left
#6-#0: 60% done, 0:00:21.028000 left
#8-#0: 8% done, 0:03:16.052619 left
#9-#0: 2% done, 0:07:10.373125 left
#7-#0: 26% done, 0:01:16.787387 left
m0 = 16384.0

running iteration #10
b'compute-0-102\n' cls.isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16384.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.003
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 5120.0)
START: i = 0
START: @ 2015-09-05 09:29:47
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 3.6
frequencies = [  4.00000000e+02   7.81250000e-02   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 921600
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-09-05 09:29:47
Silent? True Debug? False
Running 921600 time-steps:
#10-#0: 0% done, 0:00:00.440000 left
#10-#0: 0% done, 0:00:00.439999 left
#6-#0: 70% done, 0:00:15.816000 left
#8-#0: 13% done, 0:03:08.128979 left
#9-#0: 4% done, 0:07:03.356258 left
#6-#0: 80% done, 0:00:10.540000 left
#10-#0: 1% done, 0:14:39.601020 left
#7-#0: 34% done, 0:01:07.971139 left
#6-#0: 90% done, 0:00:05.270000 left
#8-#0: 17% done, 0:02:57.797239 left
#9-#0: 6% done, 0:06:51.738222 left
#10-#0: 2% done, 0:14:30.834074 left
#7-#0: 43% done, 0:00:58.932424 left
#8-#0: 21% done, 0:02:47.752895 left
#9-#0: 8% done, 0:06:41.796424 left
#10-#0: 3% done, 0:14:14.274698 left
#7-#0: 52% done, 0:00:49.735008 left
#8-#0: 26% done, 0:02:38.201904 left
#9-#0: 10% done, 0:06:32.614255 left
#10-#0: 4% done, 0:14:05.536667 left
#7-#0: 60% done, 0:00:40.707308 left
#8-#0: 30% done, 0:02:29.029422 left
#9-#0: 13% done, 0:06:20.616658 left
#10-#0: 5% done, 0:13:54.297109 left
#7-#0: 69% done, 0:00:31.701267 left
#8-#0: 34% done, 0:02:19.738311 left
#9-#0: 15% done, 0:06:11.120235 left
#10-#0: 6% done, 0:13:46.336103 left
#7-#0: 78% done, 0:00:22.695225 left
#8-#0: 39% done, 0:02:10.361644 left
#9-#0: 17% done, 0:06:01.938500 left
#10-#0: 7% done, 0:13:35.958470 left
#7-#0: 86% done, 0:00:13.691189 left
#8-#0: 43% done, 0:02:01.150429 left
#9-#0: 19% done, 0:05:52.732191 left
#10-#0: 8% done, 0:13:24.839906 left
#7-#0: 95% done, 0:00:04.683593 left
#8-#0: 47% done, 0:01:51.419331 left
#9-#0: 21% done, 0:05:42.936955 left
#10-#0: 9% done, 0:13:07.022414 left
#8-#0: 52% done, 0:01:41.160133 left
#9-#0: 23% done, 0:05:33.432050 left
#10-#0: 10% done, 0:12:48.036601 left
#8-#0: 56% done, 0:01:32.084579 left
#9-#0: 26% done, 0:05:23.675096 left
#10-#0: 11% done, 0:12:37.259082 left
#8-#0: 60% done, 0:01:22.806400 left
#9-#0: 28% done, 0:05:15.127489 left
#10-#0: 13% done, 0:12:27.928635 left
#8-#0: 65% done, 0:01:13.702513 left
#10-#0: 14% done, 0:12:20.638001 left
#9-#0: 30% done, 0:05:04.931260 left
#8-#0: 69% done, 0:01:04.535533 left
#10-#0: 15% done, 0:12:09.956051 left
#9-#0: 32% done, 0:04:55.216294 left
#8-#0: 73% done, 0:00:55.352720 left
#10-#0: 16% done, 0:12:01.908826 left
#9-#0: 34% done, 0:04:45.717700 left
#8-#0: 78% done, 0:00:46.221438 left
#10-#0: 17% done, 0:11:52.552828 left
#9-#0: 36% done, 0:04:36.219106 left
#8-#0: 82% done, 0:00:37.036349 left
#10-#0: 18% done, 0:11:43.196830 left
#9-#0: 39% done, 0:04:26.720512 left
#8-#0: 86% done, 0:00:27.868936 left
#10-#0: 19% done, 0:11:33.244075 left
#9-#0: 41% done, 0:04:17.381061 left
#8-#0: 91% done, 0:00:18.699717 left
#10-#0: 20% done, 0:11:23.315353 left
#9-#0: 43% done, 0:04:07.876590 left
#8-#0: 95% done, 0:00:09.532719 left
#10-#0: 21% done, 0:11:13.410337 left
#9-#0: 45% done, 0:03:58.099114 left
#8-#0: 99% done, 0:00:00.366644 left
#10-#0: 22% done, 0:11:05.177057 left
#9-#0: 47% done, 0:03:48.731363 left
#10-#0: 23% done, 0:10:54.227079 left
#9-#0: 49% done, 0:03:36.458144 left
#10-#0: 24% done, 0:10:45.421292 left
#9-#0: 52% done, 0:03:26.867942 left
#10-#0: 26% done, 0:10:35.080800 left
#9-#0: 54% done, 0:03:17.402489 left
#10-#0: 27% done, 0:10:28.210384 left
#9-#0: 56% done, 0:03:08.388050 left
#10-#0: 28% done, 0:10:19.331398 left
#9-#0: 58% done, 0:02:59.164181 left
#10-#0: 29% done, 0:10:09.048880 left
#9-#0: 60% done, 0:02:49.561208 left
#10-#0: 30% done, 0:09:58.808566 left
#9-#0: 62% done, 0:02:39.992897 left
#10-#0: 31% done, 0:09:48.609806 left
#9-#0: 65% done, 0:02:30.625631 left
#10-#0: 32% done, 0:09:41.805999 left
#9-#0: 67% done, 0:02:21.554468 left
#10-#0: 33% done, 0:09:31.634435 left
#9-#0: 69% done, 0:02:12.081522 left
#10-#0: 34% done, 0:09:22.287894 left
#9-#0: 71% done, 0:02:02.700732 left
#10-#0: 35% done, 0:09:12.181826 left
#9-#0: 73% done, 0:01:53.224938 left
#10-#0: 36% done, 0:09:02.481089 left
#9-#0: 75% done, 0:01:43.931939 left
#10-#0: 37% done, 0:08:52.799177 left
#9-#0: 78% done, 0:01:34.485650 left
#10-#0: 39% done, 0:08:43.820212 left
#9-#0: 80% done, 0:01:25.129966 left
#10-#0: 40% done, 0:08:34.492930 left
#9-#0: 82% done, 0:01:15.754419 left
#10-#0: 41% done, 0:08:25.802215 left
#9-#0: 84% done, 0:01:06.378872 left
#10-#0: 42% done, 0:08:15.849900 left
#9-#0: 86% done, 0:00:57.003325 left
#10-#0: 43% done, 0:08:06.817611 left
#9-#0: 88% done, 0:00:47.633378 left
#10-#0: 44% done, 0:07:57.484451 left
#9-#0: 91% done, 0:00:38.253117 left
#10-#0: 45% done, 0:07:48.151292 left
#9-#0: 93% done, 0:00:28.877353 left
#10-#0: 46% done, 0:07:38.555903 left
#9-#0: 95% done, 0:00:19.501589 left
#10-#0: 47% done, 0:07:28.976408 left
#9-#0: 97% done, 0:00:10.125825 left
#10-#0: 48% done, 0:07:21.584420 left
#9-#0: 99% done, 0:00:00.750061 left
#10-#0: 49% done, 0:07:10.371277 left
#10-#0: 50% done, 0:07:01.047810 left
#10-#0: 52% done, 0:06:51.512742 left
#10-#0: 53% done, 0:06:42.194065 left
#10-#0: 54% done, 0:06:33.261124 left
#10-#0: 55% done, 0:06:23.565645 left
#10-#0: 56% done, 0:06:14.246752 left
#10-#0: 57% done, 0:06:05.094255 left
#10-#0: 58% done, 0:05:55.771113 left
#10-#0: 59% done, 0:05:46.298137 left
#10-#0: 60% done, 0:05:37.120904 left
#10-#0: 61% done, 0:05:27.663731 left
#10-#0: 62% done, 0:05:18.471130 left
#10-#0: 64% done, 0:05:08.909579 left
#10-#0: 65% done, 0:04:59.818300 left
#10-#0: 66% done, 0:04:50.706300 left
#10-#0: 67% done, 0:04:41.278129 left
#10-#0: 68% done, 0:04:32.136459 left
#10-#0: 69% done, 0:04:22.803933 left
#10-#0: 70% done, 0:04:13.471407 left
#10-#0: 71% done, 0:04:04.287394 left
#10-#0: 72% done, 0:03:54.880503 left
#10-#0: 73% done, 0:03:45.798374 left
#10-#0: 74% done, 0:03:36.568818 left
#10-#0: 75% done, 0:03:26.844845 left
#10-#0: 77% done, 0:03:17.510691 left
#10-#0: 78% done, 0:03:08.220638 left
#10-#0: 79% done, 0:02:58.764796 left
#10-#0: 80% done, 0:02:49.398908 left
#10-#0: 81% done, 0:02:40.102726 left
#10-#0: 82% done, 0:02:30.829402 left
#10-#0: 83% done, 0:02:21.446024 left
#10-#0: 84% done, 0:02:12.181080 left
#10-#0: 85% done, 0:02:02.846258 left
#10-#0: 86% done, 0:01:53.495392 left
#10-#0: 87% done, 0:01:44.229459 left
#10-#0: 88% done, 0:01:34.845099 left
#10-#0: 90% done, 0:01:25.528160 left
#10-#0: 91% done, 0:01:16.198250 left
#10-#0: 92% done, 0:01:06.860229 left
#10-#0: 93% done, 0:00:57.518091 left
#10-#0: 94% done, 0:00:48.183628 left
#10-#0: 95% done, 0:00:38.845715 left
#10-#0: 96% done, 0:00:29.507803 left
#10-#0: 97% done, 0:00:20.170397 left
#10-#0: 98% done, 0:00:10.832104 left
#10-#0: 99% done, 0:00:01.494086 left
