nprocs = 11
chunk-size = 1
m0 = 16.0

running iteration #0
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 160.0)
START: i = 0
START: @ 2015-08-10 23:18:33
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   80.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:33
Silent? True Debug? False
Running 12000 time-steps:
#0-#0: 0% done, 0:00:00.759937 left
#0-#0: 0% done, 0:00:00.859840 left
started <Process(Process-1, started)>
m0 = 32.0

running iteration #1
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 32.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 226.27416997969522)
START: i = 0
START: @ 2015-08-10 23:18:35
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   40.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:35
Silent? True Debug? False
Running 12000 time-steps:
#1-#0: 0% done, 0:00:00.969911 left
#1-#0: 0% done, 0:00:00.879853 left
started <Process(Process-2, started)>
#0-#0: 10% done, 0:00:18.918000 left
#1-#0: 10% done, 0:00:12.780000 left
#0-#0: 20% done, 0:00:13.936000 left
m0 = 64.0

running iteration #2
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 64.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 320.0)
START: i = 0
START: @ 2015-08-10 23:18:38
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 12000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:38
Silent? True Debug? False
Running 12000 time-steps:
#2-#0: 0% done, 0:00:00.879927 left
#2-#0: 0% done, 0:00:00.879853 left
started <Process(Process-3, started)>
#1-#0: 20% done, 0:00:12.640000 left
#0-#0: 30% done, 0:00:10.430000 left
#0-#0: 40% done, 0:00:08.400000 left
#1-#0: 30% done, 0:00:12.481000 left
#2-#0: 10% done, 0:00:22.257000 left
#0-#0: 50% done, 0:00:07.775000 left
started <Process(Process-4, started)>
m0 = 128.0

running iteration #3
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 128.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 452.54833995939043)
START: i = 0
START: @ 2015-08-10 23:18:42
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   10.   20.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 24000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:42
Silent? True Debug? False
Running 24000 time-steps:
#3-#0: 0% done, 0:00:00.649973 left
#3-#0: 0% done, 0:00:00.649946 left
#1-#0: 40% done, 0:00:10.662000 left
#0-#0: 60% done, 0:00:05.740000 left
#2-#0: 20% done, 0:00:19.784000 left
#1-#0: 50% done, 0:00:08.910000 left
#0-#0: 70% done, 0:00:04.476000 left
#0-#0: 80% done, 0:00:02.984000 left
#3-#0: 10% done, 0:00:30.798000 left
#2-#0: 30% done, 0:00:17.262000 left
#1-#0: 60% done, 0:00:07.112000 left
m0 = 256.0

running iteration #4
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 256.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 640.0)
START: i = 0
START: @ 2015-08-10 23:18:47
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.   20.    5.]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 48000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:47
Silent? True Debug? False
Running 48000 time-steps:
#4-#0: 0% done, 0:00:00.739983 left
#4-#0: 0% done, 0:00:00.649973 left
started <Process(Process-5, started)>
#0-#0: 90% done, 0:00:01.492000 left
#1-#0: 70% done, 0:00:05.343000 left
#2-#0: 40% done, 0:00:14.868000 left
#3-#0: 20% done, 0:00:27.184000 left
#1-#0: 80% done, 0:00:03.558000 left
#2-#0: 50% done, 0:00:12.340000 left
#1-#0: 90% done, 0:00:01.796000 left
m0 = 512.0

running iteration #5
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 512.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 905.0966799187809)
START: i = 0
START: @ 2015-08-10 23:18:53
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.     2.5   20. ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 96000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:18:53
Silent? True Debug? False
Running 96000 time-steps:
#5-#0: 0% done, 0:00:00.939989 left
#5-#0: 0% done, 0:00:00.849982 left
started <Process(Process-6, started)>
#4-#0: 10% done, 0:00:51.777000 left
#3-#0: 30% done, 0:00:23.835000 left
#2-#0: 60% done, 0:00:09.888000 left
#2-#0: 70% done, 0:00:07.416000 left
#3-#0: 40% done, 0:00:20.394000 left
#4-#0: 20% done, 0:00:37.896000 left
#2-#0: 80% done, 0:00:04.940000 left
started <Process(Process-7, started)>
m0 = 1024.0

running iteration #6
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 1024.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1280.0)
START: i = 0
START: @ 2015-08-10 23:19:00
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.      1.25   20.  ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 192000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:19:00
Silent? True Debug? False
Running 192000 time-steps:
#6-#0: 0% done, 0:00:01.041994 left
#6-#0: 0% done, 0:00:00.859991 left
#3-#0: 50% done, 0:00:17.520000 left
#2-#0: 90% done, 0:00:02.471000 left
#4-#0: 30% done, 0:00:43.253000 left
#3-#0: 60% done, 0:00:15.120000 left
m0 = 2048.0

running iteration #7
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 2048.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 1810.1933598375617)
START: i = 0
START: @ 2015-08-10 23:19:08
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [ 400.       0.625   20.   ]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 384000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:19:08
Silent? True Debug? False
Running 384000 time-steps:
#7-#0: 0% done, 0:00:00.679998 left
#7-#0: 0% done, 0:00:01.063992 left
started <Process(Process-8, started)>
#3-#0: 70% done, 0:00:10.899000 left
#4-#0: 40% done, 0:00:31.674000 left
#3-#0: 80% done, 0:00:07.242000 left
#5-#0: 10% done, 0:03:08.199000 left
m0 = 4096.0

running iteration #8
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 4096.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 2560.0)
START: i = 0
START: @ 2015-08-10 23:19:17
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   3.12500000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 768000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:19:17
Silent? True Debug? False
Running 768000 time-steps:
#8-#0: 0% done, 0:00:00.679999 left
#8-#0: 0% done, 0:00:00.679998 left
started <Process(Process-9, started)>
#3-#0: 90% done, 0:00:03.587000 left
#6-#0: 5% done, 0:05:09.972542 left
#4-#0: 50% done, 0:00:32.920000 left
#4-#0: 60% done, 0:00:24.608000 left
m0 = 8192.0

running iteration #9
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 8192.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 3620.3867196751235)
START: i = 0
START: @ 2015-08-10 23:19:27
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   1.56250000e-01   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 1536000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:19:27
Silent? True Debug? False
Running 1536000 time-steps:
#9-#0: 0% done, 0:00:00.740000 left
#9-#0: 0% done, 0:00:02.275995 left
started <Process(Process-10, started)>
#7-#0: 2% done, 0:14:53.474312 left
#4-#0: 70% done, 0:00:18.168000 left
#8-#0: 1% done, 0:20:48.552333 left
#6-#0: 10% done, 0:05:08.503500 left
m0 = 16384.0

running iteration #10
b'compute-0-102\n' isCloudCompute? True
threads_per_block = 1
array total size = 4 : blocks_per_grid = 4
array total size = 6 : blocks_per_grid = 6
Initial configuration parameters read in from FrequencyDependenceOfSystemBathCoupling.ini

[ DEBUG ]
is_debug	: False
is_numba	: False
is_batch	: True
is_multithreading	: False

[ TOPOLOGY ]
n_dim	: 1
n_atoms	: 3

[ PARTICLES ]
m0	: 16384.0

[ BATHS ]
m_b	: 16
omega_d	: 400
omega_m	: 0
omega_b	: 200
t_r	: 0
delta_t	: 0

[ INTERACTION ]
v0	: 8000
a	: 1
harmonic_approx	: False
gamma_b	: 20

[ MD ]
steps_per_period	: 50
n_slow_periods	: 0.01
min_allowed_time_steps	: 2
max_allowed_time_steps	: 100000000
coarse_graining_resolution	: 1

40.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 starting i = 0 (m_0 = 5120.0)
START: i = 0
START: @ 2015-08-10 23:19:38
initialization in progress:
Baths initialization completed: 0.0 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 \* A$^2 = 1280.0, self.STEPS_PER_PERIOD = 50, self.N_SLOW_PERIODS = 12.0
frequencies = [  4.00000000e+02   7.81250000e-02   2.00000000e+01]
dt = 5e-05 = 1.49896229e-06 ps
self.n_steps = 3072000
Time initialization completed
PhaseData initialization completed
Rt.shape = (303, 2, 1)
Outputs initialization completed
MD initialization completed
initialization completed
RUN: @2015-08-10 23:19:38
Silent? True Debug? False
Running 3072000 time-steps:
#10-#0: 0% done, 0:00:00.690000 left
#10-#0: 0% done, 0:00:00.690000 left
#5-#0: 20% done, 0:02:47.992000 left
#4-#0: 80% done, 0:00:12.272000 left
#9-#0: 0% done, 0:40:21.901089 left
#4-#0: 90% done, 0:00:06.281000 left
#8-#0: 2% done, 0:21:52.946479 left
#10-#0: 0% done, 1:19:31.014633 left
#6-#0: 15% done, 0:04:25.416750 left
#7-#0: 5% done, 0:14:30.274708 left
#9-#0: 1% done, 0:41:05.880594 left
#5-#0: 30% done, 0:02:26.993000 left
#8-#0: 3% done, 0:22:18.653203 left
#6-#0: 20% done, 0:04:01.692667 left
#10-#0: 0% done, 1:30:34.630432 left
#9-#0: 1% done, 0:43:49.758375 left
#7-#0: 7% done, 0:14:00.818219 left
#5-#0: 40% done, 0:02:05.598000 left
#10-#0: 0% done, 1:14:40.000535 left
#8-#0: 5% done, 0:19:54.919104 left
#6-#0: 26% done, 0:03:49.572583 left
#9-#0: 2% done, 0:47:19.595000 left
#10-#0: 1% done, 1:13:40.385568 left
#8-#0: 6% done, 0:21:51.413911 left
#6-#0: 31% done, 0:03:37.035500 left
#7-#0: 10% done, 0:13:36.449958 left
#5-#0: 50% done, 0:01:44.665000 left
#9-#0: 3% done, 0:40:18.078021 left
#10-#0: 1% done, 1:02:37.733051 left
#6-#0: 36% done, 0:03:28.112937 left
#8-#0: 7% done, 0:21:46.202844 left
#10-#0: 1% done, 1:06:51.083930 left
#9-#0: 3% done, 0:38:05.501437 left
#7-#0: 13% done, 0:13:13.587479 left
#5-#0: 60% done, 0:01:23.748000 left
#10-#0: 2% done, 1:07:56.973984 left
#6-#0: 41% done, 0:03:03.998500 left
#9-#0: 4% done, 0:35:24.501240 left
#8-#0: 9% done, 0:21:55.032000 left
#7-#0: 15% done, 0:12:46.820250 left
#9-#0: 5% done, 0:29:56.711583 left
#5-#0: 70% done, 0:01:03.027000 left
#6-#0: 46% done, 0:02:47.136563 left
#10-#0: 2% done, 1:28:45.191208 left
#8-#0: 10% done, 0:17:58.010000 left
#9-#0: 5% done, 0:38:14.849070 left
#10-#0: 2% done, 1:11:11.113164 left
#6-#0: 52% done, 0:02:32.073125 left
#8-#0: 11% done, 0:19:10.926187 left
#7-#0: 18% done, 0:12:22.896198 left
#5-#0: 80% done, 0:00:41.978000 left
#9-#0: 6% done, 0:31:17.568130 left
