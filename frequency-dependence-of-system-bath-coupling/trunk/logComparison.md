Local:
## NVE


    init(omega_D=400, gamma_B=20)

    initialization in progress:
    $\Omega_D$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Omega_M$ = 0.0 cm$^{-1}$ --> 0.0 system units
    $\Omega_B$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Gamma_B$ = 20.0 cm$^{-1}$ --> 0.2 system units
    Baths initialization completed: 0.0 = 0.0 + 0.0
    $V_0$ = 8000.0 cm $^{-1}$
    $a$^{-1}$ = 1.0 $\AA$
    $V_0a^2$ = 80.0
    HARMONIC_APPROX? False
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    V0 = 80.0, A = 1.0, V0 * A$^2 = 80.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 3600.0
    frequencies = [ 5.  4.]
    dt = 0.004 = 0.0001199169832 ps
    slow_component_period = 0.25 = 0.007494811449999999 ps
    n_steps = 225000
    Time initialization completed
    [m] = <class 'numpy.ndarray'>, dim(m) =(3,)
    [x] = <class 'numpy.ndarray'>, dim(x) =(3, 1)
    v[:,0] = [ 0.  0.  0.]
    a[:,0] = [ 0.  0.  0.]
    PhaseData initialization completed
    Initial configuration for 3 atoms in 1 dimensions:
    t = 0 = 0.0 ps, t_n - 1 = -0.0001199169832 ps, i = 0, x[i] = [ 0.], <x[i]> = 0.0, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = -0.0001199169832 ps, i = 1, x[i] = [ 10.1], <x[i]> = 0.0, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = -0.0001199169832 ps, i = 2, x[i] = [ 20.], <x[i]> = 0.0, v[i] = [ 0.]
    COARSE_GRAINING_RESOLUTION = 750
    x0t.shape = (302, 3)
    v0t.shape = (302, 3)
    T[0] = [ 0.  0.  0.]
    Outputs initialization completed
    MD initialization completed
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    initialization completed



    run_numba()

    Silent? False Debug? False
    Running 225000 time-steps:
    Final configuration:
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 0, x[i] = [ 0.], <x[i]> = 0.0, v[i] = [ 0.]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 1, x[i] = [ 10.094], <x[i]> = 10.097119510833874, v[i] = [-0.001]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 2, x[i] = [ 20.], <x[i]> = 20.0, v[i] = [ 0.]
    simulated time = 26.981321219999998 ps
    simulation wall-clock duration = 0:04:06.690731
    time dilation = 9.1e+14%
    6.56 ps per minute
    Var[\Gamma] / t = 0.0 / 6745.3003257542 = 0.0





    (0.0, 0.0, 246.690731)



## $NV\{T_1>T_2=0\}$ Markovian


    N_SLOW_PERIODS = float(1200) * 3
    D_T = float(10)
    init(Delta_T=D_T)

    initialization in progress:
    $\Omega_D$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Omega_M$ = 0.0 cm$^{-1}$ --> 0.0 system units
    $\Omega_B$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Gamma_B$ = 20.0 cm$^{-1}$ --> 0.2 system units
    T_L = 0.06944444444444445
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    $V_0$ = 8000.0 cm $^{-1}$
    $a$^{-1}$ = 1.0 $\AA$
    $V_0a^2$ = 80.0
    HARMONIC_APPROX? False
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    V0 = 80.0, A = 1.0, V0 * A$^2 = 80.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 3600.0
    frequencies = [ 5.  4.]
    dt = 0.004 = 0.0001199169832 ps
    slow_component_period = 0.25 = 0.007494811449999999 ps
    n_steps = 225000
    Time initialization completed
    [m] = <class 'numpy.ndarray'>, dim(m) =(3,)
    [x] = <class 'numpy.ndarray'>, dim(x) =(3, 1)
    v[:,0] = [ 0.  0.  0.]
    a[:,0] = [ 0.  0.  0.]
    PhaseData initialization completed
    Initial configuration for 3 atoms in 1 dimensions:
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 0, x[i] = [ 0.], <x[i]> = 0.0, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 1, x[i] = [ 10.1], <x[i]> = 10.097119510833874, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 2, x[i] = [ 20.], <x[i]> = 20.0, v[i] = [ 0.]
    COARSE_GRAINING_RESOLUTION = 750
    x0t.shape = (302, 3)
    v0t.shape = (302, 3)
    T[0] = [ 0.  0.  0.]
    Outputs initialization completed
    MD initialization completed
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    initialization completed



    run_numba(debugWhat="effectiveGamma")

    Silent? False Debug? False
    Running 225000 time-steps:
    Final configuration:
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 0, x[i] = [ 4.944], <x[i]> = 4.856369530001414, v[i] = [ 0.059]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 1, x[i] = [ 10.389], <x[i]> = 11.859445991728887, v[i] = [-0.298]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 2, x[i] = [ 20.], <x[i]> = 20.0, v[i] = [ 0.]
    simulated time = 26.981321219999998 ps
    simulation wall-clock duration = 0:04:48.036448
    time dilation = 1.1e+15%
    5.62 ps per minute
    time-averaged atomic temperatures: 5.699811888321359e-09 1.0205580857105452 0
    Var[\Gamma] / t = [  4.482e+11] / 6745.3003257542 = [ 66440146.376]





    (array([ 7837.546]), 669446.5930871924, 288.036448)

## $NV\{T_1>T_2=0\}$ Non-Markovian


    """
    global N_SLOW_PERIODS
    N_SLOW_PERIODS /= 16
    """
    init(omega_M = float(400), Delta_T=D_T)

    initialization in progress:
    $\Omega_D$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Omega_M$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Omega_B$ = 400.0 cm$^{-1}$ --> 4.0 system units
    $\Gamma_B$ = 20.0 cm$^{-1}$ --> 0.2 system units
    T_L = 0.06944444444444445
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    $V_0$ = 8000.0 cm $^{-1}$
    $a$^{-1}$ = 1.0 $\AA$
    $V_0a^2$ = 80.0
    HARMONIC_APPROX? False
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    V0 = 80.0, A = 1.0, V0 * A$^2 = 80.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 3600.0
    frequencies = [ 5.  4.  4.]
    dt = 0.004 = 0.0001199169832 ps
    slow_component_period = 0.25 = 0.007494811449999999 ps
    n_steps = 225000
    Time initialization completed
    [m] = <class 'numpy.ndarray'>, dim(m) =(3,)
    [x] = <class 'numpy.ndarray'>, dim(x) =(3, 1)
    v[:,0] = [ 0.  0.  0.]
    a[:,0] = [ 0.  0.  0.]
    PhaseData initialization completed
    Initial configuration for 3 atoms in 1 dimensions:
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 0, x[i] = [ 0.], <x[i]> = 4.856369530001414, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 1, x[i] = [ 10.1], <x[i]> = 11.859445991728887, v[i] = [ 0.]
    t = 0 = 0.0 ps, t_n - 1 = 0.0358551779768 ps, i = 2, x[i] = [ 20.], <x[i]> = 20.0, v[i] = [ 0.]
    COARSE_GRAINING_RESOLUTION = 750
    x0t.shape = (302, 3)
    v0t.shape = (302, 3)
    T[0] = [ 0.  0.  0.]
    Outputs initialization completed
    MD initialization completed
    DBG: V0A2 = 80.0 * 1.0^2 = 80.0
    initialization completed



    run_numba()

    Silent? False Debug? False
    Running 225000 time-steps:
    Final configuration:
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 0, x[i] = [-0.356], <x[i]> = -0.6090512402946465, v[i] = [ 0.055]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 1, x[i] = [ 12.018], <x[i]> = 12.199801411843564, v[i] = [-0.045]
    t = 224999 = 26.981201303016796 ps, t_n - 1 = 0.0358551779768 ps, i = 2, x[i] = [ 20.], <x[i]> = 20.0, v[i] = [ 0.]
    simulated time = 26.981321219999998 ps
    simulation wall-clock duration = 0:04:35.419773
    time dilation = 1.0e+15%
    5.88 ps per minute
    time-averaged atomic temperatures: 2.6106683222379617e-14 0.009773588897389154 0
    Var[\Gamma] / t = [  1.395e+13] / 6745.3003257542 = [  2.068e+09]





    (array([ 38239.801]), 3735156.380723595, 275.41977299999996)


# Scan over 'bath-particle' mass to extrapolate low-frequency dependence


    OMEGA_M=0
    A=float(4)

about an hour for two million time-steps,
i.e. 1.8 milliseconds per time-step.


    #N_SLOW_PERIODS = float(100) * 5 # ~ 10 minutes wall-clock time per simulation
    #MAX_ALLOWED_TIME_STEPS = 1e7
    n_points = 10
    N_SLOW_PERIODS = float(100) * 10
    MAX_ALLOWED_TIME_STEPS = 1e8


    #max_scan_wall_clock_time = n_points * (60 * 10)
    
    #N_SLOW_PERIODS = min(N_SLOW_PERIODS, (max_scan_wall_clock_time / (n_points * 10 * 60)) * 100 * 5)

$
\begin{align}
\log_x (0.50 / 0.01) &= n_{points} = 100 \\
\ln (0.50 / 0.01) / \ln x &= 100 \\
\ln (0.50 / 0.01) / 100 &= \ln x \\
x &= \exp (\ln (0.50 / 0.01) / 100) \approx 103.76\% \\
\end{align}
$


    #np.set_printoptions(precision=3)
    base = math.exp(math.log(0.40 / 0.01) / n_points)
    print(base)

    1.4461255495919247



    couplingFrequencyDependence = np.zeros(shape=(n_points, 3)) # 2-D matrix: first column is frequency, second column is avg. Gamma, third column is Gamma Std. Dev. error estimate
    
    m_0 = math.sqrt(M0)
    if (1 > m_0):
        raise RuntimeError("initial bath atom mass ({}) must be greater than one for scan!".format(m_0))
    
        
    time_elapsed = 0
    for i in range(n_points):
        
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        ###################################################################################
        #m_0 *= base                                                                     ##
        #print(" starting i = {} (m_0 = {})".format(i, m_0))                             ##
        ###################################################################################
        # DEBUG -- REMOVE!!! ##############################################################
        N_SLOW_PERIODS = float(100) * (pow(2,i))                                          ##
        print("DEBUG -- REMOVE!!! i = {}, N_SLOW_PERIODS = {}".format(i, N_SLOW_PERIODS))##
        ###################################################################################
        ###################################################################################
        
        init(m0=m_0, isDebug=False) 
        GammaAvg, GammaSD, simulation_duration = run_numba(isSilent=True)
        
        if (isnearlyzero(GammaSD)):
            raise RuntimeError("standard deviation of effective Gamma is zero!")
        
        omega = math.sqrt(V0A2 / M0)
        couplingFrequencyDataPoint = [omega, GammaAvg, GammaSD]
        # print(couplingFrequencyDataPoint)
        if(abs(GammaAvg) > GammaSD):
            couplingFrequencyDependence[i] = couplingFrequencyDataPoint
        
        print()
        print("finished i = {} (m_0 = {}) {}".format(i, m_0, couplingFrequencyDataPoint))    
        time_left = calcTimeLeft(time_elapsed, simulation_duration, segment_no=i+1, totSegments=n_points)
        print("{} left".format(datetime.timedelta(seconds=time_left)))
        time_elapsed += simulation_duration
    
    
    print("\ndone! (total # points = {}, total wall-clock time = {})".format(n_points, datetime.timedelta(seconds=time_elapsed)))

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    DEBUG -- REMOVE!!! i = 0, N_SLOW_PERIODS = 100.0
    initialization in progress:
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 100.0
    frequencies = [ 640.    4.]
    dt = 3.125e-05 = 9.3685143125e-07 ps
    n_steps = 1280000
    Time initialization completed
    PhaseData initialization completed
    Outputs initialization completed
    MD initialization completed
    initialization completed
    Silent? True Debug? False
    Running 1280000 time-steps:
    Final configuration:
    t = 1279999 = 1.1991688951485686 ps, t_n - 1 = 0.00028105542937499996 ps, i = 0, x[i] = [-1.911], <x[i]> = -1.891994384961609, v[i] = [-0.339]
    t = 1279999 = 1.1991688951485686 ps, t_n - 1 = 0.00028105542937499996 ps, i = 1, x[i] = [ 1.141], <x[i]> = 1.14624527498948, v[i] = [-0.077]
    t = 1279999 = 1.1991688951485686 ps, t_n - 1 = 0.00028105542937499996 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
    simulated time = 1.199169832 ps
    simulation wall-clock duration = 0:28:51.395642
    time dilation = 1.4e+17%
    0.0416 ps per minute
    time-averaged atomic temperatures: 2.0218107792307876e-12 0.05660410844157134 0
    Var[\Gamma] / t = [  6.142e+10] / 38373.4046447542 = [ 1600703.684]
    
    finished i = 0 (m_0 = 1.4142135623730951) [30.084824744691147, array([ 3159.703]), 247839.5653754773]
    0:25:58.256078 left
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    DEBUG -- REMOVE!!! i = 1, N_SLOW_PERIODS = 200.0
    initialization in progress:
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 200.0
    frequencies = [ 905.097    4.   ]
    dt = 2.2097086912079613e-05 = 6.624540000011976e-07 ps
    n_steps = 2262741
    Time initialization completed
    PhaseData initialization completed
    Outputs initialization completed
    MD initialization completed
    initialization completed
    Silent? True Debug? False
    Running 2262741 time-steps:
    Final configuration:
    t = 2262740 = 1.4989611639627098 ps, t_n - 1 = 0.0001987362000003593 ps, i = 0, x[i] = [-0.04], <x[i]> = -0.04226999945185279, v[i] = [ 0.035]
    t = 2262740 = 1.4989611639627098 ps, t_n - 1 = 0.0001987362000003593 ps, i = 1, x[i] = [ 2.57], <x[i]> = 2.5910798973090396, v[i] = [-0.239]
    t = 2262740 = 1.4989611639627098 ps, t_n - 1 = 0.0001987362000003593 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
    simulated time = 1.49896182641671 ps
    simulation wall-clock duration = 1:03:52.838412
    time dilation = 2.6e+17%
    0.0235 ps per minute
    time-averaged atomic temperatures: 2.012078205261629e-12 0.2103395419137517 0
    Var[\Gamma] / t = [  2.231e+12] / 67835.23864149199 = [ 32887923.462]
    
    finished i = 1 (m_0 = 1.4142135623730951) [30.084824744691147, array([ 4319.519]), 1493639.8951728847]
    3:55:47.202838 left
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    DEBUG -- REMOVE!!! i = 2, N_SLOW_PERIODS = 400.0
    initialization in progress:
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 400.0
    frequencies = [ 905.097    4.   ]
    dt = 2.2097086912079613e-05 = 6.624540000011976e-07 ps
    n_steps = 4525483
    Time initialization completed
    PhaseData initialization completed
    Outputs initialization completed
    MD initialization completed
    initialization completed
    Silent? True Debug? False
    Running 4525483 time-steps:
    Final configuration:
    t = 4525482 = 2.99792365283342 ps, t_n - 1 = 0.0001987362000003593 ps, i = 0, x[i] = [ 0.767], <x[i]> = 0.7617916985347161, v[i] = [-0.314]
    t = 4525482 = 2.99792365283342 ps, t_n - 1 = 0.0001987362000003593 ps, i = 1, x[i] = [ 2.612], <x[i]> = 2.637925787695378, v[i] = [-0.137]
    t = 4525482 = 2.99792365283342 ps, t_n - 1 = 0.0001987362000003593 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
    simulated time = 2.99792431528742 ps
    simulation wall-clock duration = 2:24:58.244291
    time dilation = 2.9e+17%
    0.0207 ps per minute
    time-averaged atomic temperatures: 2.7149607472761754e-14 0.33978786463513555 0
    Var[\Gamma] / t = [  3.616e+14] / 135670.5372414756 = [  2.665e+09]
    
    finished i = 2 (m_0 = 1.4142135623730951) [30.084824744691147, array([ 34319.483]), 19015135.79058453]
    9:15:53.517868 left
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    DEBUG -- REMOVE!!! i = 3, N_SLOW_PERIODS = 800.0
    initialization in progress:
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 800.0
    frequencies = [ 905.097    4.   ]
    dt = 2.2097086912079613e-05 = 6.624540000011976e-07 ps
    n_steps = 9050966
    Time initialization completed
    PhaseData initialization completed
    Outputs initialization completed
    MD initialization completed
    initialization completed
    Silent? True Debug? False
    Running 9050966 time-steps:
    Final configuration:
    t = 9050965 = 5.99584796812084 ps, t_n - 1 = 0.0001987362000003593 ps, i = 0, x[i] = [ 2.64], <x[i]> = 2.6698970102864554, v[i] = [ 0.047]
    t = 9050965 = 5.99584796812084 ps, t_n - 1 = 0.0001987362000003593 ps, i = 1, x[i] = [ 3.604], <x[i]> = 4.092323498493563, v[i] = [-2.159]
    t = 9050965 = 5.99584796812084 ps, t_n - 1 = 0.0001987362000003593 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
    simulated time = 5.99584863057484 ps
    simulation wall-clock duration = 4:10:40.647675
    time dilation = 2.5e+17%
    0.0239 ps per minute
    time-averaged atomic temperatures: 2.7912253549709133e-13 5.636772513594573 0
    Var[\Gamma] / t = [  3.935e+13] / 271341.104462197 = [  1.450e+08]
    
    finished i = 3 (m_0 = 1.4142135623730951) [30.084824744691147, array([ 19228.461]), 6272660.431955301]
    16:46:09.310647 left
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    DEBUG -- REMOVE!!! i = 4, N_SLOW_PERIODS = 1600.0
    initialization in progress:
    Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
    V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 1600.0
    frequencies = [ 905.097    4.   ]
    dt = 2.2097086912079613e-05 = 6.624540000011976e-07 ps
    n_steps = 18101933
    Time initialization completed
    PhaseData initialization completed
    Outputs initialization completed
    MD initialization completed
    initialization completed
    Silent? True Debug? False
    Running 18101933 time-steps:
    0% done, 18:08:49.545755 left





CloudSageMath:
DEBUG -- REMOVE!!! i = 0, N_SLOW_PERIODS = 100.0
initialization in progress:
Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 100.0
frequencies = [ 80.   4.]
dt = 0.00025 = 7.49481145e-06 ps
n_steps = 225000
Time initialization completed
PhaseData initialization completed
Outputs initialization completed
MD initialization completed
initialization completed
Silent? True Debug? False
Running 225000 time-steps:
Final configuration:
t = 224998 = 1.6863175866270999 ps, t_n - 1 = 0.00224094862355 ps, i = 0, x[i] = [-0.735], <x[i]> = -0.7857309794969634, v[i] = [ 0.113]
t = 224998 = 1.6863175866270999 ps, t_n - 1 = 0.00224094862355 ps, i = 1, x[i] = [ 1.567], <x[i]> = 1.574333422787848, v[i] = [-0.026]
t = 224998 = 1.6863175866270999 ps, t_n - 1 = 0.00224094862355 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
simulated time = 1.6863325762499999 ps
simulation wall-clock duration = 0:02:59.977768
time dilation = 1.1e+16%
0.562 ps per minute
time-averaged atomic temperatures: 1.0065684571542605e-11 0.0011054195277679227 0
Var[\Gamma] / t = [  8.391e+10] / 6745.2703465084 = [ 12439739.469]

finished i = 0 (m_0 = 4.0) [17.88854381999832, array([ 6097.982]), 289671.2028442904]
0:02:41.979991 left
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DEBUG -- REMOVE!!! i = 1, N_SLOW_PERIODS = 200.0
initialization in progress:
Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 200.0
frequencies = [ 320.    4.]
dt = 6.25e-05 = 1.8737028625e-06 ps
n_steps = 800000
Time initialization completed
PhaseData initialization completed
Outputs initialization completed
MD initialization completed
initialization completed
Silent? True Debug? False
Running 800000 time-steps:
Final configuration:
t = 799998 = 1.4989585425942749 ps, t_n - 1 = 0.0005621108587499999 ps, i = 0, x[i] = [ 2.206], <x[i]> = 2.189834254631676, v[i] = [ 0.211]
t = 799998 = 1.4989585425942749 ps, t_n - 1 = 0.0005621108587499999 ps, i = 1, x[i] = [ 3.527], <x[i]> = 3.5019652772071486, v[i] = [ 0.279]
t = 799998 = 1.4989585425942749 ps, t_n - 1 = 0.0005621108587499999 ps, i = 2, x[i] = [ 5.], <x[i]> = 5.0, v[i] = [ 0.]
simulated time = 1.49896229 ps
simulation wall-clock duration = 0:11:28.638219
time dilation = 4.6e+16%
0.131 ps per minute
time-averaged atomic temperatures: 3.672735038431713e-11 0.07418540363588481 0
Var[\Gamma] / t = [  6.537e+13] / 23983.336681508397 = [  2.725e+09]

finished i = 1 (m_0 = 4.0) [17.88854381999832, array([ 24806.775]), 8084872.185246536]
0:28:22.768290 left
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DEBUG -- REMOVE!!! i = 2, N_SLOW_PERIODS = 400.0
initialization in progress:
Baths initialization completed: 10.000000000000002 = 0.0 + 0.0
V0 = 80.0, A = 4.0, V0 * A$^2 = 1280.0, STEPS_PER_PERIOD = 50, N_SLOW_PERIODS = 400.0
frequencies = [ 320.    4.]
dt = 6.25e-05 = 1.8737028625e-06 ps
n_steps = 1600000
Time initialization completed
PhaseData initialization completed
Outputs initialization completed
MD initialization completed
initialization completed
Silent? True Debug? False
Running 1600000 time-steps:
0% done, 0:24:08.256441 left
0% done, 0:24:08.255086 left
0% done, 0:49:40.563370 left
~0:50:00


