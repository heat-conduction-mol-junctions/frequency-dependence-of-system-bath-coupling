'''
sandbox.py
@author: Inon Sharony
@contact: InonShar@TAU.ac.IL
@organization:
@since:  Aug 22, 2015
'''


def regex():

    s = "[[ 6.7 0. 8 ]]"
    print(s)

    import re
    r = re.compile(r'[\[\]]+')
    print(r)

    print(r.sub("", s))
    print((r.sub("", s)).split())

    line = [float(s) for s in (r.sub("", s)).split()]
    print(line)


def main():
    regex()

if __name__ == '__main__':
    main()
