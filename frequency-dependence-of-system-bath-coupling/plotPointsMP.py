'''
Created on Jul 6, 2015

.. sectionauthor:: Inon Sharony <inonshar@tau.ac.il>
see `Inspiration from thegreenplace blog <http://eli.thegreenplace.net/2012/01/16/python-parallelizing-cpu-bound-tasks-with-multiprocessing>`_
see `Documentation markup (using reStructuredText) <https://docs.python.org/devguide/documenting.html#additional-markup-constructs>`_
'''
from math import pow, ceil
from time import sleep
from warnings import warn
from logging import basicConfig, debug, info
from multiprocessing import Process, Queue
from runpy import run_module
from os import getcwd
from sys import argv

def mp_runner(particleMasses, nprocs):
    """
    .. function:: mp_runner(particleMasses, nprocs)
        :param particleMasses: domain of particle mass multiplication factors
        :type particleMasses: single-rank two-dimensional array of int
        :param nprocs: number of available processors
        :type nprocs: int
        :returns: resultDist from itrn no. to CouplingFrequencyDependence tuple
        :rtype: dictionary from int to tuple

        .. side-effects:
            .. parameter mutations:
        .. exceptions:
        .. admonitions: note, warning, etc.
        .. examples:
    """
    def worker(mass_domain, out_q):
        """ The worker function, invoked in a sub-process.
            'mass_domain' is a range of numbers to use as particle mass multiplication factors.
            The resulting plot points are placed in a dictionary that's pushed to a queue.
        """
        outdict = {}
        for n in range(mass_domain[0], mass_domain[1], 1):
            updateMass(n)
            info("running iteration #{}".format(n))
            #from PlotPoint import * # https://docs.python.org/3/library/importlib.html#module-importlib.machinery
            #try:
            #    outdict[n] = run_module(PlotPoint)
            #except:
            import PlotPoint
            outdict[n] = PlotPoint.PlotPoint(n).getCouplingFrequencyDependence()
            info("finished iteration #{}:\n{}".format(n,outdict[n]))
        out_q.put(outdict)

    # Each process will get 'chunksize' mass_domain and a queue to put his out
    # dict into
    out_q = Queue()
    chunksize = int(ceil(particleMasses / float(nprocs)))
    debug("chunk-size = {}".format(chunksize))
    procs = []

    for i in range(nprocs):
        sleep(i)
        processName = "-".join(('PlotPoint',str(i))) if chunksize == 1 else None
        p = Process(
                target=worker, name=processName,
                args=((i * chunksize,(i+1) * chunksize), 
                      out_q))
        procs.append(p)
        p.start()
        info("started {}".format(p))

    # Collect all results into a single result dict. We know how many dicts
    # with results to expect.
    resultdict = {}
    for i in range(nprocs):
        resultdict.update(out_q.get())
        debug("updated result dictionary: {}".format(resultdict))

    # Wait for all worker processes to finish
    for p in procs:
        p.join()
        info("joined {}".format(p))

    out_q.close()
    return resultdict

def updateMass(n):
    sleep(n)
    debug("Current Working Directory: %s" % getcwd())
    resource_directory = "resources"
    isResourceSubdirectoryOfCWD = True
    iniFilename = "FrequencyDependenceOfSystemBathCoupling"

    iniFilePath = '/'.join((resource_directory,iniFilename))
    if not isResourceSubdirectoryOfCWD:
        iniFilePath = '/'.join(("..",iniFilePath))

    with open('.'.join((iniFilePath, "bak")), mode='r') as inFile:
        ini = inFile.readlines()
    if len(ini) == 0:
        raise RuntimeError("n = {}, read zero lines from {}!".format(n, iniFilename))
    try:
        ini[30] = "m0 = " + str(16 * pow(2,n)) + '\n'
    except IndexError as ie:
        warn("{}\nn = {}, len(ini) = {}, ini:\n{}".format(ie, n, len(ini), ini))
        raise ie # rethrow
    debug(ini[30])
    with open('.'.join((iniFilePath, "ini")), mode='w') as outFile:
        outFile.writelines(ini)


def main():

    # configure logging
    csvLoggerFormat = "%(relativeCreated)-6d,%(levelname)-8s,%(processName)-12s,%(threadName)-12s,%(name)-4s,%(module)-14s,%(funcName)-8s,%(lineno)-4d,%(message)s"
    basicConfig(level="DEBUG", format=csvLoggerFormat)
    info("ms,level,process,thread,logger,module,function,line,message")
    info("argv = {}".format(argv))

    nodesMin1 = 8 - 1
    ppnMin1 = 16 - 1

    # import socket, re
    # hostname = socket.gethostname()
    # 
    # if (re.match(".*\.tau\.ac\.il", hostname)):
    #     nodesMin1 = 24 - 1
    # elif (re.match(".*helium\.sas\.upenn\.edu", hostname)):
    #     ppnMin1 = 48 - 1
    # else:
    #     raise RuntimeError("unrecognized original host. pbs_o_host = %s, sge_o_host = %s" % (pbs_o_host, sge_o_host))

    # not thread safe, leads to core dump
    # import os
    # pbs_o_host = os.environ.get('PBS_O_HOST') # os.getenv('PBS_O_HOST')
    # sge_o_host = os.environ['SGE_O_HOST'] # os.getenv('SGE_O_HOST')
    #
    # if (pbs_o_host == "power.tau.ac.il"):
    #     nodesMin1 = 24 - 1
    # elif (sge_o_host == "helium"):
    #     ppnMin1 = 48 - 1
    # else:
    #     raise RuntimeError("unrecognized original host. pbs_o_host = %s, sge_o_host = %s" % (pbs_o_host, sge_o_host))

    maxProcs = nodesMin1 * ppnMin1
    if len(argv) == 1:
       nprocs = maxProcs
       particleMasses = nprocs
    else:
       nprocs = min(int(int(argv[1]) / ppnMin1) + 1, maxProcs / ppnMin1)
       particleMasses = nprocs * ppnMin1 if nprocs > 1 else int(argv[1])
    debug("particleMasses = {}, nprocs = {}".format(particleMasses, nprocs))
    dictResults = mp_runner(particleMasses, nprocs)
    info(dictResults)
    return dictResults

if __name__ == '__main__':
    main()
