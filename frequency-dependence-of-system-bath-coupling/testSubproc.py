import subprocess

def submit():
    print("submitting:")
    #subproc = subprocess.check_output(["qsub", "-l nodes=1:ppn=1","PlotPoint.sh"], shell=True)
    subproc = subprocess.check_output(["qsub","../tests/probeNode.sh"])
    #print("{} --> {}".format(subproc.args,subproc))
    print(subproc)

def main():
    submit()

if __name__ == "__main__":
    # execute only if run as a script
    main()
