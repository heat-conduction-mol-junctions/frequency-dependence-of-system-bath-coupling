'''
Submit n_points many single-processor jobs, each for a given particle mass (and therefore frequency)  
'''
from configparser import ConfigParser
import subprocess

import PlotPoint

import multiprocessing


def increaseMass(config, newMass):
    with open(PlotPoint.initConfigFile, 'w') as fp:
        config.write(fp)
    config.set('PARTICLES', 'M0', newMass)
    fp.close()

def submit(i):
    print("submitting #{}".format(i))
    subprocess.call("qsub -l nodes=1:ppn=1 PlotPoint.sh")
    #subproc = subprocess.check_output("qsub -l nodes=1:ppn=1 PlotPoint.sh")
    #print(subproc)

def main():
    config = ConfigParser()
    
    config.set('DEBUG', 'IS_BATCH', True)
    with open(PlotPoint.initConfigFile, 'w') as fp:
        config.write(fp)
    
    config.read(PlotPoint.initConfigFile)
    M_0 = config.getfloat('PARTICLES', 'M0')
    
    fp.close()

    n_points = 10
    for i in range(n_points):
        increaseMass(config, M_0 * (i + 1))
        proc = multiprocessing.Process(target=submit, args=(i,))
        proc.start()
        #submit(i)
        PlotPoint.printConfigParams()
        
if __name__ == "__main__":
    # execute only if run as a script
    main()
