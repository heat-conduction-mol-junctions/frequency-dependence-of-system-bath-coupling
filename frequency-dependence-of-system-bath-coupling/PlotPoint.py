# http://ipython.org/ipython-doc/stable/interactive/reference.html#embedding-ipython
# http://pydev.org/manual_101_interpreter.html
# http://pydev.org/manual_adv_interactive_console.html
# from IPython.utils.encoding import 'utf-8'
from builtins import staticmethod
import cProfile
from configparser import ConfigParser
import datetime
import functools
import io
import itertools
from logging import basicConfig, debug, info, error, warning, critical
import math
import matplotlib
from matplotlib.pyplot import savefig
from os.path import isfile
import pstats
from scipy.optimize import curve_fit, minimize
from subprocess import check_output, call
import sys
import threading
import time
from traceback import print_exc
import warnings

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy as np


# import cairocffi
# print(IPython.sys_info())
# from IPython.core.interactiveshell import InteractiveShell
# ipythonIshell = InteractiveShell()
# coding: utf-8
def calc_blocks_per_grid(tensor, threads_per_block):
    try:
        return int((tensor.size + (threads_per_block - 1)) / threads_per_block)
    except:
        return int((len(tensor) + (threads_per_block - 1)) / threads_per_block)


def test_reduce(array):
    return functools.reduce(lambda x, y: x.size + y.size, array)


def test_reduce_no_lambda(array):
    mySum = 0
    for x in array:
        mySum += x.size
    return mySum


@staticmethod
def redrawSubplot(ax=None):
    if(ax is None):
        ax = plt.gca()  # get the current axes
    ax.relim()  # make sure all the data fits
    ax.autoscale()  # auto-scale
    # ax.draw()       # re-draw the figure


class PlotPoint():
    """
    # # Frequency Dependence of System-Bath Coupling:
    # ## Single particle in 1-D junction with exponentially decaying interactions
    # 1. Print input parameters
    # 2. Generate Markovian random force for step (method of Bath class)
    # 3. EOM integration step (e.g. velocity-Verlet) (method of MD class)
    #     1. Include possible arguments for Markovian Langevin dynamics
    #         1. Dissipation
    #         2. Random force
    # 4. Calculate potential energy term for given configuration (method of Interaction class)
    #     1. Exponentially decaying interaction function
    #     2. Harmonic Oscillator
    # 5. Calculate heat current and conductivity during a given step (method of MD class)
    # 6. Print output results
    # Useful links:
    # 1. http://codepad.org/FcbFcoue
    # 2. http://gdrcorelec.ups-tlse.fr/files/python_verlet.pdf
    # 3. http://docs.scipy.org/doc/numpy/reference/routines.html
    # 4. http://ipython.org/ipython-doc/stable/interactive/index.html
    # 5. http://nbviewer.ipython.org/github/ipython/ipython/blob/1.x/examples/notebooks/Cell%20Magics.ipynb
    #
    # Further reading:
    # 1. http://stackoverflow.com/questions/15374291/writing-a-faster-python-physics-simulator/15375757#15375757
    # 2. http://dirac.cnrs-orleans.fr/MMTK/using-mmtk/mmtk-example-scripts/molecular-dynamics/an-md-integrator-written-in-python
    # 3. http://www.sam.math.ethz.ch/~raoulb/teaching/PythonTutorial/
    # 4. http://combichem.blogspot.co.il/
    # 5. https://wiki.python.org/moin/PythonSpeed
    # 6. https://wiki.python.org/moin/PythonSpeed/PerformanceTips
    # 7. https://github.com/WeatherGod/AnatomyOfMatplotlib
    # 8. http://ipython-books.github.io/cookbook/
    # 9. https://docs.python.org/3/library/concurrency.html
    # 10. http://stackoverflow.com/a/11075375
    # 11. http://www.huyng.com/posts/python-performance-analysis/
    # 12. http://scikit-learn.org/stable/developers/performance.html
    # ##Note
    # 1. To debug a crahsed program, insert "%debug" in a code cell following the trace-back. This will bring up the IPython Debugger (ipdb), which takes the usual shorthand:
    #     1. (c)ontinue
    #     2. (n)ext, a.k.a. step-over
    #     3. (s)tep into
    #     4. (b)reakpoint insert
    # 2. Pretty-printing variable x, then all local variables and then all global variables:
    #     ```
    #     >>> import pprint
    #     >>> import pprint(x)
    #     >>> pprint.pprint(locals())
    #     >>> pprint.pprint(globals())
    #     ```
    # ## System units:
    # ### System characteristics
    # 1. Characteristic distance ~ 1 $\AA$ (Angstrom) $\Rightarrow$ [x] $\equiv 1 \AA$
    # 2. Characteristic mass ~ 1 a.m.u. $\Rightarrow$ [m] $\equiv$ 1 a.m.u.
    # 3. Characteristic energy ~ 100 cm$^{-1} \Rightarrow$ [E] $\equiv$ 100 cm$^{-1}$
    # ### Derived units:
    # 1. Temperature = 100 cm$^{-1}$ $\times$ $k_B$ $\approx$ 144 Kelvin
    #     \* $k_B$ = $1.38064 \times 10^{-23}$ J/K (Boltzmann constant in SI units)
    # 2. Time = 1/ (100 cm$^{-1}$ $\times c$ [cm / s]) s = 1 / 29979245800 s $ \approx 33.35641 \times 10^{-12}$ s = 33.35641 ps $\Rightarrow$ [t] $\approx \frac{100}{3}$ ps
    # 3. Speed = 1 Angstrom / 33.35641 ps = 1 Angstrom $\times$ 0.029979245800 ps$^{-1}$ = 0.029979245800 Angstrom / ps
    # 4. Acceleration = 0.029979245800 Angstrom / ps $\times$ 0.029979245800 ps$^{-1}$ = 0.0008987551787 Angstrom / ps$^{-2}$
    # 5. Momentum = 1 a.m.u. $\times$ 0.029979245800 Angstrom / fs = 0.029979245800 a.m.u. $\times$ Angstrom / ps
    # ## Pylab magic:
    # In[4]:
    # magic!
    # import matplotlib's pylab module (numpy + matplotlib)
    # with "--inline" flag to display plots inline in notebook
    # get_ipython().magic(u'pylab inline')
    # #from math import exp
    # import math
    # #from numpy import array, ndarray # dev note: why can't I import like this?
    # import numpy as np
    # import matplotlib.pyplot as plt
    # #from matplotlib import pyplot as plt
    # #import matplotlib as mpl
    # \"""
    # try:
    #    import matplotlib.pyplot as plt
    # except:
    #    raise
    # \"""
    # from random import gauss
    # ##CUDA on Numba
    # 1. http://numba.pydata.org/numba-doc/0.18.2/cuda/kernels.html#kernel-invocation
    # 2. http://nbviewer.ipython.org/gist/harrism/835a8ca39ced77fe751d#CUDA-JIT
    # 3. http://documen.tician.de/pycuda/metaprog.html#metaprogramming
    """

    # In[6]:

    @classmethod
    def setGpgpuMemory(cls):
        """
        shell command to find GPU memory for setting up GPGU shared-memory thread-blocks
        """
        # windoze should use WMIC ?

        # In[8]:
        cls.isCloudCompute = False
        # hostname_reply = get_ipython().getoutput(u'hostname')
        hostname_reply = check_output("hostname")
        hostname_reply
        if (type(hostname_reply) is bytes):
            cls.isCloudCompute = (hostname_reply.startswith(b'compute'))
        else:
            hostname_reply = (check_output("hostname").decode('utf-8'))
            cls.isCloudCompute = hostname_reply[0].startswith("compute")
        debug("{} cls.isCloudCompute? {}".format(
            hostname_reply, cls.isCloudCompute))
        try:
            call("lspci")
        except:
            cls.isCloudCompute = True

        if not cls.isCloudCompute:
            # get_ipython().system(u'lspci -vvv | grep "VGA compatible controller" -A 12 | grep Memory')
            get_gpu_specs_cmd = \
                u'lspci -vvv | grep "VGA compatible controller" -A 12'
            call(get_gpu_specs_cmd + " | grep Memory", shell=True)

        # In[7]:

        ram_per_gpu = 1e6
        if not cls.isCloudCompute:
            # gpu_memory = get_ipython().getoutput(u'lspci -vvv | grep "VGA compatible controller" -A 12 | grep " prefetchable" | head -n1 | cut -d= -f2 | cut -d] -f1')
            prefetchable = \
                u' | grep " prefetchable" | head -n1 | cut -d= -f2 | cut -d] -f1'
            gpu_memory = check_output(
                get_gpu_specs_cmd + prefetchable, shell=True).decode('utf-8')
            debug("gpu_memory = %s" % gpu_memory)
            if not gpu_memory:
                ram_per_gpu = 0
            else:
                ram_per_gpu = int(gpu_memory[0:-2]) * 1e6
            debug("ram_per_gpu = %d" % ram_per_gpu)

        # In[8]:

        memory_per_thread = 1e6
        threads_per_block = max(1, int(ram_per_gpu / memory_per_thread))
        debug("threads_per_block = %d" % threads_per_block)

        # In[9]:

        a = []
        a.append(np.zeros(shape=(2 * threads_per_block, 2)))
        a.append(np.zeros(shape=(2 * threads_per_block + 1, 2)))
        for array in a:
            debug("array total size = %s : blocks_per_grid = %d" %
                  (array.size, calc_blocks_per_grid(array, threads_per_block)))

        # In[10]:

        # get_ipython().magic(u'timeit test_reduce(a)')

        # In[11]:

        # PyCUDA does not yet support listcomp, lambdex, etc.

        # In[10]:

        # if not cls.isCloudCompute:
        #     from numba import cuda, jit, types
        #
        #     test_reduce_numba_cuda = cuda.autojit(test_reduce_no_lambda)
        #
        #     # http://numba.pydata.org/numba-doc/dev/reference/types.html#id2
        #     @cuda.jit(uint8(types.Array(double, array.size, 'C')))
        #     def test_reduce_numba_cuda(array):
        #         sum = 0
        #         for x in array:
        #             sum += x.size
        #         return sum
        #
        #     blocks_per_grid = calc_blocks_per_grid(a)
        #     test_numba_cuda = test_reduce_numba_cuda[blocks_per_grid, threads_per_block]
        #     get_ipython().magic(u'timeit test_numba_cuda(a)')

        # ### scikit.cuda
        # wrapper for PyCUDA, currently supports Python2 only

        # In[13]:

        # scalar = np.float64(np.random.rand())
        # input_x = np.random.rand(5).astype(np.float64)
        # io_y = np.random.rand(5).astype(np.float64)

        # get_ipython().magic(u'timeit no_cuda = scalar * input_x + io_y')

        # In[14]:

        # if not cls.isCloudCompute:
        #     import pycuda.autoinit
        #     import pycuda.gpuarray as gpuarray
        #
        #     input_x_gpu = gpuarray.to_gpu(input_x)
        #     io_y_gpu = gpuarray.to_gpu(io_y)
        #
        #     import scikits.cuda.cublas
        #
        #     h = cublasCreate()
        #     cublasDaxpy(h, input_x_gpu.size, scalar, input_x_gpu.gpudata, 1, io_y_gpu.gpudata, 1)
        #     cublasDestroy(h)
        #
        #     np.allclose(io_y_gpu.get(), no_cuda)

        return cls.isCloudCompute

    # In[15]:

    # ## Global constants:

    # In[16]:

    '''
    System units:
    length: 1 Angstrom
    mass: 1 a.m.u.
    time: from energy, using c
    energy: 100 wavenumbers
    '''
    '''
    energy scale ~ 100 wavenumbers
    '''
    WAVENUMBER_TO_SYS_UNITS = 1e-2
    '''
    100 wavenumbers ~= 3e12 Hz
    ==> linear frequency scale ~ 3 PHz
    '''
    '''
    3e12 Hz ~= 18.85e12 radians / s
    ==> angular frequency scale ~ 2e13 radians / s
    '''
    '''
    3e12 Hz ~= (33.3e-12 s)^(-1) = (33.3 picoseconds)^(-1)
    ==> time scale ~ 33.3 ps
    '''

    # exact by definition of the meter
    SPEED_OF_LIGHT_IN_CM_PER_SEC = 29979245800
    # SECONDS_TO_SYS_UNITS = float(100) / 3e-12 # float(1) / (33.3e-12)
    # self.PICOSECONDS_TO_SYS_UNITS = float(100) / 3
    # ps to s: /1e-12, s to cm$^{-1}$: /$c$
    PICOSECONDS_TO_SYS_UNITS = float(
        1) / (1e-12 * SPEED_OF_LIGHT_IN_CM_PER_SEC)
    '''
    100 wavenumbers ~= 144 Kelvin
    ==> temperature scale ~ 144 Kelvin
    '''
    KELVIN_TO_SYS_UNITS = float(1) / 144
    # mass scale ~ 1 a.m.u.

    # ## Declaration of global variables:

    # In[17]:

    @staticmethod
    def printConfigParams(config):
        for section in config.sections():
            ("\n[ {} ]".format(section))
            for key in config[section]:
                debug("{}\t: {}".format(key, config[section][key]))

    def __init__(self, m_factor):

        if sys.version_info.major < 3:
            raise Exception(
                "system major version is less than 3! (%d)" % sys.version_info.major)

        csvLoggerFormat = "%(relativeCreated)-6d,%(levelname)-8s,%(processName)-12s,%(threadName)-12s,%(name)-4s,%(module)-14s,%(funcName)-8s,%(lineno)-4d,%(message)s"
        basicConfig(level="DEBUG", format=csvLoggerFormat)
        info("ms,level,process,thread,logger,module,function,line,message")

        PlotPoint.setGpgpuMemory()

        info("initializing")

        self.m_factor = m_factor

        PlotPoint.initConfigFile = \
            'resources/FrequencyDependenceOfSystemBathCoupling.ini'
        if not isfile(PlotPoint.initConfigFile):
            raise RuntimeError(
                "could not find ini file {}".format(PlotPoint.initConfigFile))

        self.config = ConfigParser()
        if not self.config.read(PlotPoint.initConfigFile):
            raise IOError(
                "failed to read configuration file ", PlotPoint.initConfigFile)

        debug(
            "Initial configuration parameters read in from {}".
            format(self.initConfigFile))
        self.printConfigParams(self.config)

        # spatial dimensionality
        self.N_DIM = self.config.getint('TOPOLOGY', 'N_DIM', fallback=1)
        # number of atoms in extended molecule (including surfaces of left and
        # # right bulk solids)

        self.N_ATOMS = self.config.getint(
            'TOPOLOGY', 'N_ATOMS', fallback=3)

        # mass of particle, in a.m.u. (attribute of Particle class)

        self.M0 = self.config.getfloat('PARTICLES', 'M0', fallback=16)
        self.m = np.ones(self.N_ATOMS)  # mass of each atom, in a.m.u.
        # position of each atom, in Angstroms
        self.x = np.zeros(shape=(self.N_ATOMS, self.N_DIM))
        # velocity of each atom, in Angstroms per picosecond
        self.v = np.zeros(shape=(self.N_ATOMS, self.N_DIM))
        # acceleration of each atom, in Angstroms per picosecond squared
        self.a = np.zeros(shape=(self.N_ATOMS, self.N_DIM))

        self.binaryForces = np.zeros(
            shape=(self.N_ATOMS, self.N_ATOMS))

        self.N_BATHS = self.config.getint('BATHS', 'N_BATHS', fallback=2)
        # mass of surface-representation particle, in a.m.u. (attribute of
        # Particle class)
        self.M_B = self.config.getfloat('BATHS', 'M_B')
        # magnitude of particle-surface interaction, in wavenumbers (attribute of
        # Interaction class). Default = 8000
        self.V0 = self.config.getfloat(
            'INTERACTION', 'V0', fallback=8000) * \
            self.WAVENUMBER_TO_SYS_UNITS
        # exponentially decaying interaction extinction coefficient in inverse
        # Angstroms (attribute of Interaction class)
        self.A = self.config.getfloat('INTERACTION', 'A', fallback=1)
        # effective spring constant of the restoring force, in wavenumbers
        # (attribute of Interaction class).
        self.V0A2 = float(0)
        # true if the harmonic approximation is to be taken
        self.HARMONIC_APPROX = self.config.getboolean(
            'INTERACTION', 'HARMONIC_APPROX', fallback=False)
        # Debye frequency bath, in wavenumbers (attribute of Bath class). Default
        # = 400 (No support for Wide Band Approximation, Ohmic bath)
        self.OMEGA_D = self.config.getfloat('BATHS', 'OMEGA_D', fallback=400)
        # inverse of bath memory duration, in wavenumbers (attribute of Bath
        # class). Default = 0 (Markovian bath)
        self.OMEGA_M = self.config.getfloat('BATHS', 'OMEGA_M', fallback=0)
        # oscillation frequency of surface-representation particle, in wavenumbers
        # (attribute of Interaction class). Cannot be larger than Debye frequency.
        self.OMEGA_B = self.config.getfloat('BATHS', 'OMEGA_B', fallback=200)
        # coupling strength of surface-representation particle to respective bath,
        # in wavenumbers (attribute of BathInteraction class). Cannot be larger
        # than or comparable with $\Omega_B$, since this is a weak-coupling
        # model.
        self.GAMMA_B = self.config.getfloat(
            'INTERACTION', 'GAMMA_B', fallback=20)
        # non-Markovian bath force, in a.m.u. times Angstroms per picosecond
        # squared
        self.Rprev = np.zeros(shape=(self.N_BATHS, self.N_DIM))

        # temperature of the cold bath, in Kelvin (attribute of Bath class)
        self.T_R = self.config.getfloat('BATHS', 'T_R', fallback=0)
        # temperature bias between the baths, in Kelvin
        self.DELTA_T = self.config.getfloat('BATHS', 'DELTA_T', fallback=0)
        # temperature of the hot bath, in Kelvin
        self.T_L = self.config.getfloat('BATHS', 'T_L', fallback=0)

        # duration of each time step, in picoseconds
        self.dt = float(1) * self.PICOSECONDS_TO_SYS_UNITS
        self.STEPS_PER_PERIOD = self.config.getint(
            'MD', 'STEPS_PER_PERIOD', fallback=50)
        self.N_SLOW_PERIODS = 1200 * \
            self.config.getfloat('MD', 'N_SLOW_PERIODS', fallback=3)
        self.MAX_ALLOWED_TIME_STEPS = self.config.getint(
            'MD', 'MAX_ALLOWED_TIME_STEPS', fallback=1e8)
        self.n_steps = 1  # numer of time steps
        self.t = 0  # simulated time, in time-steps

        # initial spacing of the atoms, in multiples of A (see above)
        self.INITIAL_SPACING = self.config.getfloat(
            'PARTICLES', 'INITIAL_SPACING', fallback=10)
        # initial (should be equilibrium) positions, saved as independent
        # variable. See builtin copy.deepcopy()
        self.x0 = np.copy(self.x)

        # show phase data every (self.n_steps / self.COARSE_GRAINING_RESOLUTION)
        # steps
        self.COARSE_GRAINING_RESOLUTION = self.config.getfloat(
            'MD', 'COARSE_GRAINING_RESOLUTION', fallback=1)
        self.t_n = 0  # coarse-graining segment
        self.MIN_ALLOWED_TIME_STEPS = self.config.getint(
            'MD', 'MIN_ALLOWED_TIME_STEPS', fallback=2)
        self.x0t = np.zeros(shape=(self.MIN_ALLOWED_TIME_STEPS, self.N_ATOMS))
        self.v0t = np.zeros(shape=(self.MIN_ALLOWED_TIME_STEPS, self.N_ATOMS))

        self.Rt = np.zeros(
            shape=(self.MIN_ALLOWED_TIME_STEPS, self.N_BATHS, self.N_DIM))
        self.E = np.zeros(
            shape=(self.MIN_ALLOWED_TIME_STEPS, self.N_ATOMS))
        self.T = np.zeros(
            shape=(self.MIN_ALLOWED_TIME_STEPS, self.N_ATOMS))
        self.Esys = np.zeros(self.MIN_ALLOWED_TIME_STEPS)
        self.Psys = np.zeros(self.MIN_ALLOWED_TIME_STEPS)

        self.IS_DEBUG = self.config.getboolean(
            'DEBUG', 'IS_DEBUG', fallback=False)
        self.DEBUG_WHAT = []
        self.IS_NUMBA = self.config.getboolean(
            'DEBUG', 'IS_NUMBA', fallback=False)
        self.IS_BATCH = self.config.getboolean(
            'DEBUG', 'IS_BATCH', fallback=False)
        self.IS_NONINTERACTIVE = PlotPoint.isCloudCompute or self.IS_BATCH
        self.IS_MULTITHREADING = self.config.getboolean(
            'DEBUG', 'IS_MULTITHREADING', fallback=True)

        if self.IS_NONINTERACTIVE:
            backend_name = 'svg'
            matplotlib.use(backend_name)  # http://stackoverflow.com/a/3054314
            plt.ioff()
        else:
            plt.ion()
        debug("matplotlib interactive? {}, backend: {}".format(matplotlib.is_interactive(), matplotlib.get_backend()))

        # In[34]:
        if not self.IS_NONINTERACTIVE:

            debug("binding non-interactive run()")

            from numba import jit

            self.run_numba = self.run
            if PlotPoint.IS_NUMBA:

                debug("binding non-interactive run() using Numba JIT")

                # optionally add: argtypes=...,restype=...
                self.run_numba = jit(self.run)
        else:
            self.run_numba = self.run

        info("starting calculations")
        if not self.IS_BATCH:
            info("NVE")
            self.nve()
            info("NVT")
            self.nvt0()
            info("NVT_1>T_2=0")
            self.nvt1gtt2eq0()
        info("Scanning frequency dependence")
        self.scan()

    # ## Initialization:

    # In[18]:

    def init_Topology(self):
        self.N_DIM = 1
        self.N_ATOMS = 3

    def systemEnergyToTime(self, energyInSysUnits):
        energyInWavenumbers = energyInSysUnits / \
            self.WAVENUMBER_TO_SYS_UNITS
        timeInPicoseconds = energyInWavenumbers * \
            self.SPEED_OF_LIGHT_IN_CM_PER_SEC
        timeInSysUnits = timeInPicoseconds * self.PICOSECONDS_TO_SYS_UNITS
        return timeInSysUnits

    @staticmethod
    def isnearlyzero(x, atol=1e-14):
        return np.isclose(x, 0, atol=atol)

    def init_Time(self, steps=0, isDebug=True):
        # if isDebug:
        debug("V0 = {}, A = {}, V0 \* A{} = {}, self.STEPS_PER_PERIOD = {}, self.N_SLOW_PERIODS = {}".format(self.V0,
                                                                                                             self.A, str(r'$^2'), self.V0A2, self.STEPS_PER_PERIOD, self.N_SLOW_PERIODS))

        frequencySet = {self.V0A2 / self.M0, self.OMEGA_M, self.OMEGA_B}
        if not self.isnearlyzero(self.GAMMA_B):
            frequencySet.add(self.GAMMA_B)
        frequencies = np.array(list(frequencySet))

        # TODO: use np.ma.MaskedArray instead of copying array
        # recreate array, keeping only finite frequencies
        frequencies = np.array(frequencies[np.isfinite(frequencies)])
        # recreate array, keeping only non-zero frequencies
        frequencies = np.array(frequencies[np.nonzero(frequencies)])
        debug("frequencies = {}".format(frequencies))
        maxOmega = frequencies.max()
        minOmega = frequencies.min()

        self.dt = float(1) / maxOmega
        self.dt /= self.STEPS_PER_PERIOD
        debug("dt = {} = {} ps".format(
            self.dt, self.dt / self.PICOSECONDS_TO_SYS_UNITS))

        slow_component_period = float(1) / minOmega
        if PlotPoint.isnearlyzero(slow_component_period):
            raise RuntimeError("slow component period cannot be zero!")
        if isDebug:
            debug("slow_component_period = {} = {} ps".format(
                slow_component_period, slow_component_period / self.PICOSECONDS_TO_SYS_UNITS))

        if (0 == steps):
            steps = int(self.N_SLOW_PERIODS * slow_component_period / self.dt)
        if(0 < steps):
            if(self.n_steps < steps):
                self.n_steps = steps
        debug("self.n_steps = {}".format(self.n_steps))

        if (self.MIN_ALLOWED_TIME_STEPS > self.n_steps):
            critical("slow_component_period = {}, self.N_SLOW_PERIODS = {}, self.dt = {}, self.n_steps = {}".format(
                slow_component_period, self.N_SLOW_PERIODS, self.dt, self.n_steps))
            raise RuntimeError(
                "trivial number of time-steps. No point in continuing...")

        if (self.MAX_ALLOWED_TIME_STEPS < self.n_steps):
            steps = self.n_steps
            self.n_steps = 0
            raise RuntimeError("expected number of time steps is too large! (self.N_SLOW_PERIODS = {} \* slow_component_period = {} / self.dt = {}) = {} > {}".format(
                self.N_SLOW_PERIODS, slow_component_period, self.dt, steps, self.MAX_ALLOWED_TIME_STEPS))

        self.t = 0

        info("Time initialization completed")

    def init_Particles(self, m0=0, isDebug=False):

        if(0 < m0):
            self.M0 = m0

        self.m = np.ones(self.N_ATOMS) * self.M0
        if isDebug:
            debug("[m] = {0}, dim(m) ={1}".format(type(self.m), self.m.shape))

    def init_PhaseData(self, isDebug=False):

        self.x = np.zeros(shape=(self.N_ATOMS, self.N_DIM))
        # self.x[i:0] = np.arange(self.N_ATOMS)[i]
        for i in range(self.N_ATOMS):
            # set at starting positions (equilibrium, for the atoms of the
            # molecule)
            self.x[i, 0] = self.INITIAL_SPACING * i / self.A
        # save equilibrium positions as independent variable. See Python
        # builtin copy.deepcopy()
        self.x[1, 0] += float(0.1) / self.A  # initial displacement...
        self.x0 = np.copy(self.x)
        if isDebug:
            debug("[x] = {0}, dim(x) ={1}".format(type(self.x), self.x.shape))

        self.v = np.zeros(shape=(self.N_ATOMS, self.N_DIM))
        if isDebug:
            debug("v[:,0] = {}".format(self.v[:, 0]))

        self.a = np.zeros(shape=(self.N_ATOMS, self.N_DIM))
        if isDebug:
            debug("a[:,0] = {}".format(self.a[:, 0]))

        info("PhaseData initialization completed")

        if isDebug:
            debug("Initial configuration for {0} atoms in {1} dimensions:".
                  format(self.N_ATOMS, self.N_DIM))
            np.set_printoptions(precision=3)
            self.printPhaseConfiguration()

    def init_MD(self, m0, isDebug=False):
        self.init_Topology()
        self.init_Time(isDebug=isDebug)
        self.init_Particles(m0, isDebug)
        self.init_PhaseData(isDebug)
        self.init_Output(isDebug)
        info("MD initialization completed")

    # In[19]:

    def init_Output_CoarseGraining(self, isDebug=False):
        # Important! Don't over-do it so as not to overload the CPU
        MAX_PLOT_POINTS = 300
        if(MAX_PLOT_POINTS < self.n_steps):
            self.COARSE_GRAINING_RESOLUTION = int(
                self.n_steps / MAX_PLOT_POINTS)
        if self.COARSE_GRAINING_RESOLUTION > self.n_steps:
            # raise RuntimeError("COARSE_GRAINING_RESOLUTION ({0}) cannot be greater than self.n_steps ({1})!".format(COARSE_GRAINING_RESOLUTION, self.n_steps))
            self.COARSE_GRAINING_RESOLUTION = min(
                self.COARSE_GRAINING_RESOLUTION, self.n_steps)
        if (1 > self.COARSE_GRAINING_RESOLUTION):
            self.COARSE_GRAINING_RESOLUTION = 1
        if isDebug:
            debug("COARSE_GRAINING_RESOLUTION = {0}".format(
                self.COARSE_GRAINING_RESOLUTION))

        self.t_n = 0

    def init_Output_PhaseData(self, isDebug=False):
        self.x0t = np.zeros(
            shape=(3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION, self.N_ATOMS))
        """
        for i in range(self.N_ATOMS):
            self.x0t[0,i] = self.x[i,0]
        """
        """
        self.x0t[0] = self.x[:,0].transpose()
        """
        if isDebug:
            debug("x0t.shape =", self.x0t.shape)

        self.v0t = np.zeros(
            shape=(3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION, self.N_ATOMS))
        """
        for i in range(self.N_ATOMS):
            self.v0t[0,i] = self.v[i,0]
        """
        if isDebug:
            debug("v0t.shape =", self.v0t.shape)

        if(self.x0t.shape != self.v0t.shape):
            raise RuntimeError(
                "coarse-grained phase data must have the same dimensionality for both position ({0})        and momentum ({1})!".format(self.x0t.shape, self.v0t.shape))

    def init_Output_Mechanics(self):
        self.Psys = np.zeros(
            3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION)
        for i in range(self.N_ATOMS):
            self.Psys[0] += self.m[i] * self.v[i, 0]
        self.Rt = np.zeros(
            shape=(3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION, self.N_BATHS, self.N_DIM))
        debug("Rt.shape = {}".format(self.Rt.shape))

    def init_Output_Energetics(self):
        self.E = np.zeros(
            shape=(3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION, self.N_ATOMS))
        self.Esys = np.zeros(
            3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION)
        for i in range(self.N_ATOMS):
            self.E[0, i] = self.calcAtomEnergy(i)
            self.Esys[0] += self.E[0, i]

    def init_Output_Thermodynamics(self, isDebug=False):
        self.T = np.zeros(
            shape=(3 + self.n_steps / self.COARSE_GRAINING_RESOLUTION, self.N_ATOMS))
        """
        for i in range(self.N_ATOMS):
            self.T[0, i] = self.m[i] * np.dot(self.v[i],v[i]) * self.KELVIN_TO_SYS_UNITS
        """
        if isDebug:
            debug("T[0] = {}".format(self.T[0]))

    def init_Output(self, isDebug=False):
        self.init_Output_CoarseGraining(isDebug)
        self.init_Output_PhaseData(isDebug)
        self.init_Output_Mechanics()
        self.init_Output_Energetics()
        self.init_Output_Thermodynamics(isDebug)
        info("Outputs initialization completed")

    # In[20]:

    def init_Interaction(self, v0, a, harmonic, isDebug=True):
        if (0 < v0):
            self.V0 = v0 * self.WAVENUMBER_TO_SYS_UNITS
        if isDebug:
            debug("{} = {} {}".format(
                str(r'$V_0$'), self.V0 / self.WAVENUMBER_TO_SYS_UNITS, str(r'cm $^{-1}$')))

        if(0 < a):
            self.A = a
        if isDebug:
            debug(
                "{} = {} {}".format(str(r'$a$^{-1}$'), float(1) / self.A, str(r'$\AA$')))

        self.V0A2 = self.V0 * self.A * self.A
        if isDebug:
            # , str(r'cm $^{-1}$')))
            debug("{} = {}".format(str(r'$V_0a^2$'), self.V0A2))

        if (harmonic is not None):
            self.HARMONIC_APPROX = harmonic
        if isDebug:
            debug("HARMONIC_APPROX? {}".format(str(self.HARMONIC_APPROX)))

    def init_Bath(self, isColdBath, temperature, isDebug=False):
        if(isColdBath):
            self.T_R = float(temperature)
            self.T_R *= self.KELVIN_TO_SYS_UNITS
        else:
            self.DELTA_T = float(temperature)
            self.DELTA_T *= self.KELVIN_TO_SYS_UNITS
            self.T_L = self.T_R + self.DELTA_T

        if isDebug:
            debug("{0} = {1}".format(
                "T_R" if isColdBath else "T_L", self.T_R if isColdBath else self.T_L))

    def init_Baths(self, m_B, omega_D, omega_M, omega_B, gamma_B, Temp_R, Delta_T, isDebug=False):
        if(0 < m_B):
            self.M_B = float(m_B)

        if(0 < omega_D):
            self.OMEGA_D = omega_D * self.WAVENUMBER_TO_SYS_UNITS
        if isDebug:
            debug("{0} = {1} cm{2} --> {3} system units".format(str(r'$\Omega_D$'),
                                                                self.OMEGA_D / self.WAVENUMBER_TO_SYS_UNITS, str(r'$^{-1}$'), self.OMEGA_D))

        if(0 < omega_M):
            self.OMEGA_M = omega_M * self.WAVENUMBER_TO_SYS_UNITS
        # Ornstein-Uhlenbeck model: $argmax(\gamma(\omega)) =
        # argmax(\exp(-\omega / \omega_M)) = omega_M / 2$
        if isDebug:
            debug("{0} = {1} cm{2} --> {3} system units".format(str(r'$\Omega_M$'),
                                                                self.OMEGA_M / self.WAVENUMBER_TO_SYS_UNITS, str(r'$^{-1}$'), self.OMEGA_M))

        self.OMEGA_B = self.OMEGA_D
        if(0 < omega_B):
            self.OMEGA_B = omega_B * self.WAVENUMBER_TO_SYS_UNITS
        if (0 < self.OMEGA_D) and (self.OMEGA_D < self.OMEGA_B):
            raise RuntimeError(
                "Bath 'atom' vibration frequency ({})        cannot be larger than the Debye frequency ({})!".format(self.OMEGA_B, self.OMEGA_D))
        if isDebug:
            debug("{0} = {1} cm{2} --> {3} system units".format(str(r'$\Omega_B$'),
                                                                self.OMEGA_B / self.WAVENUMBER_TO_SYS_UNITS, str(r'$^{-1}$'), self.OMEGA_B))

        if(0 < gamma_B):
            self.GAMMA_B = gamma_B * self.WAVENUMBER_TO_SYS_UNITS
        if (self.OMEGA_B <= self.GAMMA_B * 10):
            warning(
                "Bath 'atom' coupling ({}) cannot be larger or comprable to vibration frequency ({}),        within weak-coupling!".format(self.GAMMA_B, self.OMEGA_B))
        if isDebug:
            debug("{0} = {1} cm{2} --> {3} system units".format(str(r'$\Gamma_B$'),
                                                                self.GAMMA_B / self.WAVENUMBER_TO_SYS_UNITS, str(r'$^{-1}$'), self.GAMMA_B))

        if(0 < Temp_R):
            self.init_Bath(True, Temp_R, isDebug)

        if(0 < Delta_T):
            self.init_Bath(False, Delta_T, isDebug)

        info("Baths initialization completed: {} = {} + {}".format(
            self.T_L / self.KELVIN_TO_SYS_UNITS,
            self.T_R / self.KELVIN_TO_SYS_UNITS,
            self.DELTA_T / self.KELVIN_TO_SYS_UNITS))

    # In[21]:

    def init(self, m0=0, m_B=0, omega_D=0, omega_M=0, omega_B=0, gamma_B=0, Temp_R=0, Delta_T=0, v0=0, a=0, harmonic=None, isDebug=True):
        """
        initialize parameters and variables in order of prerequisites / dependence
        """
        info("initialization in progress:")
        self.init_Baths(
            m_B, omega_D, omega_M, omega_B, gamma_B, Temp_R, Delta_T, isDebug)
        self.init_Interaction(v0, a, harmonic, isDebug)
        if isDebug:
            debug(
                "DBG: self.V0A2 = {} \* {}^2 = {}".format(self.V0, self.A, self.V0A2))
        self.init_MD(m0, isDebug)
        if isDebug:
            debug(
                "DBG: self.V0A2 = {} \* {}^2 = {}".format(self.V0, self.A, self.V0A2))
        info("initialization completed")
        sys.stdout.flush()

    # ## Interaction and derived functions:

    # In[22]:

    def generateMarkovianRandomForce(self, isColdBath):
        # random.random() # draw random number from the uniform distribution
        # over the range [0.0, 0.1)
        T_K = self.T_R if isColdBath else self.T_L
        sigma_sq = 2 * self.M_B * self.GAMMA_B * T_K / self.dt
        if (PlotPoint.isnearlyzero(sigma_sq)):
            return 0
        # print  "DEBUG:", self.M_B, self.GAMMA_B, T_K, sigma_sq, random.gauss(0,
        # math.sqrt(sigma_sq))
        return np.random.normal(0, math.sqrt(sigma_sq))

    def interact(self, x, isHarmonic):
        if isHarmonic:
            return (self.V0 * self.A * self.A) * ((abs(x) - float(1) / self.A)) * ((abs(x) - float(1) / self.A))

        try:
            return self.V0 * math.exp(-self.A * abs(x))
        except OverflowError as e:
            raise OverflowError(e, x, self.V0, self.A)

    def force(self, x_this, x_that, isHarmonic, isDebug=False):

        d = x_that - x_this
        V = self.interact(d, isHarmonic)

        if isHarmonic:
            if(PlotPoint.isnearlyzero(d)):
                f = 0
            else:
                f = -1. * V / d
        else:
            if(PlotPoint.isnearlyzero(d)):
                raise ZeroDivisionError(
                    "Pauli exclusion exception! x_this = {0} == {1} = x_that".format(x_this, x_that))

            f = -self.A * V * (d / abs(d))

        if(isDebug):
            debug("x_this = {0}, x_that = {1}, d = {2}, isHarmonic = {3}, V = {4}, f = {5}".format(
                x_this, x_that, d, isHarmonic, V, f))

        return f

    def calcMarkovianBathForce(self, v, bathIndex, isDebug=False):

        try:
            for d in range(self.N_DIM):

                # random force term (fluctation)
                R = self.generateMarkovianRandomForce(1 == bathIndex)

                # friction force term (dissipation)
                self.Rt[self.t_n, bathIndex, d] = R - \
                    self.M_B * self.GAMMA_B * self.v[d]

                # prints all coarse-graining time-segments where the Markovian
                # bath force was zero, although bath temperature > 0
                if(isDebug):
                    if(PlotPoint.isnearlyzero(self.Rt[self.t_n, bathIndex, d])):
                        debug(self.t_n)

        except ValueError as e:
            raise RuntimeError(
                e, self.M_B, self.GAMMA_B, v, R, self.t_n, bathIndex, self.Rt[self.t_n, bathIndex])

        return self.Rt[self.t_n, bathIndex]

    def calcOrnsteinUhlnbeckForce(self, v, bathIndex):

        try:
            memory_propagator = math.exp(-self.OMEGA_M * self.dt)

            for d in range(self.N_DIM):

                # integrated memory term
                R = self.Rprev[bathIndex, d]
                R *= memory_propagator

                # integrated random force term
                R_Markovian = self.generateMarkovianRandomForce(1 == bathIndex)
                R += (R_Markovian - self.M_B * self.GAMMA_B *
                      self.v[d]) * (1 - memory_propagator)

                # save memory
                self.Rprev[bathIndex, d] = R

                self.Rt[self.t_n, bathIndex] = R

        except ValueError as e:
            raise RuntimeError(
                e, self.M_B, self.GAMMA_B, v, R_Markovian, self.t_n, bathIndex, self.Rt[self.t_n, bathIndex])

        return R

    def calcBathForce(self, v, isColdBath, isDebug=False):
        # TODO: pass boolean to check that self.Rt indeed needs updating, i.e. that
        # (0 == self.t % segment_length)

        bathIndex = 1 if isColdBath else 0

        # no coupling
        if(PlotPoint.isnearlyzero(self.GAMMA_B)):
            self.Rt[self.t_n, bathIndex].fill(0)
            return self.Rt[self.t_n, bathIndex]

        # Markovian bath
        if(PlotPoint.isnearlyzero(self.OMEGA_M)):
            return self.calcMarkovianBathForce(v, bathIndex, isDebug=(isDebug and (0 < self.T_R if isColdBath else self.T_L)))

        # Non-Markovian bath interaction
        return self.calcOrnsteinUhlnbeckForce(v, bathIndex)

    def restraintPosition(self, isColdBath, isDebug=False):

        if(isColdBath):
            """
            if((1e-2 / A) > abs(self.x[i]))):
                return 0
            """
            return self.force(self.x[self.N_ATOMS - 1], -1. / self.A, isHarmonic=True, isDebug=isDebug)
        """
        if ((1e-3 / A) > abs(((self.N_ATOMS - 1) / A) - self.x[i])):
            return 0
        """
        return self.force(self.x[0], self.N_ATOMS / self.A, isHarmonic=True, isDebug=isDebug)

    # ## Mechanics:

    # In[23]:

    def calcBinaryForces(self, i, isDebug=False):

        f = float(0)
        for j in range(self.N_ATOMS):

            if(i == j):
                continue

            if(j > i):

                try:
                    self.binaryForces[i, j] = self.force(
                        self.x[i], self.x[j], self.HARMONIC_APPROX, isDebug)
                except OverflowError as e:
                    raise OverflowError(e, i, self.x[i], j, self.x[j], f)
                except ZeroDivisionError as e:
                    raise ZeroDivisionError(e, i, self.x[i], j, self.x[j], f)

                f += self.binaryForces[i, j]
                continue

            f -= self.binaryForces[j, i]

        return f

    def calcNetForce(self, i, isDebug=False):

        f = self.calcBinaryForces(i)

        if((0 != i) and ((self.N_ATOMS - 1) != i)):
            return f

        f_B = self.calcBathForce(
            self.v[i], self.N_ATOMS - 1 == i, isDebug=True)

        if isDebug:
            debug("DEBUG: ", i, self.v[i], f_B)
        """
        f += f_B
        f += restraintPosition(0 == i, isDebug)
        """

        if (PlotPoint.isnearlyzero(self.v[i])):
            if (0 < self.t):
                bathTemp = self.T_L if (0 == i) else self.T_R
                if(0 < bathTemp):
                    raise RuntimeError(
                        "bath temp = {} > 0, but bath atom velocity vector ({}) is close to zero! (bath force = {})".format(bathTemp, self.v[i], f_B))

        return f_B

    # ## Kinematics:

    # In[24]:

    def accelerate(self, i):
        return self.calcNetForce(i) / self.m[i]

    def velocityVerlet(self, i, isDebug=False):
        '''
        This form assumes calculation of acceleration depends only on position, and not on velocity
        '''
        x_full = self.x[i] + self.v[i] * \
            self.dt + 0.5 * self.a[i] * self.dt * self.dt
        # x_half = 0.5 * (self.x[i] + x_full)
        a_full = self.accelerate(i)
        v_full = self.v[i] + 0.5 * (self.a[i] + a_full) * self.dt

        if(isDebug):
            debug("x_full = {} , v_full = {}, a_full = {}".format(
                x_full, v_full, a_full))

        return (x_full, v_full, a_full)

    # ## Conservation:

    # In[25]:

    def calcTimeAvgAtomT(self, i):
        '''
        Returns the time-averaged atomic temperature in KELVIN, assuming $dV/dv_i = 0$ and ergodicity.
        '''
        if (0 == i and 0 == self.T_L) or (self.N_ATOMS - 1 == i and 0 == self.T_R):
            return 0

        timeAvgVelocitySquared = pow(
            np.linalg.norm(self.v0t[i]), 2) / self.v0t[i].size
        return self.m[i] * timeAvgVelocitySquared / self.KELVIN_TO_SYS_UNITS

    def calcAtomEnergy(self, i):
        retval = 0.5 * self.m[i] * abs(self.v[i]) * abs(self.v[i])
        for j in range(self.N_ATOMS):

            if (i == j):
                continue

            try:
                retval += self.interact(self.x[j] -
                                        self.x[i], self.HARMONIC_APPROX)
            except OverflowError as e:
                raise OverflowError(e, i, self.x[i], j, self.x[j], retval)

            # if((0 == i) or ((self.N_ATOMS - 1) == i)): calc bath interaction
            # energy

        return retval

    def calcSystemEnergy(self):
        tot = 0
        for i in range(self.N_ATOMS):
            tot += self.calcAtomEnergy(i)
        return tot

    def calcSystemMomentum(self):
        tot = np.zeros(shape=(self.N_DIM))
        for i in range(self.N_ATOMS):
            # scalar multiplication of vectors (using 'transpose') is
            # equivalent to the Python list builtin 'zip'
            tot += self.m[i] * self.v[i].transpose
        return tot

    # ## Heat current:

    # In[26]:

    def calcHeatCurrent(self):
        heatCurrent = 0
        for i in range(self.N_ATOMS - 1):
            heatCurrent += 0.5 * \
                self.binaryForces[i, i + 1] * (self.v[i] + self.v[i + 1])
        return heatCurrent

    def calcHeatConductivity(self):
        if(PlotPoint.isnearlyzero(self.DELTA_T)):
            raise ZeroDivisionError(
                "Zero temperature gradient -- can't calculate conductivity!")
        return self.calcHeatCurrent() / self.DELTA_T

    def calcDiffHeatConductivity(self):
        pass

    # ## Output:

    # In[27]:

    def printPhaseConfiguration(self, threadIndex=None):
        print
        for i in range(self.N_ATOMS):
            # print "t =", self.t, "; i =", i, "; self.x[i] =", self.x[i], "; self.v[i]
            # =", self.v[i]
            if threadIndex is not None:
                debug("proc# = {}, th# = {}, self.t = {} = {} ps, self.t_n - 1 = {} ps, i = {}, self.x[i] = {}, <x[i]> = {}, self.v[i] = {}".format(
                    self.m_factor, threadIndex, self.t, self.t * self.dt / self.PICOSECONDS_TO_SYS_UNITS, (self.t_n - 1) * self.dt / self.PICOSECONDS_TO_SYS_UNITS, i, self.x[i], self.x0t[self.t_n - 1, i], self.v[i]))
            else:
                debug("t = {} = {} ps, self.t_n - 1 = {} ps, i = {}, self.x[i] = {}, <x[i]> = {}, self.v[i] = {}".format(
                    self.t, self.t * self.dt / self.PICOSECONDS_TO_SYS_UNITS, (self.t_n - 1) * self.dt / self.PICOSECONDS_TO_SYS_UNITS, i, self.x[i], self.x0t[self.t_n - 1, i], self.v[i]))

    # In[28]:

    # In[29]:

    # In[30]:

    def plotTimeSeries(self):
        # takes self, data, label, units, title, conversionFactor
        # uses self.fig, self.nSubplots, self.subplotIndex, self.t_n,
        # self.t_max, self.t_grid, self.atom_series_colors
        self.subplotIndex += 1
        pass

    def plotTrajectory(self, isDebug=False):

        debug("in plotTrajectory (debug={}) noninteractive={}".format(
            isDebug, self.IS_NONINTERACTIVE))

        #if self.IS_NONINTERACTIVE:
        #    return

        # plt.plot(t, self.x0t, c=range(self.N_ATOMS), alpha=0.5)

        # object-oriented pyplot
        # use plot.ly to make figures interactive: plotly.plotly.plot_mpl(fig)
        fig = plt.figure(figsize=(12, 12), dpi=300)
        fig.suptitle('Frequency Dependence of System-Bath Coupling')

        debug("setting up time-grid")
        t_max = self.t_n * self.COARSE_GRAINING_RESOLUTION * \
            self.dt / self.PICOSECONDS_TO_SYS_UNITS
        t_grid = np.linspace(0, t_max, self.t_n)

        if isDebug:
            debug(self.t_n)
            debug(t_grid.shape)
            debug(self.x0t[:self.t_n].shape)

        xdomain = np.linspace(0, 1, self.N_ATOMS + 1)
        myCM = cm.get_cmap("hsv")
        atom_series_colors = itertools.cycle(myCM(xdomain))

        nSubplots = 8
        self.subplotIndex = 0

        info("plotting x(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title(
            'Trajectory: self.x[0,i] vs. self.t, for all i in (1,N_ATOMS)')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel(r'x/[$\AA$]')  # use raw string and latex

        for i in range(self.N_ATOMS):
            series_label = '_'.join(["x", str(i)])
            atom_color = next(atom_series_colors)
            ax1.scatter(
                t_grid,
                self.x0t[:self.t_n, i],
                color=atom_color,
                s=5,
                cmap=plt.cool,
                alpha=0.5,
                label=series_label)
        # float(self.t_n) * self.COARSE_GRAINING_RESOLUTION * self.dt / self.PICOSECONDS_TO_SYS_UNITS,
        # plt.legend(['x_{}(t)'.format(i) for i in range(self.N_ATOMS)], loc=2, bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=11)
        plt.legend(['x_{}(t)'.format(i) for i in range(self.N_ATOMS)])
        next(atom_series_colors)

        info("plotting v(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title(
            'Trajectory: self.v[0,i] vs. self.t, for all i in (1,N_ATOMS)')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel(r'v/[$\AA$ / ps]')  # use raw string and latex

        for i in range(self.N_ATOMS):
            series_label = '_'.join(["v", str(i)])
            atom_color = next(atom_series_colors)
            ax1.scatter(
                t_grid,
                self.v0t[:self.t_n, i],
                color=atom_color,
                s=5,
                cmap=plt.cool,
                alpha=0.5,
                label=series_label)

        next(atom_series_colors)

        info("plotting v(x)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title(
            'Phase-space trajectory: self.v[0,i] vs. self.x[0,i], for all i in (1,N_ATOMS)')
        plt.xlabel(r'x/[$\AA$]')
        plt.ylabel(r'v/[$\AA$ / ps]')
        for i in range(self.N_ATOMS):
            series_label = '_'.join(["v", str(i)])
            atom_color = next(atom_series_colors)
            for t_p in range(1, self.t_n):
                ax1.scatter(
                    self.x0t[t_p, i],
                    self.v0t[t_p, i] / self.PICOSECONDS_TO_SYS_UNITS,
                    color=atom_color,
                    s=5,
                    alpha=0.5,
                    label=series_label)

        next(atom_series_colors)

        info("plotting T_i(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title(
            'Atomic temperatures: self.T[i] vs. self.t, for all i in (1,N_ATOMS)')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel('T/[K]')
        for i in range(self.N_ATOMS):
            series_label = '_'.join(["T", str(i)])
            series_color = next(atom_series_colors)
            ax1.scatter(
                t_grid,
                self.T[:self.t_n, i] / self.KELVIN_TO_SYS_UNITS,
                color=series_color,
                s=5,
                alpha=0.5,
                label=series_label)

        next(atom_series_colors)

        info("plotting E_i(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title(
            'Atomic energies: self.E[i] vs. self.t, for all i in (1,N_ATOMS)')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel('E/[cm$^{-1}$]')
        for i in range(self.N_ATOMS):
            series_label = '_'.join(["E", str(i)])
            series_color = next(atom_series_colors)
            ax1.scatter(
                t_grid,
                self.E[:self.t_n, i] / self.WAVENUMBER_TO_SYS_UNITS,
                color=series_color,
                s=5,
                alpha=0.5,
                label=series_label)

        next(atom_series_colors)

        info("plotting E(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title('System energy: E vs. t')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel('E/[cm$^{-1}$]')
        # for self.t_n in range(1, int(self.n_steps /
        # self.COARSE_GRAINING_RESOLUTION)):
        ax1.scatter(
            t_grid,
            self.Esys[:self.t_n] / self.WAVENUMBER_TO_SYS_UNITS,
            s=5,
            alpha=0.5,
            label='E_sys')

        debug("plotting P(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title('System momentum: P vs. t')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max)
        plt.ylabel(r'P/[a.m.u. $\AA$ / ps')
        ax1.scatter(
            t_grid,
            self.Psys[:self.t_n],
            s=5,
            alpha=0.5,
            label='P_sys')

        info("plotting R(t)")
        self.subplotIndex += 1
        ax1 = fig.add_subplot(nSubplots, 1, self.subplotIndex)
        ax1.set_title('Bath random force: R vs. t')
        plt.xlabel('t/[ps]')
        plt.xlim(0, t_max - 1)
        plt.ylabel(r'R/[a.m.u. $\AA$ / {ps}^2')
        for bathIndex in range(2):
            series_label = '_'.join(["R", str(i)])
            series_color = next(atom_series_colors)
            ax1.scatter(
                t_grid,
                self.Rt[:self.t_n, bathIndex, 0],
                color=series_color,
                s=5,
                alpha=0.5,
                label='_'.join(["R_0", str(bathIndex)]))

        next(atom_series_colors)

        plt.autoscale(tight=True)

        try:
            fig.tight_layout()
            # fig.tight_layout(pad=2, w_pad=2, h_pad=2) # Don't over-do it
            # fig.tight_layout(w_pad=8, h_pad=4)
        except:
            pass

        plt.subplots_adjust(top=0.9)

        if self.IS_NONINTERACTIVE:
            debug("isCloudCompute? {}".format(PlotPoint.isCloudCompute))
            fname = "out/plots/traj-{}.svg".format(
                datetime.datetime.now().strftime('%Y%m%dT%H%M%S'))
            debug("saving figure under the name {}".format(fname))
            savefig(fname, transparent=True, bbox_inches='tight')
        else:
            info("displaying plots:")
            plt.show()

    # ## Runner

    # In[31]:

    def validateSystemNotMelted(self, i):
        """
        Taking Lindemann constant = 1.0:
        Is self.x[i] on the opposite side of self.x[i - 1] with respect to self.x[i - 2]?

        Using analytical geometry:
        """

        if (2 > i):
            return

        # 1. Calculate slope of line going through self.x[i] and self.x[i - 1]

        """
        # loop:
        slope1 = np.zeros(self.N_DIM)
        for d in range(self.N_DIM):
            slope1[d] = self.x[i, d] - self.x[i - 1, d]

        # listcomp:
        slope1 = [x[i, d] - self.x[i - 1, d] for d in range(self.N_DIM)]
        """

        # generator expression (genexp)
        """
        # a generator is a function that returns an iterator
        # a genexp is an in-place generator (just like a lambda expression is an in-place function)
        # more appropriate (better perfromance) than listcomp if there's not need to iterate more than once or to keep the output list
        """
        # dIter(j) = (self.x[j, d] - self.x[j - 1, d] for d in range(self.N_DIM)) # can't pass argument to non-callable
        # def dIter(j): yield (self.x[j, d] - self.x[j - 1, d] for d in range(self.N_DIM)) #
        # returns generator, not iterator
        dIter1 = (self.x[i, d] - self.x[i - 1, d] for d in range(self.N_DIM))

        slope1 = np.fromiter(dIter1, float, self.N_DIM)

        # 2. Calculate slope of line going through self.x[i - 1] and self.x[i -
        # 2]
        dIter2 = (self.x[i - 1, d] - self.x[i - 2, d]
                  for d in range(self.N_DIM))
        slope2 = np.fromiter(dIter2, float, self.N_DIM)

        # 3. Calculate angle between the two slopes
        theta = math.acos((slope1 * slope2) / (abs(slope1) * abs(slope2)))

        # 4. If the angle between the two slopes is between 1/2 * pi radians
        # and 3/2 * pi radians, self.x[i] and self.x[i - 2] cannot be said to lay on
        # opposite sides of plane. Raise exception
        halfPi = math.pi / 2
        if ((halfPi < theta) and (3 * halfPi > theta)):
            try:
                self.plotTrajectory(True)
            except:
                pass
            raise RuntimeError("system melted! (atom={}, angle={} degrees, time={}\n{}".format(
                i, (float(180) / math.pi) * theta, self.t, self.printPhaseConfiguration()))

    def coarseGrain(self, i):
        self.x0t[self.t_n, i] /= self.COARSE_GRAINING_RESOLUTION
        self.v0t[self.t_n, i] /= self.COARSE_GRAINING_RESOLUTION
        self.E[self.t_n, i] /= self.COARSE_GRAINING_RESOLUTION

        isLastAtom = (self.N_ATOMS - 1 == i)
        if((0 == i) or isLastAtom):
            self.Rt[
                self.t_n, int(isLastAtom)] /= self.COARSE_GRAINING_RESOLUTION

        self.T[self.t_n, i] = self.m[i] * 3 * pow(self.v0t[self.t_n, i], 2)
        self.Esys[self.t_n] += self.E[self.t_n, i]
        self.Psys[self.t_n] = self.m[i] * self.v0t[self.t_n, i]

        if(isLastAtom):
            self.t_n += 1

    def propagate(self, isDebug=False):
        for i in range(self.N_ATOMS):

            try:
                self.x[i], self.v[i], self.a[
                    i] = self.velocityVerlet(i, isDebug)
            except OverflowError as exptn:
                raise OverflowError(exptn, self.t, i, self.x[i], self.v[i])
            except ZeroDivisionError as exptn:
                raise ZeroDivisionError(exptn, self.t, i, self.x[i], self.v[i])
            except RuntimeError as exptn:
                raise RuntimeError(exptn, self.t, i, self.x[i], self.v[i])

            self.validateSystemNotMelted(i)

            self.x0t[self.t_n, i] += self.x[i, 0]
            self.v0t[self.t_n, i] += self.v[i, 0]
            self.E[self.t_n, i] += self.calcAtomEnergy(i)

            isLastAtom = (self.N_ATOMS - 1 == i)
            if((0 == i) or isLastAtom):
                self.Rt[
                    self.t_n, int(isLastAtom)] /= self.COARSE_GRAINING_RESOLUTION

            if(0 == self.t % self.COARSE_GRAINING_RESOLUTION):
                self.coarseGrain(i)

    def finalize(self, start_timer, timer, GammaSum, GammaSquaredSum, isDebug=False):
        info("Final configuration:")
        self.printPhaseConfiguration()

        simulated_time = self.n_steps * self.dt / \
            self.PICOSECONDS_TO_SYS_UNITS
        simulation_duration = timer - start_timer
        info("simulated time = {} ps ; simulation wall-clock duration = {}".format(
            simulated_time, datetime.timedelta(seconds=simulation_duration)))

        time_dilation_ratio = 1e12 * simulation_duration / simulated_time
        if(PlotPoint.isnearlyzero(time_dilation_ratio)):
            warning("time dilation is zero!")
        else:
            debug("time dilation = {:.1e}% ; {:.3g} ps per minute".format(
                100. * time_dilation_ratio, 60 * 1e12 / time_dilation_ratio))

        if not PlotPoint.isnearlyzero(self.T_L):

            if isDebug:
                if (not (0 != self.Rt).any()):  # or (1 > np.count_nonzero(Rt))
                    warning("all random force terms are identically zero!")

            atomTemperatures = []
            for j in range(self.N_ATOMS):
                atomTemperatures.append(self.calcTimeAvgAtomT(j))
            info(
                "time-averaged atomic temperatures: {}".format(str(atomTemperatures)))

            if(isDebug):
                debug("GammaSum = {}".format(GammaSum))
            # or (1 > np.count_nonzero(Rt)):
            if PlotPoint.isnearlyzero(GammaSum) and (0 < self.GAMMA_B) and ((0 != self.Rt).any()):
                # np.set_printoptions(precision=7)
                raise RuntimeError(
                    "Effective Gamma is exactly zero! ({})".format(GammaSum))

        debug("\Gamma_B = {}".format(self.GAMMA_B))

        # average over bridge atoms
        nBridgeAtoms = self.N_ATOMS - self.N_BATHS
        GammaSum /= nBridgeAtoms
        GammaSquaredSum /= nBridgeAtoms

        # average over time-steps
        GammaSum /= self.n_steps
        GammaSquaredSum /= self.n_steps
        GammaVar = GammaSquaredSum - GammaSum * GammaSum

        self.t_max = self.t_n * self.COARSE_GRAINING_RESOLUTION * \
            self.dt / self.PICOSECONDS_TO_SYS_UNITS
        self.t_grid = np.linspace(0, self.t_max, self.t_n)

        if self.isnearlyzero(self.T_L):
            try:
                param, covar = self.fitDissipation()
                dissipationGammaVar = covar[1, 1]
                if (self.isnearlyzero(GammaVar) or GammaVar > dissipationGammaVar):
                    GammaVar = dissipationGammaVar
                    GammaSum = param[1]
            except ArithmeticError as ae:
                warning(ae, exc_info=True)  # unavoidable. just report, anyway.
            except Exception as e:
                print_exc()
                # avoidable. raise as exception, to be handled and corrected by
                # client code
                warnings.warn(str(e))
            finally:
                try:
                    self.plotTrajectory()
                except:
                    pass

        info("{} = {} / {} = {}".format(r'Var[\Gamma] / t', GammaVar, self.t /
                                        self.PICOSECONDS_TO_SYS_UNITS, GammaVar / (self.t / self.PICOSECONDS_TO_SYS_UNITS)))

        GammaAvg = GammaSum
        return GammaAvg, GammaVar

    # In[32]:

    def calcEffectiveCouplingFrequency(self, atomIndex, isDebug=False):
        """
        Calculates the effective $\gamma(\omega_B)$ applied to the molecular atom/s:
        $ \Gamma (\delta x / self.dt) \equiv \gamma \delta x = m0 \omega ^2 \delta x_K \delta x $
        $ \Gamma = m0 \omega ^2 \delta x_K \delta x / (\delta x / self.dt) $
        """

        isPrintDebug = False
        if(isDebug):
            isPrintDebug = (0 == self.t % (self.n_steps / 10))

        if(2 < self.N_BATHS):
            raise RuntimeError("More than 2 baths are not yet supported!")

        x2 = float(0)
        for i in range(self.N_BATHS):
            bathIndex = 0 if (0 == i) else (self.N_ATOMS - 1)
            x2 += (self.x[bathIndex] - self.x0[bathIndex]) * \
                (self.x[atomIndex] - self.x0[atomIndex])  # scalar product

            if(isPrintDebug):
                debug("{} (K = {}) = ({} - {}) {} ({} - {}) = {}".format(str(r'$\delta x_K \delta x$'), i, self.x[bathIndex], self.x0[
                      bathIndex], str(r'\times'), self.x[atomIndex], self.x0[atomIndex], (self.x[bathIndex] - self.x0[bathIndex]) * (self.x[atomIndex] - self.x0[atomIndex])))

        if PlotPoint.isnearlyzero(x2):
            return 0

        x2 /= self.N_BATHS

        # if not all elements are finite, return 0 (avoid NaN)
        if (not np.all(np.isfinite(self.v[atomIndex]))):
            if(isPrintDebug):
                debug(
                    "| self.v[{}] | = |{}| is not < infty".format(atomIndex, self.v[atomIndex]))
            return 0

        isVelocityZero = True
        for vd in self.v[atomIndex]:
            if not PlotPoint.isnearlyzero(vd):
                isVelocityZero = False
                break
        # if no (finite) element is also nonzero, return 0 (avoid divide by
        # zero). Equivalent to (1 > self.v[atomIndex].nonzero().size)
        if isVelocityZero:
            if(isPrintDebug):
                debug("v[{}] = {} is zero! (elements different from zero: {}. # of such elements = {})".format(
                    atomIndex, self.v[atomIndex], self.v[atomIndex].nonzero(), np.count_nonzero(self.v[atomIndex])))
            return 0

        if(isPrintDebug):
            debug("{}[{}] = {} \* {} / {}".format(str(r'$V_0 a^2 \times x^2 / v$'),
                                                  atomIndex, self.V0A2, x2, self.v[atomIndex]))

        # Lebesque 2-norm (i.e. Eucledean distnace) by default
        return self.V0A2 * x2 / np.linalg.norm(self.v[atomIndex])

    # In[33]:

    @staticmethod
    def calcTimeLeft(recent_duration, accum_duration, segment_no, totSegments, inertia_factor=None):
        '''
        note that if the run aborts before concluding,
        the information it returns would not be correct,
        as actual the totSegments is not the one passed in
        (i.e. what consists the totSegments for a full run)
        '''

        segments_left = totSegments - segment_no

        if (inertia_factor is None):
            inertia_factor = segment_no / totSegments
        elif (0 > inertia_factor) or (1 <= inertia_factor):
            raise RuntimeError("inertia_factor must be in [0,1)")

        return ((accum_duration / segment_no) * inertia_factor + recent_duration * (1 - inertia_factor)) * segments_left

    def run(self, threadIndex=None, isSilent=False, isDebug=None, debugWhat=None):

        info("RUN: @{}".format(datetime.datetime.strftime(
            datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))

        if debugWhat is None:
            debugWhat = self.DEBUG_WHAT
        if isDebug is None:
            isDebug = self.IS_DEBUG
        if isSilent:
            isDebug = False
        info("Silent? {} Debug? {}".format(isSilent, isDebug))

        GammaSum = 0
        GammaSquaredSum = 0

        REPORT_TIMES = max(10, self.n_steps / 1e4)
        timer = 0  # Declaration, just to make the PyPY compiler happy...
        start_timer = 0  # Declaration, just to make the PyPY compiler happy...
        prev_timer = 0

        info("Running {} time-steps:".format(self.n_steps))

        for self.t in range(self.n_steps - 1):

            try:

                self.propagate(isDebug)

                if ((0 == self.t % math.ceil(self.n_steps / REPORT_TIMES)) or (3 > self.t)):
                    timer = time.clock()
                    if (0 == self.t):
                        start_timer = timer
                        time_left = "unknown time"
                    else:
                        # time_left = (timer - prev_timer) * REPORT_TIMES * (1 - self.t / self.n_steps)
                        time_left = PlotPoint.calcTimeLeft(
                            timer - prev_timer, prev_timer, segment_no=(self.t / self.n_steps) * REPORT_TIMES, totSegments=REPORT_TIMES)
                        # debug("DEBUG: self.t = {0}, timer = {1}, prev_timer = {2}, time_left = {3}".format(t, timer, prev_timer, time_left))
                    prev_timer = timer

                    if (str != type(time_left)):
                        if threadIndex is not None:
                            info("#{}-#{}: {}% done, {} left".format(self.m_factor, threadIndex, int(
                                100. * self.t / self.n_steps), datetime.timedelta(seconds=time_left)))
                        else:
                            info("{0}% done, {1} left".format(
                                int(100. * self.t / self.n_steps), datetime.timedelta(seconds=time_left)))
                    if (not isSilent):
                        self.printPhaseConfiguration(threadIndex)
                    sys.stdout.flush()

                if(0 < self.GAMMA_B):
                    for atomIndex in range(self.N_ATOMS):
                        if (0 == atomIndex) or (self.N_ATOMS - 1 == atomIndex):
                            continue
                        currentGamma = self.calcEffectiveCouplingFrequency(
                            atomIndex, isDebug=(True if "effectiveGamma" in debugWhat else False))
                        GammaSum += currentGamma
                        GammaSquaredSum += currentGamma * currentGamma

                if(0 != self.DELTA_T):
                    self.calcHeatConductivity()

            except RuntimeError as re:
                print_exc()
                warnings.warn(str(re))
                break

        GammaAvg, GammaVar = self.finalize(
            start_timer, timer, GammaSum, GammaSquaredSum, isDebug=(not isSilent))

        # <{\gamma}>, {\sigma}_{\gamma}, t_{wall-clock}
        return GammaAvg, math.sqrt(GammaVar), timer - start_timer

    # ## NVE

    # In[40]:
    def nve(self):
        ############################
        # For debugging only:     ##
        self.N_SLOW_PERIODS /= 50
        ############################

        self.init(omega_D=400, gamma_B=20)

        # In[41]:

        if not self.IS_NONINTERACTIVE:
            self.run_numba()

        # In[42]:

        try:
            self.plotTrajectory()
        except:
            pass

    # ## $NV\{T_1>T_2=0\}$ Markovian
    def nvt0(self):
        # In[43]:

        self.N_SLOW_PERIODS = float(1200) * 3
        ############################
        # For debugging only:     ##
        self.N_SLOW_PERIODS /= 50
        ############################
        self.D_T = float(10)
        self.init(Delta_T=self.D_T)

        # In[44]:
        if not self.IS_NONINTERACTIVE:
            self.run_numba(debugWhat="effectiveGamma")

        # In[45]:

        try:
            self.plotTrajectory()
        except:
            pass

        # ## Analysis of the random force

        # In[46]:

        debug(self.Rt.shape)
        debug(self.Rt[0:5])
        debug(self.Rt[-5:])

        # In[47]:

        aRt = abs(self.Rt)
        debug(aRt.shape)
        debug(aRt[0:5])
        debug(aRt[-5:])

        # In[48]:

        debug(aRt.max())
        debug(np.amax(aRt))
        debug(np.argmax(aRt))

        # ### visualization (scatter plot)

        # In[49]:

        if not self.IS_NONINTERACTIVE:
            plt.autoscale(tight=True)
            plt.scatter(self.t_grid, self.Rt[:self.t_n, 0, 0])

        # ### auto-convolution

        # In[50]:

        # In[53]:

        self.plotGaussianFit()

        # ### frequency domain

        # In[54]:

        # In[55]:

        self.plotRandomForceFreqDom()

    def plotRandomForceFreqDom(self):
        if self.IS_NONINTERACTIVE:
            return

        out = np.fft.fft(self.Rt[:self.t_n, 0, 0])
        debug(out.shape)
        plt.plot(out.real)
        plt.plot(out.imag)
        return out

    def autoconvoluteRandomForce(self, isDebug=False):
        sig = self.Rt[:self.t_n, 0, 0]
        # list[::-1] means iterate backwards over list
        autocorr = np.convolve(sig, sig[::-1])
        if isDebug:
            debug("autocorr.shape = ", autocorr.shape)
        return autocorr

    # In[51]:

    def fitGaussianToRandomForce(self, sym_t_grid, autocorr):

        guessVariance = 2 * self.M_B * self.GAMMA_B * self.T_L
        guessParams = [1e-10, 0, guessVariance * guessVariance]
        if(self.N_ATOMS > self.t_n):
            raise RuntimeError("len(guessParams) == {} > {} == self.t_n . Too few samples to perform fit of this many variables!".format(
                len(guessParams), self.t_n))
        fitParams, fitCovar = curve_fit(
            self.gaussian_fit, sym_t_grid, autocorr, p0=guessParams)

        info("amplitude", math.sqrt(
            guessParams[0]), "-->", math.sqrt(fitParams[0]))
        info("mu", guessParams[1], "-->", fitParams[1])
        info(
            "sigma", math.sqrt(guessParams[2]), "-->", math.sqrt(fitParams[2]))
        info("covar", fitCovar)
        return fitParams, fitCovar

    # In[52]:

    @staticmethod
    def gaussian_fit(x, amp2, mu, sigma2):
        '''
        As a lambda expression:
        gaussian_fit =
            lambda x, amp2, mu, sigma2:
                math.sqrt(amp2) \*
                math.exp(-(x - mu) \* (x - mu) / (2 \* sigma2)) /
                np.sqrt(2 \* math.pi \* sigma2)

        However, not that this function will be called for a range of 'x' values, and therefore 'x' is not a scalar.
        This can be mitegated by using np.sqrt and np.exp instead of the equivalent methods from the 'math' library.
        '''
        dx = x - mu
        dx2 = dx * dx
        # if not np.isscalar(dx2):
        #   raise RuntimeError("dx2 should be a scalar! {}".format(dx2.shape))
        twoSigma2 = 2 * sigma2
        return np.sqrt(amp2) * np.exp(-dx2 / twoSigma2) / np.sqrt(np.pi * twoSigma2)

    def plotGaussianFit(self):

        if self.IS_NONINTERACTIVE:
            return

        try:
            sym_t_grid = np.linspace(-self.t_max, self.t_max, 2 * self.t_n - 1)
            autocorr = self.autoconvoluteRandomForce()
            fitParams, fitCovar = self.fitGaussianToRandomForce(
                sym_t_grid, autocorr)
            info("fit Covariance:{}".format(fitCovar))
        except RuntimeError as rtException:
            warning(rtException, exc_info=True)
            return

        if not self.IS_NONINTERACTIVE:
            return
        plt.plot(sym_t_grid, autocorr)
        plt.plot(sym_t_grid, self.gaussian_fit(
            sym_t_grid, fitParams[0], fitParams[1], fitParams[2]))
        plt.xlabel("$\Delta$t/[ps]")
        plt.ylabel(r'$\langle R^2(\Delta t) \rangle$')
        plt.show()

    # ## $NV\{T_1>T_2=0\}$ Non-Markovian
    def nvt1gtt2eq0(self):
        # In[56]:
        """
        global self.N_SLOW_PERIODS
        self.N_SLOW_PERIODS /= 16
        """
        self.init(omega_M=float(400), Delta_T=self.D_T)

        # In[57]:
        if not self.IS_NONINTERACTIVE:
            self.run_numba()

        # In[58]:

        try:
            self.plotTrajectory()
        except:
            pass

        # In[59]:

        self.t_max = self.t_n * self.COARSE_GRAINING_RESOLUTION * \
            self.dt / self.PICOSECONDS_TO_SYS_UNITS
        self.t_grid = np.linspace(0, self.t_max, self.t_n)

        if not self.IS_NONINTERACTIVE:
            plt.autoscale(tight=True)
            plt.scatter(self.t_grid, self.Rt[:self.t_n, 0, 0])

        # In[60]:

        self.plotRandomForceFreqDom()

        # In[61]:

        self.plotGaussianFit()

    # In[ ]:

    @staticmethod
    def plotCouplingFrequency(couplingFrequencyDependence, isNoninteractive=True):
        """
        Plots the effective $\Gamma(\omega)$
        applied to the molecular atom/s
        """

        debug("in {}".format("plotCouplingFrequency"))

        if isNoninteractive:
            backend='AGG'
            #matplotlib.use(backend)  # http://stackoverflow.com/a/3054314
            plt.ioff()
        else:
            plt.ion()
        debug("matplotlib backend: {}".format(matplotlib.get_backend()))


        debug("after setting backend for matplotlib to use ({}), actually using {}".format(backend,matplotlib.get_backend()))

        plt.xlabel(str(r'$\omega / cm^{-1}$'))
        plt.ylabel(str(r'$\Gamma_{eff.} / cm^{-1}$'))
        plt.plot(
            couplingFrequencyDependence[
                :, 0] / PlotPoint.WAVENUMBER_TO_SYS_UNITS,
            couplingFrequencyDependence[
                :, 1] / PlotPoint.WAVENUMBER_TO_SYS_UNITS
        )
        ymax = plt.ylim()[1]
        plt.errorbar(
            couplingFrequencyDependence[
                :, 0] / PlotPoint.WAVENUMBER_TO_SYS_UNITS,
            couplingFrequencyDependence[
                :, 1] / PlotPoint.WAVENUMBER_TO_SYS_UNITS,
            yerr=couplingFrequencyDependence[
                :, 2] / PlotPoint.WAVENUMBER_TO_SYS_UNITS,
            fmt='r:'
        )  # consider using fill_between instead
        plt.ylim(ymin=0, ymax=ymax)
        if isNoninteractive:
            debug("isNoninteractive: {}, saving figure".format(isNoninteractive))
            savefig("out/plots/FreqDepSysBathCou.svg", orientation='landscape',
                    transparent=True, bbox_inches='tight')
        else:
            debug("isNoninteractive: {}, showing figure".format(isNoninteractive))
            plt.show()

        debug("exiting {} {}".format("NON-interactive" if isNoninteractive else "interactive","plotCouplingFrequency"))


    # http://stackoverflow.com/a/22365754

    @staticmethod
    def outputFrequencyOrderedCSV(infilename, outfilename):

        import re
        r = re.compile(r'[\[\]]+')

        with open(infilename, "r") as inFilestream:  # RAII

            # lines = [[float(s) for s in r.sub("", line).split()] for line in inFilestream]
            lines = set()
            for line in inFilestream:
                currentLine = []
                parsedLine = r.sub("", line)
                #debug("parsedLine = %s" % parsedLine)
                splitLine = parsedLine.split()
                for s in splitLine:
                    #debug("float element = %g" % float(s))
                    currentLine.append(float(s))
                #debug("currentLine = %s" % ','.join(str(s) for s in currentLine))
                lines.add(tuple(currentLine))
        # inFilestream is closed

        #debug("lines = %s" % '\n'.join((','.join(str(s) for s in l)) for l in lines))

        # set sort key to __getitem__() of the item with index '0'
        from operator import itemgetter
        # sortedLines.sort(key=itemgetter(0))
        sortedLines = sorted(lines, key=itemgetter(0))
        #debug("sortedLines = %s" % '\n'.join((','.join(str(s) for s in l)) for l in sortedLines))

        with open(outfilename, "w") as outFilestream:
            headers = [
                "frequency / {cm}^{-1}", "{Gamma}_{effective} / {cm}^{-1}", "{sigma}_{Gamma} / {cm}^{-1}"]
            outFilestream.write("{}\n".format(','.join(headers)))
            for line in sortedLines:
                outFilestream.write(
                    "{}\n".format(','.join(str(s) for s in line)))
        # outFilestream is closed

        debug("exiting {}".format("outputFrequencyOrderedCSV"))

    # # Scan over 'bath-particle' mass to extrapolate low-frequency dependence

    # In[62]:
    def scan(self):
        self.T_L = self.T_R = self.DELTA_T = 0
        self.OMEGA_M = 0
        self.A = float(4)

        # about an hour for two million time-steps,
        # i.e. 1.8 milliseconds per time-step.

        # In[63]:

        # self.N_SLOW_PERIODS = float(100) * 5 # ~ 10 minutes wall-clock time per simulation
        # self.MAX_ALLOWED_TIME_STEPS = 1e7
        self.n_points = 10

        if self.IS_BATCH:
            self.n_points = 1
        else:
            self.N_SLOW_PERIODS *= 100
            self.MAX_ALLOWED_TIME_STEPS = 1e8

        # In[64]:

        # max_scan_wall_clock_time = self.n_points * (60 * 10)

        # self.N_SLOW_PERIODS = min(self.N_SLOW_PERIODS, (max_scan_wall_clock_time / (self.n_points * 10 * 60)) * 100 * 5)

        # $
        # \begin{align}
        # \log_x (0.50 / 0.01) &= n_{points} = 100 \\
        # \ln (0.50 / 0.01) / \ln x &= 100 \\
        # \ln (0.50 / 0.01) / 100 &= \ln x \\
        # x &= \exp (\ln (0.50 / 0.01) / 100) \approx 103.76\% \\
        # \end{align}
        # $

        # In[ ]:

        # np.set_printoptions(precision=3)
        base = math.exp(math.log(0.40 / 0.01) / self.n_points)
        info(base)

        # In[ ]:

        # 2-D matrix: first column is frequency, second column is avg. Gamma,
        # third column is Gamma Std. Dev. error estimate
        self.couplingFrequencyDependence = np.zeros(shape=(self.n_points, 3))

        m_0 = math.sqrt(self.M0)
        if (1 > m_0):
            raise RuntimeError(
                "initial bath atom mass ({}) must be greater than one for scan!".format(m_0))

        time_elapsed = 0

        runners = []
        for i in range(self.n_points):

            debug(
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            ###################################################################
            m_0 *= base  # #
            info(" starting i = {} (m_0 = {})".format(i, m_0))  # #
            ###################################################################
            # DEBUG -- REMOVE!!! ##############################################
            # if not self.IS_BATCH                                                                          ##
            #     self.N_SLOW_PERIODS = float(100) * i                                                      ##
            #     debug(                                                                                    ##
            #         "DEBUG -- REMOVE!!! i = {}, self.N_SLOW_PERIODS = {}".format(i, self.N_SLOW_PERIODS)) ##
            ###################################################################
            ###################################################################

            if self.IS_MULTITHREADING:
                runners.append(PlotPoint.Runner(self, i, m_0, self.m_factor))
                runners[-1].start()
                info("process #{} starting thread #{} (currently {})".format(
                    self.m_factor, i, threading.active_count()))
                info(str(threading.enumerate()))
                with open("out/FreqDepSysBathCou.txt", "a") as outFilestream:
                    outFilestream.write(
                        np.array_str(self.couplingFrequencyDependence / PlotPoint.WAVENUMBER_TO_SYS_UNITS) + '\n')
            else:
                self.perPoint(i, m_0, time_elapsed)

        if self.IS_MULTITHREADING:
            for i in range(self.n_points):
                runners[i].join()

        info("done! (total # points = {}, total wall-clock time = {})".format(self.n_points,
                                                                              datetime.timedelta(seconds=time_elapsed)))

        # In[ ]:

        info(self.couplingFrequencyDependence)

        with open("out/FreqDepSysBathCou.txt", "a") as outFilestream:
            outFilestream.write(
                np.array_str(self.couplingFrequencyDependence / PlotPoint.WAVENUMBER_TO_SYS_UNITS) + '\n')

        try:
            PlotPoint.outputFrequencyOrderedCSV(
                "out/FreqDepSysBathCou.txt", "out/FreqDepSysBathCou.csv")
        except Exception as e:
            #    error("failed to outputFrequencyOrderedCSV! %s" % str(e))
            raise e

        # TODO: plot sd[coupling] vs. index
        if not (PlotPoint.isnearlyzero(self.couplingFrequencyDependence).any()):
            try:
                self.plotCouplingFrequency(
                    self.couplingFrequencyDependence, isNoninteractive=self.IS_NONINTERACTIVE)
            except:
                pass

    def getCouplingFrequencyDependence(self):
        return np.array_str(self.couplingFrequencyDependence / PlotPoint.WAVENUMBER_TO_SYS_UNITS)

    def perPoint(self, i, m_0, time_elapsed):
        '''
        '''

        info("START: i = {} @ {}".format(i, datetime.datetime.strftime(
            datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))

        self.init(m0=m_0, isDebug=False)
        GammaAvg, GammaSD, simulation_duration = self.run_numba(
            threadIndex=i, isSilent=True)

        # if (PlotPoint.isnearlyzero(GammaSD)):
        #    raise RuntimeError(
        #        "standard deviation of effective Gamma is zero!")

        omega = math.sqrt(self.V0A2 / self.M0)
        couplingFrequencyDataPoint = [omega, GammaAvg, GammaSD]
        # info(couplingFrequencyDataPoint)
        # if(abs(GammaAvg) > GammaSD):
        self.couplingFrequencyDependence[i] = couplingFrequencyDataPoint

        info("FINISH: i = {} (m_0 = {}) {} @{}".format(
            i, m_0, couplingFrequencyDataPoint, datetime.datetime.strftime(
                datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))
        # the following should probably not work asynchronously:
        time_left = self.calcTimeLeft(
            time_elapsed, simulation_duration, segment_no=i + 1, totSegments=self.n_points)
        info("{} left".format(datetime.timedelta(seconds=time_left)))
        time_elapsed += simulation_duration

    @staticmethod
    def exponential_fit(x, amp2, gamma2):
        """
        note: x is an np.ndarray
        lambda x, amp2, gamma2: math.sqrt(amp2) \* np.exp(-x \* math.sqrt(gamma2))
        """
        return math.sqrt(amp2) * math.exp(-x * math.sqrt(gamma2))

    def fitDissipationCurve(self, x):
        guessAmplitude = math.log(self.Esys[0])
        guessGammaEffective = math.log(self.GAMMA_B) if not self.isnearlyzero(
            self.GAMMA_B) else -1e10
        guessParams = [guessAmplitude, guessGammaEffective]
        if(self.N_ATOMS > self.t_n):
            raise RuntimeError("len(guessParams) == {} > {} == self.t_n . Too few samples to perform fit of this many variables!".format(
                len(guessParams), self.t_n))
        fitParams, fitCovar = curve_fit(lambda x_prime, lnAmp, lnGamma: math.exp(
            lnAmp) * np.exp(-x_prime * math.exp(lnGamma)), self.t_grid, x, p0=guessParams)

        info("amplitude", math.exp(guessParams[
            0]), "-->", math.exp(fitParams[0]), " +/- ", math.exp(math.sqrt(fitCovar[0, 0])))
        info("\gamma_{effective}", math.exp(guessParams[
            1]), "-->", math.exp(fitParams[1]), " +/- ", math.exp(math.sqrt(fitCovar[1, 1])))
        info("covariance matrix (ln):", fitCovar)
        return math.exp(guessParams[1]), math.exp(math.sqrt(fitCovar[1, 1]))

    def fitDissipationConstrained(self):
        """
        minimize $|| E[x,1] - amp \* \exp(-\gamma \* x) ||^2$
        subject to $amp, \gamma > 0$
        """

        # objective function: E[x, 1] - amp * exp(-gamma * x)
        # parameters' vector:            0           1
        def objective_fun(p, y, x):
            amp, gamma = p
            err = y - amp * np.exp(-gamma * x)
            return err

        def residuals_fun(p, y, x):
            return np.square(objective_fun(p, y, x))

        # derivative of objective function: -gamma * amp * exp(-gamma * x)
#         def objective_fun_deriv(p, sign=1.0):
#             dfdp0 = np.sign(sign) * ((p[0] * np.exp(-p[1] * x_prime)) * (p[0] * np.exp(-p[
#                 1] * x_prime) - 2 * E[x_prime]) * np.exp(-p[1] * x_prime) for x_prime in t_grid)
#             dfdp1 = np.sign(sign) * ((p[0] * np.exp(-p[1] * x_prime)) * (p[0] * np.exp(-p[1] * x_prime) - 2 * E[
#                 x_prime]) * (-x_prime * p[0] * np.exp(-p[1] * x_prime)) for x_prime in t_grid)
#             return np.array([dfdp0, dfdp1])

        # constraint functions: amp > 0, gamma > 0
        # constraint function Jacobian:
        # constraints = ({'type':'ineq', 'fun':,'jac'=},)

        systemAtom = 1
        x = self.t_grid
        y = self.E[:self.t_max, systemAtom]
        p0 = [self.E[0, systemAtom], self.GAMMA_B]
        cons = ({'type': 'ineq', 'fun': lambda p: p[0]}, {
                'type': 'ineq', 'fun': lambda p: p[1]})
        minimize(objective_fun, p0, args=(y, x), method="COBYLA",
                 constraints=cons, options={'disp': True})

    def calcRsquared(self, x, y, coeffs):
        '''
        http://stackoverflow.com/a/895063/3368225

        For some reason, plofit with full=True gives SVD values (and residuals),
        but the correlation matrix is given only with full=False
        '''

        debug("in calcRsquared")

        # total residuals
        yBar = np.sum(y) / len(y)  # mean value
        ssTot = np.sum((y - yBar)**2)
        if PlotPoint.isnearlyzero(ssTot):
            warning(
                "total sum of squared residuals is nearly zero -- Coefficient of Determination (R^2) diverges.")
            return float('nan')

        # explained resuduals
        p = np.poly1d(coeffs)  # build polinomial function from coefficients
        yHat = p(x)  # fit values
        ssReg = np.sum((yHat - yBar)**2)

        # R^2 (Coefficient of Determination)
        return ssReg / ssTot

    def fitDissipationPoly(self, systemAtom=1):
        x = self.t_grid
        yList = [math.log(self.E[t_n, systemAtom]) for t_n in x]
        y = np.array(yList)
        polynomialDegree = 1
        isPrintCovarMx = True
        p, V = np.polyfit(x, y, deg=polynomialDegree, cov=isPrintCovarMx)
        amp = p[0]
        gamma = -p[1]
        d_amp = math.sqrt(V[0, 0])
        d_gamma = math.sqrt(V[1, 1])
        debug(
            "amp = {} +/- {}, gamma = {} +/ {}".format(amp, d_amp, gamma, d_gamma))
        # return amp, d_amp, gamma, d_gamma

        R2 = self.calcRsquared(x, y, p)
        if not math.isnan(R2):
            debug("R^2 = %g%%" % 100.0 * R2)

        return [p[0], -p[1]], V

    def fitDissipation(self):  # , x, exp_constraints=False):
        """
        extract coefficient of first-order dissipation:
        $\gamma_{effective} ~ \lim_{t \rightarrow \inf} - ln(E(t)/E(0)) / t$
        """
        # return self.fitDissipationCurve(x) if exp_constraints else
        # self.fitDissipationConstrained(x)
        return self.fitDissipationPoly()


class Runner(threading.Thread):
    '''
    Thread runner, per plot point
    '''

    def __init__(self, plotPoint, i, m_0, m_factor):
        threading.Thread.__init__(self, name="{}:{}".format(i, m_0))
        self.i = i
        if m_factor is None:
            self.m0 = m_0
        else:
            self.m0 = math.sqrt(plotPoint.M0) * math.pow(2, m_factor)
        self.time_elapsed = 0

    def run(self):
        try:
            self.plotPoint.perPoint(self.i, self.m0, self.time_elapsed)
        except Exception as e:
            error(self.i, self, e)
        info(threading.enumerate())

# ## V&V

# ### cProfile and profile stats

# In[35]:

pr = None


def profile():  # plotPoint):
    cProfile.run('plotPoint.run_numba()')
    """
    pr = cProfile.Profile()
    pr.enable()
    plotPoint.run_numba()
    #pr.create_stats()
    pr.disable()
    """

# In[36]:


def profile_stats():
    profile()
    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats('cumulative')
    ps.print_stats()
    info(s.getvalue())

# ### Profile Run

# In[37]:

# %%prun
# run_numba()

# #### line_profiler run

# In[38]:

# %%lprun
# run_numba()

# #### memory_profiler run

# In[39]:

# %%mprun
# run_numba()


def main(m_factor=1):
    p = PlotPoint(m_factor)
    debug("{}".format(p.getCouplingFrequencyDependence()))
    info("exiting {}({})".format(__name__, m_factor))

if ('__main__' == __name__):
    m_factor = 1 if (len(sys.argv) < 2 or sys.argv[1] is None) else sys.argv[1]
    main(m_factor)
